define(['require', 'exports', './Util', './Resources'], function (require, exports, Util, Resources) {
    'use strict';
    var ListenerManager = (function () {
        function ListenerManager() {
            this.util = Util.getInstance();
            $('#message').hide();
            window.addEventListener('orientationchange', doOnOrientationChange);
            this.addListenerOn(document.getElementById('canvas'), ['touchmove'], this.util.preventDefault);
        }

        ListenerManager.getInstance = function () {
            if (!this.instance) {
                this.instance = new ListenerManager();
            }
            return this.instance;
        };
        ListenerManager.prototype.addListenerOn = function (el, events, callback, options) {
            for (var _i = 0, events_1 = events; _i < events_1.length; _i++) {
                var e = events_1[_i];
                el.addEventListener(e, callback, options || false);
            }
        };
        ListenerManager.prototype.removeListenerOn = function (el, events, callback, options) {
            for (var _i = 0, events_2 = events; _i < events_2.length; _i++) {
                var e = events_2[_i];
                el.removeEventListener(e, callback, options || false);
            }
        };
        ListenerManager.prototype.clearAllInputListener = function () {
            for (const id of Resources.INPUTS) {
                const el = document.getElementById(id),
                    elClone = el.cloneNode(true);
                el.parentNode.replaceChild(elClone, el);
            }
        };
        return ListenerManager;
    }());

    function doOnOrientationChange() {
        switch (window.orientation) {
            case -90:
            case 90:
                $('#message').hide();
                $('#screen').show();
                break;
            default:
                $('#message').show();
                $('#screen').hide();
                break;
        }
        $('#screen').show();
    }

    doOnOrientationChange();
    return ListenerManager;
});
