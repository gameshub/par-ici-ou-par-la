/**
 * @interface
 * @classdesc Interface Asset, défini un asset, soit un élément du canvas comme un bouton ou une grille
 * @author Vincent Audergon
 * @version 1.0
 */
class Asset {

    /**
     * Retourne la liste des éléments PIXI d'un asset
     * @return {Object[]} les éléments PIXI
     */
    getPixiChildren(){}

    /**
     * Retourne la position y du composant
     * @return {number} posY
     */
    getY(){}
    /**
     * Retourne la position x du composant
     * @return {number} posX
     */
    getX(){}
    setY(y){}
    setX(x){}
    getWidth(){}
    getHeight(){}
    setVisible(visible){}

}
class Arrow extends Asset {
    constructor(startX, startY, toX, toY, color, arrow = false, cpXY = null, width = 8, dashed = null) {
        super();
        this.startX = startX;
        this.startY = startY;
        this.toX = toX;
        this.toY = toY;
        this.arrow = arrow;
        this.color = color;
        this.cpXY = cpXY;
        this.dashed = dashed;
        this.width = width;
        this.container = new PIXI.Container();
        this.display();
    }

    display() {
        //Supprimer le contenu et setup de notre objet graphique
        this.container.removeChildren();
        this.container.x = 0;
        this.container.y = 0;
        this.container.width = 600;
        this.container.height = 600;

        let distX = this.toX - this.startX;
        let distY = this.toY - this.startY;
        let normal = [-(distY), distX]

        let hypo = Math.sqrt(normal[0] ** 2 + normal[1] ** 2);
        normal[0] /= hypo;
        normal[1] /= hypo;

        //permet de déplacer le vecteur normal plus vers l'arrière de la ligne
        let tangent = [ -normal[1] * 57, normal[0] * 57]
        normal[0] *= 14;
        normal[1] *= 14;

        let propX = this.startX + (distX / hypo * (hypo-39));
        let propY = this.startY + (distY / hypo * (hypo-39));
        
        let line = new PIXI.Graphics();

        if(this.cpXY == null){
            let endX = this.toX;
            let endY = this.toY;
            if(this.arrow){
                endX = this.startX + (distX / hypo * (hypo - 47));
                endY = this.startY + (distY / hypo * (hypo - 47));
            }
            line.lineStyle(this.width, this.color, 1);
            line.moveTo(this.startX, this.startY);

            //si c'est traitillé il appelle la méthode pour dessinner les traits
            if(this.dashed == null){
                line.lineTo(endX, endY);
            }else{
                this.drawDashLine(line, this.startX, this.startY, endX, endY, this.dashed[0] ,this.dashed[1]);
            }
                                              
        }else{
            line
                .lineStyle(this.width, this.color, 1)
                .moveTo(this.startX, this.startY)
                .bezierCurveTo(this.cpXY[0], this.cpXY[1], this.cpXY[2], this.cpXY[3], this.toX, this.toY);

            //Flèche au bout de la flèche ronde
            if(this.arrow){
                normal = [ -(this.toY - this.cpXY[3]), this.toX - this.cpXY[2]]
                hypo = Math.sqrt(normal[0] ** 2 + normal[1] ** 2);
                normal[0] /= hypo;
                normal[1] /= hypo;    
                tangent = [ -normal[1] * 20, normal[0] * 20]
                normal[0] *= 14;
                normal[1] *= 14;
                propX = this.toX;
                propY = this.toY;
            }
        }
        
        //enlever this.cpXY == null si on veut ajouter flèche au bout des courbes
        if(this.arrow && this.cpXY == null){
            line
                //triangle bout flèche
                .beginFill(this.color, 1)
                .lineStyle(0, this.color, 1)
                .moveTo(this.toX - normal[0] + tangent[0], this.toY - normal[1] + tangent[1])
                .lineTo(this.toX + normal[0] + tangent[0], this.toY + normal[1] + tangent[1])
                .lineTo(this.toX + normal[0] + tangent[0], this.toY + normal[1] + tangent[1])
                .lineTo(propX , propY)
                .endFill();
        }
        this.container.addChild(line);
    }

    getPixiChildren() {
        return [this.container];
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    getFromX(){
        return this.startX;
    }

    setFromX(fromX){
        this.startX = fromX;
    }

    getFromY(){
        return this.startY;
    }

    setFromY(fromY){
        this.startY = fromY;
    }

    getFromY(){
        return this.startY;
    }

    setFromY(fromY){
        this.startY = fromY;
    }

    drawDashLine(line, x, y, toX, toY, tailleTrais, tailleEspace) {
        let distX = toX - x;
        let distY = toY - y;
        let hypo = Math.sqrt(distX ** 2 + distY ** 2);

        let pointHypo = tailleTrais;

        let startX = x;
        let startY = y;

        while (pointHypo <= hypo) {
            startX = x + (distX / hypo * (pointHypo));
            startY = y + (distY / hypo * (pointHypo)); 
            
            line.lineTo(startX, startY);

            pointHypo += tailleEspace;

            startX = x + (distX / hypo * (pointHypo));
            startY = y + (distY / hypo * (pointHypo)); 

            line.moveTo(startX, startY);

            pointHypo += tailleTrais;

            //permet de dessiner un trait plus petit si besoin pour bien correspondre à la traille voulue
            if((pointHypo - hypo) > 0 && (pointHypo - tailleTrais) < hypo){
                pointHypo = hypo;
            }
        }
      };
}
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class Button extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     */
    constructor(x, y, label, bgColor, fgColor, autofit = false, width = 150,height,radius) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
        this.lbl = new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 18, fill: fgColor, align: 'center'})

        this.height = height;
        this.autofit = autofit;
        this.width = width;
        this.radius=radius;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
    }


    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        this.lbl.text = text;
        this.update();
    }

    update(){
        let buttonWidth = this.getWidth();
        let buttonHeight = this.lbl.height * 1.5;
        //this.height = buttonHeight;
        this.graphics.clear();
        this.graphics.beginFill(this.bgColor);
        this.graphics.drawRoundedRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight,this.radius);
        this.graphics.endFill();
        this.lbl.anchor.set(0.5);
        this.lbl.x = this.x;
        this.lbl.y = this.y;
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics, this.lbl];
    }


    updateFont(font) {
        this.lbl.style.fontFamily = font;
    }

    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        for (let element of this.getPixiChildren()) {
            element.visible = visible
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getWidth() {
        return this.autofit ? (this.width < this.lbl.width + 20 ? this.lbl.width + 20 : this.width) : this.width;
    }
}
/**
 * @classdesc Asset bouttonTexte
 * @author Xavier Montasell
 * @version 1.0
 */
class ButtonTexte extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     */
    constructor(x, y, label, bgColor, fgColor, autofit = false, width = 150, height) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
        this.lbl = new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 12, fill: fgColor, align: 'left'})

        this.height = height;
        this.autofit = autofit;
        this.width = width;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
        this.onRelease = function () {
            console.log('Replace this action with button.setOnRelease')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        this.graphics.interactive = true;
        this.graphics.buttonMode = true;
        this.graphics.visible= false;
        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
        this.graphics.on('mouseup', function () {
            this.onRelease()
        }.bind(this));
        this.graphics.on('touchend', function () {
            this.onRelease()
        }.bind(this));
        this.graphics.on('mouseupoutside', function () {
            this.onRelease()
        }.bind(this));
        this.graphics.on('touchendoutside', function () {
            this.onRelease()
        }.bind(this));
    }

    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        this.lbl.text = text;
        this.update();
    }

    update() {
        let buttonWidth = 200;
        let buttonHeight = 12;
        this.graphics.clear();
        this.graphics.beginFill(this.bgColor);
        this.graphics.drawRect(this.x - this.width / 2, this.y - this.height / 2, this.width, this.height);
        this.graphics.endFill();
        this.lbl.anchor.set(0.5);
        this.lbl.x = this.x;
        this.lbl.y = this.y;
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }

    setOnRelease(onRelease) {
        this.onRelease = onRelease;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics, this.lbl];
    }


    updateFont(font) {
        this.lbl.style.fontFamily = font;
    }

    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        for (let element of this.getPixiChildren()) {
            element.visible = visible
        }
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }

    setBgColor (color){
        this.bgColor = color;
        this.update();
    }
    getWidth() {
        return this.autofit ? (this.width < this.lbl.width + 20 ? this.lbl.width + 20 : this.width) : this.width;
    }
}
/**
 * @classdesc Asset bouttonZone
 * @author Xavier Montasell
 * @version 1.0
 */
class ButtonZone extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     */
    constructor(x, y, label, bgColor, fgColor, autofit = false, width = 150, height, radius) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
        this.lbl = new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 12, fill: fgColor, align: 'left'})

        this.height = height;
        this.radius = radius;
        this.autofit = autofit;
        this.width = width;
        //this.graphics.visible=false;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
        this.onRelease = function () {
            console.log('Replace this action with button.setOnRelease')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        this.graphics.interactive = false;
        this.graphics.buttonMode = false;

        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
        this.graphics.on('mouseup', function () {
            this.onRelease()
        }.bind(this));
        this.graphics.on('touchend', function () {
            this.onRelease()
        }.bind(this));
        this.graphics.on('mouseupoutside', function () {
            this.onRelease()
        }.bind(this));
        this.graphics.on('touchendoutside', function () {
            this.onRelease()
        }.bind(this));
    }


    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        this.lbl.text = text;
        this.update();
    }

    update() {
        let buttonWidth = 200;
        let buttonHeight = 12;
        //this.height = buttonHeight;
        this.graphics.clear();
        this.graphics.beginFill(this.bgColor);
        this.graphics.drawRoundedRect(this.x - this.width / 2, this.y - this.height / 2, this.width,  this.height, this.radius);
        this.graphics.endFill();
        this.lbl.anchor.set(0.5);
        this.lbl.x = this.x;
        this.lbl.y = this.y;
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }
    setOnRelease(onRelease) {
        this.onRelease = onRelease;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics, this.lbl];
    }


    updateFont(font) {
        this.lbl.style.fontFamily = font;
    }

    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
         for (let element of this.getPixiChildren()) {
             element.visible = visible
         }
        //this.container.visible = visible;
    }
    setInteractive(interactive) {
        for (let element of this.getPixiChildren()) {
            element.interactive = interactive
        }
        //this.container.visible = visible;
    }
    setButtonmode(buttonmode) {
        for (let element of this.getPixiChildren()) {
            element.visible = buttonmode
        }
        //this.container.visible = visible;
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getWidth() {
        return this.autofit ? (this.width < this.lbl.width + 20 ? this.lbl.width + 20 : this.width) : this.width;
    }
}
class CheckBox extends Asset {

    /**
     * Créé le composant graphic
     */
    constructor(x = 0, y = 0, text = '', sizeFactor=1) {
        super();
        this.y = y;
        this.x = x;
        this.text = text;
        this.sizeFactor = sizeFactor;

        this.selected = false;

        this.elements = {
            square: new PIXI.Graphics(),
            line1: new PIXI.Graphics(),
            line2: new PIXI.Graphics(),
            text: new PIXI.Text('', {fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'left', breakWords:true, wordWrap:true})
        };

        this.elements.square.interactive = true;
        this.elements.square.buttonMode = true;
        this.elements.square.on('pointerdown', function () {
            this.select();
        }.bind(this));

        this.draw();
    }


    /**
     * Modifie la position de la checkbox.
     * @param x {number} Position X
     * @param y {number} Position Y
     */
    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.draw();
    }

    setText(value) {
        this.text = value;
        this.draw();
    }

    /**
     * Affiche ou cache le composant.
     * @param visible {boolean} Est visible ?
     */
    setVisible(visible) {
        if (!visible)
            for (let element of this.getPixiChildren())
                element.visible = false;
        else {
            this.elements.text.visible = true;
            this.elements.square.visible = true;
            this.select(false);
        }
    }

    draw() {
        let line1StartX = this.x + 3*this.sizeFactor;
        let line1StartY = this.y + 20*this.sizeFactor;
        let line1EndX = this.x + 13*this.sizeFactor;
        let line1EndY = this.y + 30*this.sizeFactor;

        let line2StartX = line1EndX-5*this.sizeFactor/4;
        let line2StartY = line1EndY+5*this.sizeFactor/4;
        let line2EndX = this.x + 35*this.sizeFactor;
        let line2EndY = this.y + 8*this.sizeFactor;


        this.elements.square.clear();
        this.elements.square.lineStyle(1, 0x000000, 1);
        this.elements.square.beginFill(0xe0e0e0, 0.25);
        this.elements.square.drawRoundedRect(this.x, this.y, 38*this.sizeFactor, 38*this.sizeFactor, 5);
        this.elements.square.endFill();

        this.elements.line1.clear();
        this.elements.line1.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line1StartX, line1StartY).lineTo(line1EndX, line1EndY);

        this.elements.line2.clear();
        this.elements.line2.lineStyle(5*this.sizeFactor, 0x000000).moveTo(line2StartX, line2StartY).lineTo(line2EndX, line2EndY);


        this.elements.text.text = this.text;
        this.elements.text.style.wordWrapWidth =600-Math.floor( this.x + 5 + 38*this.sizeFactor +25);
        this.elements.text.anchor.set(0.0);
        this.elements.text.x = this.x + 38*this.sizeFactor+5;
        this.elements.text.y = this.y + (38*this.sizeFactor-this.elements.text.height)/2;

    }

    getPixiChildren() {
        let objects = [];
        for (let element of Object.keys(this.elements)) {
            objects.push(this.elements[element]);
        }
        return objects;
    }

    select(toggle = true) {
        if (toggle)
            this.selected = !this.selected;
        this.elements.line1.visible = this.selected;
        this.elements.line2.visible = this.selected;
    }

    isChecked() {
        return this.selected;
    }


    updateFont(font){
        this.elements.text.style.fontFamily = font;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }


    setY(y) {
        this.y = y;
        this.setPosition(this.x, this.y);
    }

    setX(x) {
        this.x = x;
        this.setPosition(this.x, this.y);
    }
}
/**
 * @classdesc Asset grille de dessin, retourne une séries de points lorsque l'utilisateur a dessiné le chemin
 * @author Xavier Montasell
 * @version 3.0
 */
class DrawingGrid extends Asset {

    /**
     * Constructeur de la grille de dessin
     * @param {int} col Le nombre de colonnes qui composent la grille
     * @param {int} lines Le nombre de lignes qui composent la grille
     * @param {double} width La largeur / hauteur entre chaque noeuds de la grille
     * @param {Pixi.Stage} stage Le stage Pixi
     * @param {function} onShapeCreated La fonction de callback appelée lorsqu'une forme a été dessinée
     */
    constructor(col, lines, width, stage, onShapeCreated) {
        super();
        /** @type {int} le nombre de colonnes */
        this.col = col;
        /** @type {int} le nombre de lignes */
        this.lines = lines;
        /** @type {double} la largeur / hauteur d'une cellule */
        this.width = width;
        /** @type {PIXI.Stage} le stage PIXI sur lequel dessiner les éléments de la grille */
        this.stage = stage;
        /** @type {Node[]} la liste des noeuds qui composent la grille */
        this.nodes = [];
        /** @type {ImgStatic[]} la liste des images qui composent la grille */
        this.logoCreate = [];
        /** @type {Point[]} la liste des points de la forme en cours de dessin */
        this.points = [];
        /** @type {PIXI.Graphics} la liste des lignes dessinées */
        this.strLines = [];
        /** @type {Point} l'origine de la forme en cours de dessin */
        this.origin = new Point(0, 0);
        /** @type {Node} le dernier noeud rencontré lors du dessin */
        this.lastnode = undefined;
        /** @type {PIXI.Graphics} le trait qui suit le curseur lors d'un dessin */
        this.line = undefined;
        /** @type {function} fonction de callback appelée lorsqu'une forme est dessinée */
        this.onShapeCreated = onShapeCreated;
        /** @type {boolean} si un dessin est en cours */
        this.drawing = false;
        /** @type {boolean} si l'option est en true le chemin change de couleur lorsque nous vérifions le chemin */
        this.visibleWay = true;
        /** @type {boolean} si l'option est en false nous ne pourrons pas changer le mode visibleWays */
        this.blockVisibleWays = false;
        /** @type {int} le nombre de point qui doit y passer */
        this.nombrePointAPasser = 0;
        /** Permet de savoir si c'est le mode "Créer" */
        this.modeCreate = false;
        this.blockNode = false;
        this.createWays = false;
        this.pointStartEnd = [];
        this.startSetup = false;
        this.endSetup = false;
        this.tradImage = [];
        this.init();
    }

    /**
     * Initialise la grille de dessin
     */
    init() {
        this.stage.interactive = true;
        this.startSetup = false;
        this.endSetup = false;
        this.stage.on('pointermove', this.onPointerMove.bind(this));
        this.stage.on('pointerup', this.onReleased.bind(this));

        for (let c = 0; c < this.col; c++) {
            for (let l = 0; l < this.lines; l++) {

                if (this.col === 5 && this.lines === 4) {
                    this.nodes.push(new Node(c * this.width + 95, l * this.width + 45, this.width / 50, this, c + ";" + l));
                } else {
                    /** Modification effectué par David Dumas
                     * this.nodes.push(new Node(c * this.width + 15, l * this.width + 30, this.width / 4.5, this, c + ";" + l)); (jeu par défaut)
                     * this.nodes.push(new Node(c * this.width + 38, l * this.width + 10, this.width / 50, this, c + ";" + l));
                     * */
                    this.nodes.push(new Node(c * this.width + 38, l * this.width + 12, this.width / 50, this, c + ";" + l));
                }
            }
        }
    }

    /**
     * Cette méthode permet d'afficher l'image donnée avec les paramètres suivants
     * @param logo le nom du logo dans la DB
     * @param nodeHold les coordonnées ou l'implenter
     * @param namePeople Le nom de l'image en fonction de ci c'est un bonhomme ou non
     * @param booleanStart le boolean de l'information de start
     * @param booleanEnd le boolean de l'information de end
     */
    affichageImage(logo, nodeHold, namePeople, booleanStart, booleanEnd) {
        let nbreLang = 4;

        let lang = this.refGameDG.global.resources.getLanguage();
        let numberLang = this.nbreLangue(lang);

        let trad = [];
        for (let i = 0; i < nbreLang; i++) {
            trad[i] = namePeople[i];
        }
        this.tradImage.push(trad);

        // On récupère l'image dans la DB
        let defaultWidth = 65;
        // on prend l'image en question
        let logoImg = this.refGameDG.global.resources.getImage(logo).image.src;
        let logoImage = new ImgStatic(nodeHold.x, nodeHold.y, defaultWidth, logoImg, namePeople[numberLang], true, "");
        logoImage.setNameLogo(logo);
        logoImage.setModeCreate(true);
        logoImage.setRefDG(this);
        logoImage.setRefCreate(this.refCreate);
        logoImage.setRefTrad(this.refGameDG);
        logoImage.setStart(booleanStart)
        logoImage.setEnd(booleanEnd);
        this.logoCreate.push(logoImage);
        this.refCreate.displayLogo(this.logoCreate);
        this.resetNoLogoCreate();
    }

    nbreLangue(lang) {
        let nbre;
        switch (lang) {
            case "fr":
                return nbre = 0;
            case "de":
                return nbre = 1;
            case "it":
                return nbre = 2;
            case "en":
                return nbre = 3;
        }
    }

    /**
     * Permet de récupérer LogoCreate un tableau d'ImgStatic
     * @returns {ImgStatic[]}
     */
    getLogoCreate() {
        return this.logoCreate;
    }

    /**
     * Réinitialise la grille de dessin
     */
    reset() {
        for (let line of this.strLines) {
            this.stage.removeChild(line);
        }
        this.strLines = [];
        this.points = [];
        this.logoCreate = [];
        this.origin = new Point(0, 0);
        this.drawing = false;
        this.blockVisibleWays = false;
        this.lastnode = null;
    }

    /**
     * Méthode qui permet de tout reset sauf le tableau logoCreate
     */
    resetNoLogoCreate() {
        for (let line of this.strLines) {
            this.stage.removeChild(line);
        }
        this.strLines = [];
        this.points = [];
        this.origin = new Point(0, 0);
        this.drawing = false;
        this.blockVisibleWays = false;
        this.lastnode = null;
    }

    /**
     * Change la valeur de la variable blockNode
     * @param boolean
     */
    setBlockNode(boolean) {
        this.blockNode = boolean;
    }

    /**
     * Méthode qui permet de bloquer le fait qu'on puisse cliquer sur une image
     * @param boolean
     */
    setBlockLogo(boolean) {
        for (let i = 0; i < this.logoCreate.length; i++) {
            if (this.logoCreate[i] !== null) {
                this.logoCreate[i].setTouchable(boolean);
            } else {
                this.logoCreate[i] = null;
            }
        }
    }

    /**
     * Méthode qui permet de modifier la valeur de "logoCreate"
     * @param arrayImg
     */
    setLogoCreate(arrayImg) {
        this.logoCreate = arrayImg;
    }

    /**
     * Créer le prochain point du polygone en cours de dessin
     * @param {Node} node le {@link Node} qui défini le nouveau point
     * @return {Point} le nouveau {@link Point}
     */
    createNextPoint(node) {
        let nodeCoords = node.getid();
        let coords = nodeCoords.split(";");
        let p = new Point(coords[0], coords[1]);
        p.name = node.getid();
        return p;
    }

    /**
     * Vérifie qu'un point ne soit pas deja existant dans la grille
     * @param {Point} p le point à controller
     * @return {boolean} si le point existe déjà
     */
    containsPoint(p) {
        for (let point of this.points) {
            if (point.x === p.x && point.y === p.y) return true;
        }
        return false;
    }

    /**
     * Créer le trait affiché sur la grille lorsque l'utilisateur déssine
     * @param {Node} node le noeud de départ
     */
    initLine(node) {
        this.line = new PIXI.Graphics();
        this.stage.addChild(this.line);
        this.line.lineStyle(6, 0xFF0000, 1);
        this.line.moveTo(node.center().x, node.center().y);
    }

    /**
     * Dessine une ligne le dernier noeud rencontré et le noeud donné en argument
     * @param {Node} node le noeud jusqu'au quel faire le trait
     */
    drawStrLine(node) {
        if (this.lastnode.center().x == node.center().x || this.lastnode.center().y == node.center().y) {
            let lastNodeCoords = this.lastnode.getid();
            let lnCoords = lastNodeCoords.split(";");
            let nodeCoords = node.getid();
            let coords = nodeCoords.split(";");
            let sommeX = coords[0] - lnCoords[0];
            let sommeY = coords[1] - lnCoords[1];
            if (sommeX == 1 || sommeX == -1 || sommeX == 0) {
                if (sommeY == 1 || sommeY == -1 || sommeY == 0) {
                    this.stage.removeChild(this.line);
                    this.initLine(node);
                    let straightLine = new PIXI.Graphics();
                    this.strLines.push(straightLine);
                    this.stage.addChild(straightLine);
                    straightLine.lineStyle(6, ORANGE, 1);
                    straightLine.moveTo(this.lastnode.center().x, this.lastnode.center().y);
                    straightLine.lineTo(node.center().x, node.center().y);
                    this.lastnode = node;
                    if (this.onLineDrawn) this.onLineDrawn();
                }
            }
        }
    }

    /**
     * Retourne la liste des éléments PIXI de la grille de dessin
     * @return {Object[]} les éléments PIXI qui composent la grille
     */
    getPixiChildren() {
        let children = [];
        for (let n of this.nodes) {
            if (n.graphics) children.push(n.graphics);
            if (n.point) children.push(n.point);
        }
        return children;
    }

    /**
     * Méthode de callback appelée lorsqu'un click est effectué sur un noeud de la grille.
     * Défini le noeud comme étant le noeud de départ et crée le trait de dessin
     * @param {Event} e l'événement JavaScript
     * @param {Node} node le noeud sur lequel on a clické
     */
    onNodeClicked(e, node) {
        if (!this.blockNode) {
            if (this.lastnode != null) {
                if (this.lastnode.getid() == node.getid()) {
                    this.initLine(this.lastnode);
                    this.drawing = true;
                    this.blockVisibleWays = true;
                    this.points.push();
                    this.origin = this.createNextPoint(node);
                }
            } else {
                this.lastnode = node;
                this.initLine(node);
                let nodeCoords = node.getid();
                let coords = "";
                coords = nodeCoords.split(";");
                if (this.modeCreate) {
                    this.drawing = false;
                    this.actionPopPup(coords);
                } else {
                    this.drawing = true;
                    this.blockVisibleWays = true;
                    this.points.push();
                    this.origin = this.createNextPoint(node);
                    let p = new Point(coords[0], coords[1]);
                    p.name = this.origin.name;
                    this.points.push(p);
                    this.drawStrLine(node)
                }

            }
        }
    }

    /**
     * Méthode de callback appelée lorsque le click est relâché
     * @param {Event} e L'événement JavaScript
     */
    onReleased(e) {
        if (this.points.length === 1) {
            this.stage.removeChild(this.line);
            this.initLine(this.lastnode);
            let straightLine = new PIXI.Graphics();
            this.strLines.push(straightLine);
            this.stage.addChild(straightLine);
            straightLine.lineStyle(6, ORANGE, 1);
            const squareLine = {
                width: 10,
                height: 10
            };
            straightLine.beginFill(ORANGE, 1);
            straightLine.drawRect(this.lastnode.center().x - squareLine.width / 2, this.lastnode.center().y - squareLine.height / 2, squareLine.width, squareLine.height);
            straightLine.endFill();
        }
        this.drawing = false;
        this.stage.removeChild(this.line);
    }

    /**
     * Méthode de callback appelée lorsque le curseur bouge.
     * Déssine le trait à la position du curseur
     * @param {Event} e L'événement JavaScript
     */
    onPointerMove(e) {
        if (this.drawing) {
            let position = e.data.getLocalPosition(this.stage);
            this.line.lineTo(position.x, position.y);
            /** Correction David Dumas "Hors du jeu, on réinitialise le lien" */
            if (position.x < 0 || position.x > 600 || position.y < 0 || position.y > 600) {
                this.drawing = false;
                this.stage.removeChild(this.line);
            }
            for (let n of this.nodes) {
                if (n.graphics.containsPoint(e.data.getLocalPosition(n.graphics.parent))) this.onNodeEncountered(e, n);
            }
        }
    }

    /**
     * Méthode de callback appelée lorsque que le curseur touche un noeud de la grille.
     * Vérifie si la forme est terminée ou si il faut dessiner un trait droit entre ce noeud et le noeud de départ.
     * @param {Event} e L'événement JavaScript
     * @param {Node} node Le noeud de la grille touché
     */
    onNodeEncountered(e, node) {
        if (this.drawing && this.lastnode !== node) { // Si le noeud rencontré n'est pas le dernier noeud
            let point = this.createNextPoint(node);
            this.drawStrLine(node);
            let len = this.points.length;
            if (this.lastnode.center().x == node.center().x || this.lastnode.center().y == node.center().y) {
                this.points.push(point);
            }
        }
    }

    /**
     * Défini la fonction de callback appelée lorsqu'une ligne est dessinée
     * @param {function} onLineDrawn La fonction de callback
     */
    setOnLineDrawn(onLineDrawn) {
        this.onLineDrawn = onLineDrawn;
    }

    /**
     * Ce qu'il va etre réalise l'hors que le bouton reesayer est appuyé
     * @param {Array} boutons liste des boutons
     */
    onReset(boutons) {
        this.reset();
    }

    /**
     * Cette methode permet changer la couleur du chemin dessiné
     * @param pointDebut String contenant les coordonees du point initial
     * @param pointFin String contenant les coordonees du point final
     * @param color nouveau couleur de la ligne
     */
    changecolor(pointDebut, pointFin, color) {
        if (this.visibleWay) {
            let count = 0;
            let debutX = 0;
            let debutY = 0;
            let finX = 0;
            let finY = 0;
            for (let key in this.nodes) {
                let coords = this.nodes[count].getid();
                let coordss = coords.split(";");
                if (coordss[0] == pointDebut.x) {
                    if (coordss[1] == pointDebut.y) {
                        debutX = this.nodes[count].center().x;
                        debutY = this.nodes[count].center().y;
                    }
                }
                if (coordss[0] == pointFin.x) {
                    if (coordss[1] == pointFin.y) {
                        finX = this.nodes[count].center().x;
                        finY = this.nodes[count].center().y;
                    }
                }
                count++;
            }
            let straightLine = new PIXI.Graphics();
            straightLine.lineStyle(6, color, 1);
            straightLine.moveTo(debutX, debutY);
            straightLine.lineTo(finX, finY);
            this.strLines.push(straightLine);
            this.stage.addChild(straightLine);
        }
    }

    /**
     * Réalise la verification du chemin déssiné
     * @param {json} chemin Le chemin déssiné par l'utilisateur
     * @param {Array} listeBoutons Le chemin déssiné par l'utilisateur
     * @param {boolean} evaluation est un boolean pour savoir si c'est en mode evaluation
     */
    setOnShapeCreated(chemin, listeBoutons, evaluation) {
        // on regarde si c'est la création du chemin dans "créer" ou si c'est le jeu
        if (!this.createWays) {
            /** On réinitialise à 0 en cas ou nous avions déjà utilisé */
            this.nombrePointAPasser = 0;
            this.boutonsAafficher = new Array();
            this.onShapeCreated = chemin;
            this.points.name;
            // si plusieurs points add -> || chemin.debut[2] == this.points[0].name || chemin.debut[3] == this.points[0].name || chemin.debut[4] == this.points[0].name
            if(this.points.length !== 0) {
                if (chemin.debut[1] === this.points[0].name) {
                    if (!evaluation) {
                        this.changecolor(this.points[0], this.points[1], VERT);
                    }
                    let count = 1;
                    let ok = 1;
                    let positionSegment = [];
                    let i = 0;
                    this.dernierPointCorrecte = this.points[1];
                    this.pourcentage = -1;
                    for (let key in chemin.segments) {
                        let segment = chemin.segments[count].split("/");
                        this.nombrePointAPasser++;
                        for (let key in this.points) {
                            if (this.points[i].name == segment[0] || this.points[i].name == segment[1]) {
                                if ((this.points[i + 2].name == segment[1] || this.points[i + 2].name == segment[0]) || (segment[0] === segment[1])) {
                                    if (positionSegment > i) {
                                        ok = -1;
                                        this.dernierPointCorrecte = this.points[i];
                                        break;
                                    } else {
                                        positionSegment.push(i);
                                        this.dernierPointCorrecte = this.points[i + 2];
                                        ok++;
                                    }
                                    break;
                                }
                            }
                            i++;
                        }
                        i = 0;
                        count++;
                        // lorsque c'est en mode entraine pour afficher les corrections
                        if (!evaluation) {
                            if (ok == count) {
                                for (let i = 0; i < this.points.length - 1; i++) {
                                    this.changecolor(this.points[i], this.points[i + 1], VERT);
                                }
                                Swal.fire({
                                    title: this.refGameDG.global.resources.getOtherText('good'),
                                    width: 500,
                                    icon: 'success',
                                    confirmButtonColor: BLEU,
                                    confirmButtonText: 'OK',
                                    showConfirmButton: true,
                                    html: this.refGameDG.global.resources.getOtherText('goodWays')
                                });
                                this.pourcentage = 1;
                            } else if (ok == -1) {
                                this.segmentsok(false);
                                Swal.fire({
                                    title: this.refGameDG.global.resources.getOtherText('errorPopup'),
                                    width: 500,
                                    icon: 'error',
                                    confirmButtonColor: BLEU,
                                    confirmButtonText: 'OK',
                                    showConfirmButton: true,
                                    html: this.refGameDG.global.resources.getOtherText('notAllGoodWays')
                                });
                                this.pourcentage = 0;
                            } else {
                                /*
                                listeBoutons[count - 1].setVisible(true);
                                listeBoutons[count - 1].interactive = false;
                                listeBoutons[count - 1].buttonMode = false;
                                listeBoutons[count - 1].update();
                                 */
                                this.segmentsok(false);
                                Swal.fire({
                                    title: this.refGameDG.global.resources.getOtherText("errorPopup"),
                                    width: 500,
                                    icon: 'error',
                                    confirmButtonColor: BLEU,
                                    confirmButtonText: 'OK',
                                    showConfirmButton: true,
                                    html: this.refGameDG.global.resources.getOtherText("notGoodFinish")
                                });
                                this.pourcentage = 0;
                            }
                            if (ok == count) {
                                // si plusieurs point de départ (3) -> || chemin.fin[2] == this.points[this.points.length - 1].name || chemin.fin[3] == this.points[this.points.length - 1].name || chemin.fin[4] == this.points[this.points.length - 1].name
                                if (chemin.fin[1] == this.points[this.points.length - 1].name) {
                                } else {
                                    /*
                                    listeBoutons[listeBoutons.length - 1].setVisible(true);
                                    listeBoutons[listeBoutons.length - 1].interactive = false;
                                    listeBoutons[listeBoutons.length - 1].buttonMode = false;
                                    listeBoutons[listeBoutons.length - 1].update();
                                     */
                                    this.segmentsok(false);
                                    Swal.fire({
                                        title: this.refGameDG.global.resources.getOtherText("errorPopup"),
                                        width: 500,
                                        icon: 'error',
                                        confirmButtonColor: BLEU,
                                        confirmButtonText: 'OK',
                                        showConfirmButton: true,
                                        html: this.refGameDG.global.resources.getOtherText('goodWays')
                                    });
                                }
                            } else {
                                let passageText;
                                let passage = "";
                                for (let j = 1; j < ok; j++) {
                                    this.pourcentage += (60 / this.nombrePointAPasser);
                                }
                                for (let j = 0; j < ok; j++) {
                                    if (ok !== j + 1) {
                                        passageText = "Ok";
                                    } else {
                                        passageText = "Ko";
                                    }
                                    if ((j + 1) === this.nombrePointAPasser) {
                                        passage += passageText;
                                    } else {
                                        passage += passageText + ";";
                                    }
                                }
                                this.pourcentage = this.pourcentage + "," + passage;
                            }
                        } else {
                            this.pourcentage = 0;
                            if (chemin.fin[1] == this.points[this.points.length - 1].name) this.pourcentage += 20;
                            if (chemin.debut[1] == this.points[0].name) this.pourcentage += 20;
                            let passageText = "";
                            let passage = "";
                            // ok = 3 donc this.nombrePointAPasser + 1
                            if (ok === count) {
                                this.pourcentage = 100;
                                for (let j = 0; j < this.nombrePointAPasser; j++) {
                                    passageText = "Ok";
                                    if ((j + 1) === this.nombrePointAPasser) {
                                        passage += passageText;
                                    } else {
                                        passage += passageText + ";";
                                    }
                                }
                                this.pourcentage = this.pourcentage + "," + passage;
                            } else {
                                for (let j = 1; j < ok; j++) {
                                    this.pourcentage += (60 / this.nombrePointAPasser);
                                }
                                for (let j = 0; j < this.nombrePointAPasser; j++) {
                                    if (ok !== j + 1) {
                                        passageText = "Ok";
                                    } else {
                                        passageText = "Ko";
                                    }
                                    if ((j + 1) === this.nombrePointAPasser) {
                                        passage += passageText;
                                    } else {
                                        passage += passageText + ";";
                                    }
                                }
                                this.pourcentage = this.pourcentage + "," + passage;
                            }
                        }

                    }
                } else {
                    if (!evaluation) {
                        /*
                        for (let i = 0; i < listeBoutons.length; i++) {
                            listeBoutons[i].setVisible(true);
                            listeBoutons[i].interactive = false;
                            listeBoutons[i].buttonMode = false;
                            listeBoutons[i].update();
                        }
                         */

                        for (let j = 0; j < this.points.length - 1; j++) {
                            this.changecolor(this.points[j], this.points[j + 1], ROUGE);
                        }
                        Swal.fire({
                            title: this.refGameDG.global.resources.getOtherText("errorPopup"),
                            width: 500,
                            icon: 'error',
                            confirmButtonColor: BLEU,
                            confirmButtonText: 'OK',
                            showConfirmButton: true,
                            html: this.refGameDG.global.resources.getOtherText("notGoodStart")
                        });
                    } else {
                        this.pourcentage = 0 + ",Ko fs";
                    }
                }
            }
        }
        return this.pourcentage;
    }

    /**
     * Cette methode permet change la couleur des segments. Elle est separeé de la methode change colo
     * @param ok {boolean} qui nous indique si c'est juste ou faux
     */
    segmentsok(ok) {
        if (ok) {
            let changerCouleur = false;
            let dernierPoint = 0;
            for (let i = 0; i < this.points.length - 1; i++) {
                if (this.points[i].name == this.dernierPointCorrecte.name) {
                    changerCouleur = true;
                }
                if (changerCouleur) {
                    this.changecolor(this.points[i], this.points[i + 1], ROUGE);
                    for (let j = 0; j < dernierPoint; j++) {
                        this.changecolor(this.points[j], this.points[j + 1], VERT);
                    }
                } else {
                    dernierPoint++;
                }
            }
            dernierPoint = 0;
        } else {
            let changerCouleur = false;
            for (let i = 0; i < this.points.length - 1; i++) {
                if (this.points[i].name == this.dernierPointCorrecte.name) {
                    changerCouleur = true;
                }
                if (changerCouleur) {
                    this.changecolor(this.points[i], this.points[i + 1], ROUGE);
                }
            }
        }
    }

    /**
     * On modifie la variable modeCreate en fonction de la valeur du boolean
     * @param boolean 'true' on active le mode 'false' on le désactive
     */
    setModeCreate(boolean) {
        this.modeCreate = boolean;
    }

    /**
     * On modifie la variable refGameDG en fonction de la valeur du donnée
     * @param ref est la ref global de l'application pour pouvoir mettre les traductions dans cette partie
     */
    setRefGameDG(ref) {
        this.refGameDG = ref;
    }

    /**
     * On modifie la variable refCreate en fonction de la valeur donnée
     * @param refCreate
     */
    setRefCreate(refCreate) {
        this.refCreate = refCreate;
    }

    /**
     * Méthode qui permet la prise d'information des images/logos ainsi que des coordonnées en question
     * @param coords les coordonnées du point sélectionner
     */
    async actionPopPup(coords) {
        let nodeHold;
        let namePeople = [];

        // On crée des variables pour les personnes
        // MR : Pas besoin de fixer ceci dans le code
        /*         var boy = "boy";
                var businessWoman = "businessWoman";
                var femaleUser = "femaleUser";
                var girl = "girl";
                var standingMan = "standingMan"; */

        for (let i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i]._id === coords[0] + ";" + coords[1]) {
                nodeHold = this.nodes[i];
            }
        }
        // ERROR COME HERE
        this.tableImage = this.getImages();

        // Implémentation toutes les images/logos
        /** Popup avec toutes les images dont nous avons besoin */
        const {value: logoselected} = await Swal.fire({
            title: this.refGameDG.global.resources.getOtherText('selectImage'),
            html: this.addLogo(),
            width: 600,
            focusConfirm: false,
            preConfirm: () => {
                if (document.querySelector("input[name='logo']:checked") !== null) {
                    return [
                        document.querySelector("input[name='logo']:checked").id
                    ]
                } else {
                    return [null]
                }
            }
        });

        // if (logoselected[0] == null) {
        //     Swal.fire({
        //         icon: 'error',
        //         text: this.refGameDG.global.resources.getOtherText('logoSelectOptions')
        //     });
        // }

        if (typeof logoselected === 'undefined') {
            this.resetNoLogoCreate();
        } else if (logoselected[0] !== null) {
            // On vérifie ce que l'utilisateur a sélectionné et on regarde si c'est un personnage ou non
            //Aif(logoselected[0] === boy || logoselected[0] === businessWoman || logoselected[0] === femaleUser || logoselected[0] === girl || logoselected[0] === standingMan){
            let name = [];
            /** On regarde si il y a déjà le start et/ou end actif */
            if (!this.startSetup && !this.endSetup) {
                const {value: nameB} = await Swal.fire({
                    title: this.refGameDG.global.resources.getOtherText("name"),
                    html:
                        '<input id="name" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_FR") + '">' +
                        '<input id="nameDE" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_DE") + '">' +
                        '<input id="nameIT" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_IT") + '">' +
                        '<input id="nameEN" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_EN") + '">' +
                        '<input type="checkbox" id="start">\n' +
                        '<label for="start">' + this.refGameDG.global.resources.getOtherText("useStart") + '</label><br>' +
                        '<input type="checkbox" id="end">\n' +
                        '<label for="end">' + this.refGameDG.global.resources.getOtherText("useEnd") + '</label><br>'
                    ,
                    focusConfirm: false,
                    showCancelButton: true,
                    preConfirm: () => {
                        return [
                            document.getElementById("name").value,
                            document.getElementById("nameDE").value,
                            document.getElementById("nameIT").value,
                            document.getElementById("nameEN").value,
                            document.getElementById("start").checked,
                            document.getElementById("end").checked
                        ]
                    }
                });

                /** On vérifie que l'élève n'a pas cliqué sur les start et end */
                if (name[1] && name[2]) {
                    Swal.fire({
                        icon: 'error',
                        text: this.refGameDG.global.resources.getOtherText('optionsStartEndDefined')
                    });

                } else {
                    name = nameB;
                    this.resetNoLogoCreate();
                }

                /** Si le start est déjà définit on autorise que le end */
            } else if (this.startSetup && !this.endSetup) {
                const {value: nameB} = await Swal.fire({
                    title: this.refGameDG.global.resources.getOtherText("name"),
                    html:
                        '<input id="name" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_FR") + '">' +
                        '<input id="nameDE" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_DE") + '">' +
                        '<input id="nameIT" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_IT") + '">' +
                        '<input id="nameEN" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_EN") + '">' +
                        '<input type="checkbox" id="end">\n' +
                        '<label for="end">' + this.refGameDG.global.resources.getOtherText("useEnd") + '</label><br>'
                    ,
                    focusConfirm: false,
                    showCancelButton: true,
                    preConfirm: () => {
                        return [
                            document.getElementById("name").value,
                            document.getElementById("nameDE").value,
                            document.getElementById("nameIT").value,
                            document.getElementById("nameEN").value,
                            false,
                            document.getElementById("end").checked
                        ]
                    }
                });
                nameB[1] = false;
                name = nameB;

                /** Si le end est déjà définit on autorise que le start */
            } else if (this.endSetup && !this.startSetup) {
                const {value: nameB} = await Swal.fire({
                    title: this.refGameDG.global.resources.getOtherText("namePNJ"),
                    html:
                        '<input id="name" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_FR") + '">' +
                        '<input id="nameDE" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_DE") + '">' +
                        '<input id="nameIT" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_IT") + '">' +
                        '<input id="nameEN" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_EN") + '">' +
                        '<input type="checkbox" id="start">\n' +
                        '<label for="end">' + this.refGameDG.global.resources.getOtherText("useStart") + '</label><br>'
                    ,
                    focusConfirm: false,
                    showCancelButton: true,
                    preConfirm: () => {
                        return [
                            document.getElementById("name").value,
                            document.getElementById("nameDE").value,
                            document.getElementById("nameIT").value,
                            document.getElementById("nameEN").value,
                            document.getElementById("start").checked,
                            false
                        ]
                    }
                });
                name = nameB;

                /** On autorise l'élève à mettre le nom de l'objet en question */
            } else {
                const {value: nameB} = await Swal.fire({
                    title: this.refGameDG.global.resources.getOtherText("name"),
                    html:
                        '<input id="name" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_FR") + '">' +
                        '<input id="nameDE" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_DE") + '">' +
                        '<input id="nameIT" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_IT") + '">' +
                        '<input id="nameEN" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_EN") + '">'
                    ,
                    focusConfirm: false,
                    showCancelButton: true,
                    preConfirm: () => {
                        return [
                            document.getElementById("name").value,
                            document.getElementById("nameDE").value,
                            document.getElementById("nameIT").value,
                            document.getElementById("nameEN").value,
                            false,
                            false
                        ]
                    }
                });
                name = nameB;
            }
            /** on vérifie qu'il a bien rempli l'endroit */
            if (typeof name !== 'undefined') {
                if (name[0] === "" && name[4] || name[0] === "" && name[5]) {
                    Swal.fire({
                        icon: 'error',
                        text: this.refGameDG.global.resources.getOtherText('nameEmpty')
                    });
                    // } else if (name[4] && name[5]) {
                    //     Swal.fire({
                    //         icon: 'error',
                    //         text: this.refGameDG.global.resources.getOtherText('impossibleStartEnd')
                    //     });
                } else {
                    if (name[4] && name[5]) {
                        if (!this.startSetup && !this.endSetup) {
                            this.startSetup = true;
                            this.endSetup = true;
                            this.pointStartEnd.push((coords[0] * 2 + 1) + ";" + (coords[1] * 2 + 1));
                            namePeople[0] = name[0];
                            namePeople[1] = name[0];
                            namePeople[2] = name[0];
                            namePeople[3] = name[0];
                            // this.affichageImage(logoselected[0], nodeHold, namePeople, name[1], name[2]);
                            this.affichageImage(logoselected[0], nodeHold, namePeople, name[4], name[5]);
                        } else {
                            Swal.fire({
                                icon: 'error',
                                text: this.refGameDG.global.resources.getOtherText('optionsStartEnable')
                            });
                        }
                    } else if (name[4] && !name[5]) {
                        if (!this.startSetup) {
                            this.startSetup = true;
                            this.pointStartEnd.push((coords[0] * 2 + 1) + ";" + (coords[1] * 2 + 1));
                            namePeople[0] = name[0];
                            namePeople[1] = name[0];
                            namePeople[2] = name[0];
                            namePeople[3] = name[0];
                            // this.affichageImage(logoselected[0], nodeHold, namePeople, name[1], name[2]);
                            this.affichageImage(logoselected[0], nodeHold, namePeople, name[4], name[5]);
                        } else {
                            Swal.fire({
                                icon: 'error',
                                text: this.refGameDG.global.resources.getOtherText('optionsStartEnable')
                            });
                        }
                    } else if (name[5] && !name[4]) {
                        if (!this.endSetup) {
                            this.endSetup = true;
                            this.pointStartEnd.push((coords[0] * 2 + 1) + ";" + (coords[1] * 2 + 1));
                            namePeople[0] = name[0];
                            namePeople[1] = name[0];
                            namePeople[2] = name[0];
                            namePeople[3] = name[0];
                            // this.affichageImage(logoselected[0], nodeHold, namePeople, name[1], name[2]);
                            this.affichageImage(logoselected[0], nodeHold, namePeople, name[4], name[5]);
                        } else {
                            Swal.fire({
                                icon: 'error',
                                text: this.refGameDG.global.resources.getOtherText('optionsStartEnable')
                            });
                        }
                    } else if (!name[5] && !name[4]) {
                        namePeople[0] = name[0];
                        namePeople[1] = name[0];
                        namePeople[2] = name[0];
                        namePeople[3] = name[0];
                        // this.affichageImage(logoselected[0], nodeHold, namePeople, name[1], name[2]);
                        this.affichageImage(logoselected[0], nodeHold, namePeople, name[4], name[5]);
                    }
                }
                this.resetNoLogoCreate();
            } else {
                this.resetNoLogoCreate();
            }
        }

        // implémentation de la partie images/logos
        document.getElementById("swal2-content").innerHTML += '<div style="padding: 30px">' +
            '<table>' +
            '<tbody>' +
            '<tr>' +
            '<div class="center">' +
            '<div class="buttonsLogo"></div>' +
            '</br>' +
            '</br>' +
            '</div>' +
            '</tr>' +
            '</tbody>' +
            '</table>' +
            '</div>';
    }

    /**
     * Permet d'ajouter les images depuis la popup
     */
    addLogo() {
        // document.getElementById("swal2-content").style.display = "block";
        var nbreImageLigne = 5;
        var div = '<div style="overflow:scroll; overflow-x: hidden;  display: block; width: 100%; height:500px; border:#000000 1px solid;">';
        var nameImage = "";
        let nbreImage = 0;
        let imgCompar;
        var arrayImage = [];
        // image où nous devons rien faire (image inutile pour ici)
        let son = this.tableImage.son.image;
        let defaultSprite = this.tableImage.default_sprite_error_framework.image;
        let speaker = this.tableImage.speaker.image;
        let background = this.tableImage.background.image;
        let backgroundValidation = this.tableImage.backgroundValidation.image;
        let carreStart = this.tableImage.carreStart.image;
        let carreEnd = this.tableImage.carreEnd.image;
        let segmentHorizon = this.tableImage.segmentHorizon.image;
        let segmentVertical = this.tableImage.segmentVertical.image;
        let backgroundCreate = this.tableImage.backgroundCreate.image;
        const organisedItems = []
        for (let i = 0; i < Object.keys(this.tableImage).length; i++) {
            imgCompar = Object.values(this.tableImage)[i].image;
            if ((imgCompar !== son) && (imgCompar !== defaultSprite) && (imgCompar !== speaker) && (imgCompar !== background) && (imgCompar !== backgroundValidation) && (imgCompar !== carreStart) && (imgCompar !== carreEnd) && (imgCompar !== segmentHorizon) && (imgCompar !== segmentVertical) && (imgCompar !== backgroundCreate)){
                if (this.refGameDG.global.resources.getOtherText(Object.keys(this.tableImage)[i]) !== '404 - Text Not Found') {
                    organisedItems.push(
                        {
                            'trad': this.refGameDG.global.resources.getOtherText(Object.keys(this.tableImage)[i]),
                            'key': Object.keys(this.tableImage)[i],
                            'value': Object.values(this.tableImage)[i]
                        })
                }
            }
        }
        organisedItems.sort((a, b) => a.trad.localeCompare(b.trad))

        for (let i = 0; i < organisedItems.length; i++) {
            // On récupère le nom de l'image
                nameImage = Object.keys(this.tableImage)[i];
                var trad = this.refGameDG.global.resources.getOtherText(nameImage);
                arrayImage.push(nameImage);
                // On divise toutes les images par 8 pour avoir 8 logo par pages

                if ((nbreImage % nbreImageLigne) === 0) {
                    div += '<div style="display: flex;"><div style="display: inline;width:25%;height:80px"><img src="' + organisedItems[i].value.image.currentSrc + ' " /></br><span>' + organisedItems[i].trad + '</span></br><input type="radio" name="logo" id="' + organisedItems[i].key + '"></div>';
                } else if ((nbreImage % nbreImageLigne) === (nbreImageLigne - 1)) {
                    div += '<div><img src="' + organisedItems[i].value.image.currentSrc + '"/></br><span>' + organisedItems[i].trad + '</span></br><input type="radio" name="logo" id="' + organisedItems[i].key + '"></div></div></br>';
                } else {
                    div += '<div><img src="' + organisedItems[i].value.image.currentSrc + '"/></br><span>' + organisedItems[i].trad + '</span></br><input type="radio" name="logo" id="' + organisedItems[i].key + '"></div>';
                }
                nbreImage++;
        }
        // on ferme les div's
        div += '</div></div>';
        // on modifie dans la popup pour l'affichage
        // document.getElementsByClassName("buttonsLogo")[0].innerHTML = div;
        return div;
    }

    /**
     * Cette méthode retourne toutes les images de la db
     */
    getImages() {
        this.tableImage = this.refGameDG.global.resources.IMAGES;
        return this.tableImage;
    }

    /**
     * Cette méthode retourne les tradImages
     */
    getTradImage() {
        return this.tradImage;
    }

    /**
     * Cette méthode permet de modifier si les points sont visible (dans Node.js)
     * @param actif est l'état, si c'est visible ou non
     */
    setVisiblePoint(actif) {
        for (let i = 0; i < this.nodes.length; i++) {
            this.nodes[i].setVisiblePoint(actif);
        }
    }

    /**
     * Change la valeur pour ne plus afficher les corrections
     */
    setVisibleCorrectWays(actif) {
        this.visibleWay = actif
    }

    /**
     * Permet de récupérer le nombre de point à passer
     * @returns {Number}
     */
    getNombrePointAPasser() {
        return this.nombrePointAPasser;
    }

    /**
     * Permet de modifier la valeur du nombrePointAPasser en fonction du nbre donnée
     * @param nombrePointAPasser
     */
    setNombrePointAPasser(nombrePointAPasser) {
        this.nombrePointAPasser = nombrePointAPasser;
    }

    /**
     * Cette méthode modifie la variable de "createWays"
     * @param boolean
     */
    setCreateWays(boolean) {
        this.createWays = boolean;
    }

    /**
     * Méthode qui retourne un tableau de "Point"
     * @returns {Point[]}
     */
    getPoint() {
        return this.points;
    }

    /**
     * Retourne le tableau pointStartEnd
     * @returns {[]|*[]}
     */
    getPointStartEnd() {
        return this.pointStartEnd;
    }

    /**
     * Méthode qui permet de modifier la valeur de startSetup
     * @param boolean valeur pour modifier
     */
    setStartSetup(boolean) {
        this.startSetup = boolean;
    }

    /**
     * Méthode qui permet de modifier la valeur de endSetup
     * @param boolean valeur pour modifier
     */
    setEndSetup(boolean) {
        this.endSetup = boolean
    }
}
class ImgBack extends Asset {

    constructor(x, y, width, height, imgBackground, name, onDBClick) {
        super();
        this.y = y;
        this.x = x;
        this.width = width;
        this.height = height;
        this.imgBackground = imgBackground;
        this.name = name;
        this.onDBClick = onDBClick;
        this.isFirstClick = true;

        this.container = new PIXI.Container();
        this.imgStatic = undefined;
        this.init();
    }

    init() {
        this.container.removeChildren();
        this.imgStatic = PIXI.Sprite.fromImage(this.imgBackground);
        this.imgStatic.anchor.x = 0.5;
        this.imgStatic.anchor.y = 0.5;
        this.imgStatic.x = this.x;
        this.imgStatic.y = this.y;
        this.imgStatic.width = this.width;
        this.imgStatic.height = this.height;

        this.imgStatic.interactive = true;
        this.imgStatic.on('pointerdown', this.onClick.bind(this));

        this.container.addChild(this.imgStatic);
    }

    onClick() {
        if (this.isFirstClick) {
            this.isFirstClick = false;
            setTimeout(function () {
                this.isFirstClick = true;
            }.bind(this), 500)
        } else {
            this.isFirstClick = true;
            if (this.onDBClick) {
                this.onDBClick(this);
            }
        }
    }

    getPixiChildren() {
        return [this.container];
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.y = y;
    }

    setX(x) {
        this.x = x;
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

}
class ImgStatic extends Asset {

    constructor(x, y, width, imgBackground, text, touchable, textSound) {
        super();
        this.y = y;
        this.x = x;
        this.width = width;
        this.height = width;
        this.imgBackground = imgBackground;
        this.text = text;
        this.start = false;
        this.end = false;
        this.isFirstClick = true;
        this.onClick = function() {
           if(this.modeCreate){
               Swal.fire({
                   title: this.refTrad.global.resources.getOtherText("deleteLogo"),
                   icon: 'warning',
                   showCancelButton: true,
                   confirmButtonColor: '#3085d6',
                   cancelButtonColor: '#d33',
                   confirmButtonText: this.refTrad.global.resources.getOtherText("yes"),
                   cancelButtonText: this.refTrad.global.resources.getOtherText("no")
               }).then((result) => {
                   // if (result.dismiss !== "cancel") {
                   if (result.value) { // on supprime l'objet
                       this.refCreate.takeOffLogo(this);
                       if (this.getStart() === true){
                           this.refDG.startSetup = false;
                       }
                       if (this.getEnd() === true){
                           this.refDG.endSetup = false;
                       }
                   }
               });
           } else {
               this.popUp(textSound);
           }
        };
        this.container = new PIXI.Container();
        this.imgStatic = undefined;
        this.touchable = touchable;
        this.init();
    }
    init() {
        this.container.removeChildren();
        // Sprite permet de rendre l'objet manipulable
        this.imgStatic = PIXI.Sprite.fromImage(this.imgBackground);
        this.imgStatic.anchor.x = 0.5;
        this.imgStatic.anchor.y = 0.3;
        this.imgStatic.x = this.x;
        this.imgStatic.y = this.y;
        this.imgStatic.width = this.width;
        this.imgStatic.height = this.height;
        let textAecrire = new PIXI.Text(this.text,{fontFamily: "\"Arial\", cursive, sans-serif", fontVariant: "small-caps", fontWeight: 600, fontSize: 14, fill : 0x000000, align : 'center'});
        textAecrire.x = this.x;
        textAecrire.y = this.y;
        textAecrire.anchor.x = 0.5;
        if(this.width === 60){
            textAecrire.anchor.y = -1;
        }else if (this.width === 40) {
            textAecrire.anchor.y = -1;
        }else{
            textAecrire.anchor.y = -3;
        }
        if(this.touchable){
            this.imgStatic.interactive = true;
            this.imgStatic.on('pointerdown', function () {this.onClick()}.bind(this));
        } else {
            this.imgStatic.interactive = false;
        }
        this.container.addChild(this.imgStatic, textAecrire);
    }

    setTouchable(boolean){
        this.imgStatic.interactive = !boolean;
    }

    setNameLogo(nameLogo){
        this.name = nameLogo;
    }

    setModeCreate(boolean){
        this.modeCreate = boolean;
    }

    setRefDG(ref){
        this.refDG = ref;
    }

    setRefTrad(ref){
        this.refTrad = ref;
    }

    setRefCreate(ref){
        this.refCreate = ref;
    }

    setOnClick(onClick) {
        this.onClick = onClick;
    }

    update(y){
        this.y = y;
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.imgStatic.y = y;
        this.update();
    }

    setX(x) {
        this.imgStatic.x = x;
        this.update();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    /**
     * Méthode vide pour un clique sans action
     */
    nothing(){}

    /**
     * Popup qui apparait quand on clique sur le hautparleur du dans le canvas
     * @param texte
     */
    popUp(texte) {
        Swal.fire({
            title: name.toUpperCase(),
            width: 1000,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td><div id="activites" style="text-align: center; padding-left : 40px">' + texte + '</div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    setStart(boolean){
        this.start = boolean;
    }
    getStart(){
        return this.start;
    }

    setEnd(boolean){
        this.end = boolean;
    }

    getEnd(){
        return this.end;
    }
}
/**
 * @classdesc Bean node. Noeud qui compose une grille de dessin
 * @author Vincent Audergon
 * @version 1.0
 */
class Node extends Asset{

    getY() {
       // super.getY();
        return this.y;
    }

    getX() {
       // super.getX();
        return this.x;
    }

    getWidth() {
        // super.getWidth();
        return this.width;
    }

    /**
     * Constructeur de Node
     * @param {double} x La position x du noeud
     * @param {double} y La position y du noeud
     * @param {double} width La largeur et hauteur du noeud
     * @param {DrawingGrid} refGrid La référence vers la {@link DrawingGrid} (grille de dessin)
     */
    constructor(x, y, width, refGrid,id ) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.refGrid = refGrid;
        this.init();
        this._id = id;
    }

    /**
     * Initialise le noeud
     */
    init() {
      this.graphics = new PIXI.Graphics();
      this.graphics.interactive = true;
      this.graphics.buttonMode = true;
      // 0xFFFFFF
      this.graphics.beginFill(0xFFFFFF);
      this.graphics.drawRect(this.x -5 ,this.y -5, this.width+40,this.width+40);
      this.graphics.endFill(0xFFFFFF);
      // Enlever renderable et mettre visible en cas de besoin
      this.graphics.renderable =false;
      //this.graphics.visible = true;
      this.graphics.on('pointerdown', this.onNodeClicked.bind(this));

        this.point = new PIXI.Graphics();
        this.point.beginFill(0x000000);
        this.point.drawRect(this.x+10, this.y+10, this.width-10, this.width-10);
        this.point.endFill(0x000000);
        this.point.visible = true;

        this.point.interactive = true;
        this.point.buttonMode = true;
        this.point.on('pointerdown', this.onNodeClicked.bind(this));
    }

    /**
     * Modifier la valeur visible
     */
    setVisiblePoint (actif){
        if(!actif){
            this.point.visible = true;
        } else {
            this.point.visible = false;
        }
    }

    /**
     * Retourne les coordonées du centre du noeud
     * @return {Point} le centre du noeud
     */
    center() {
        return new Point(this.x+5 + this.width / 2, this.y+5 + this.width / 2);
    }

    /**
     * Méthode de callback appelée lorsqu'un click est détecté sur le noeud
     * @param {Event} e L'événement JavaScript
     */
    onNodeClicked(e) {
        this.refGrid.onNodeClicked(e, this);
    }

    /**
     * Méthode de callback appelée lorsque le click est enfoncé
     * et que le curseur passe au dessus du noeud
     * @deprecated
     * @param {Event} e L'événement JavaScript
     */
    onNodeEncountered(e) {
        this.refGrid.onNodeEncountered(e, this)
    }

    getid() {
        return this._id;
    }
}
class Personnage extends Asset {
    constructor(x, y, width, height, imgBackground, name, onMove, onDBClick) {
        super();
        this.y = y;
        this.x = x;
        this.width = width;
        this.height = height;
        this.imgBackground = imgBackground;
        this.name = name;
        this.onMove = onMove;
        this.onDBClick = onDBClick;
        this.container = new PIXI.Container();
        this.isFirstClick = true;
        this.data = undefined;
        this.personne = undefined;
        this.init();
    }

    init() {
        this.container.removeChildren();
        this.personne = PIXI.Sprite.fromImage(this.imgBackground);
        this.personne.anchor.x = 0.5;
        this.personne.anchor.y = 0.5;
        this.personne.x = this.x;
        this.personne.y = this.y;
        this.personne.width = this.width;
        this.personne.height = this.height;

        this.personne.interactive = true;
        this.personne.on('pointerdown', this.onClick.bind(this));
        this.personne.on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));
        this.container.addChild(this.personne);
    }

    onClick() {
        if (this.isFirstClick) {
            this.isFirstClick = false;
            setTimeout(function () {
                this.isFirstClick = true;
            }.bind(this), 500)
        } else {
            this.isFirstClick = true;
            if (this.onDBClick) {
                this.onDBClick(this);
            }
        }
    }

    onDragStart(event) {
        this.data = event.data;
    }

    onDragEnd() {
        this.data = null;
    }

    onDragMove() {
        if (this.data) {
            let newPos = this.data.getLocalPosition(this.personne.parent);

            newPos.x = newPos.x < this.height/2 ? this.height/2 : newPos.x;
            newPos.x = newPos.x > (600-this.height/2) ? (600-this.height/2) : newPos.x;
            newPos.y = newPos.y < this.width/2 ? this.width/2 : newPos.y;
            newPos.y = newPos.y > (600-this.width/2) ? (600-this.width/2) : newPos.y;
            this.personne.x = newPos.x;
            this.personne.y = newPos.y;

            if(this.onMove){
                this.onMove(this);
            }
        }
    }

    getPixiChildren() {
        return [this.container];
    }

    getX() {
        return this.x;
    }

    getY() {
        return this.y;
    }

    setX() {
        this.x = x;
        this.init();
    }

    setY() {
        this.y = y;
        this.init();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible() {
        this.container.visible = visible;
    }


}
/**
 * Original Code from https://github.com/SebastianNette/PIXI.Input
 * (PIXI Input Element v1.0.1, Copyright (c) 2014, Sebastian Nette, http://www.mokgames.com/)
 *
 * Substantial changes have been made to support Chinese/Japanese/Korean IME inputs and mobile devices,
 * by overlaying html input elemnets over canvas.
 */
(function(undefined) {
	
	"use strict";

	/* -------------------------------------------------------------------------------------------------------------------------- */
	// fundamental functions

	// check for object
	function isObject(obj)
	{
		return typeof obj === "object" && !!obj && !(obj instanceof Array);
	};

	// mixin
	function extend(dest, source, force)
	{
		for (var prop in source)
		{
			if (force)
			{
				dest[prop] = source[prop];
				continue;
			}
			var isObj = isObject(source[prop]);
			if (!dest.hasOwnProperty(prop))
			{
				dest[prop] = isObj ? {} : source[prop];
			}
			if (isObj)
			{
				if (!isObject(dest[prop]))
				{
					dest[prop] = {};
				}
				extend(dest[prop], source[prop]);
			}
		}
		return dest;
	};

	// get scrollbar width ; ref: https://stackoverflow.com/a/13382873
	function getScrollbarWidth()
	{
		var outer = document.createElement("div");
		outer.style.visibility = "hidden";
		outer.style.width = "100px";
		outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

		var inner = document.createElement("div");
		inner.style.width = "100%";

		document.body.appendChild(outer);
		var widthNoScroll = outer.offsetWidth;

		outer.style.overflow = "scroll";
		outer.appendChild(inner);
		var widthWithScroll = inner.offsetWidth;

		outer.parentNode.removeChild(outer);

		return widthNoScroll - widthWithScroll;
	};
	
	function getPropertyDescriptor(proto, propName)
	{
		while (proto)
		{
			var propDesc = Object.getOwnPropertyDescriptor(proto, propName);
			if (propDesc) return propDesc;
			proto = proto.__proto__;
		}
		return undefined;
	}

	/* -------------------------------------------------------------------------------------------------------------------------- */
	// functions for input field texture

	// parse shadow expression
	function parseShadowExpr(opts, isBoxShadow)
	{
		var shadow = isBoxShadow ? opts.boxShadow : opts.innerShadow;

		// parse shadow
		if (shadow && shadow !== "none")
		{
			var vals = shadow.split("px ");
			shadow = {
				x: parseInt(vals[0], 10),
				y: parseInt(vals[1], 10),
				blur: parseInt(vals[2], 10),
				color: vals[3]
			};
		}
		else
		{
			shadow = { x: 0, y: 0, blur: 0, color: "" };
		}

		// extra for box shadow
		if (isBoxShadow)
		{
			opts.shadowLeft = ((shadow.blur - shadow.x) > 0) ? (shadow.blur - shadow.x) : 0;
			opts.shadowRight = ((shadow.blur + shadow.x) > 0) ? (shadow.blur + shadow.x) : 0;
			opts.shadowTop = ((shadow.blur - shadow.y) > 0) ? (shadow.blur - shadow.y) : 0;
			opts.shadowBottom = ((shadow.blur + shadow.y) > 0) ? (shadow.blur + shadow.y) : 0;
			opts.shadowWidth = opts.shadowLeft + opts.shadowRight;
			opts.shadowHeight = opts.shadowTop + opts.shadowBottom;
		}

		return shadow;
	};
	
	// draw rounded rect
	function drawRoundedRect(ctx, x, y, w, h, r)
	{
		var r1, r2, r3, r4;
		if (r instanceof Array)
		{
			r1 = r[0];
			r2 = r[1];
			r3 = r[2];
			r4 = r[3];
		}
		else
		{
			r1 = r;
			r2 = r;
			r3 = r;
			r4 = r;
		}

		if (w < 2 * r1 || h < 2 * r1)
		{
			return drawRoundedRect(ctx, x, y, w || 1, h || 1, 0);
		}

		ctx.beginPath();

		ctx.moveTo(x + r1, y);
		ctx.lineTo(x + w - r2, y);
		ctx.quadraticCurveTo(x + w, y, x + w, y + r2);
		ctx.lineTo(x + w, y + h - r3);
		ctx.quadraticCurveTo(x + w, y + h, x + w - r3, y + h);
		ctx.lineTo(x + r4, y + h);
		ctx.quadraticCurveTo(x, y + h, x, y + h - r4);
		ctx.lineTo(x, y + r1);
		ctx.quadraticCurveTo(x, y, x + r1, y);

		ctx.closePath();
	};

	// generate input field texture
	var shadowCanvas = document.createElement("canvas");
	var shadowContext = shadowCanvas.getContext("2d");
	function drawInputFieldTexture(can, ctx, data)
	{
		// check if box shadow was parsed
		if (!isObject(data.boxShadowData))
		{
			data.boxShadowData = parseShadowExpr(data, true);
		}

		// check if inner shadow was parsed
		if (!isObject(data.innerShadowData))
		{
			data.innerShadowData = parseShadowExpr(data, false);
		}

		// set variables
		var width = data.width = data.width || 100;
		var height = data.height = data.height || 30;
		var padding = data.padding = data.padding || 0;

		var borderRadius = data.borderRadius = data.borderRadius || 0;
		var borderWidth = data.borderWidth = data.borderWidth || 0;

		var shadowTop = data.shadowTop = data.shadowTop || 0;
		var shadowLeft = data.shadowLeft = data.shadowLeft || 0;
		var shadowWidth = data.shadowWidth = data.shadowWidth || 0;
		var shadowHeight = data.shadowHeight = data.shadowHeight || 0;

		var textboxTop = data.textboxTop = shadowTop + borderWidth;
		var textboxLeft = data.textboxLeft = shadowLeft + borderWidth;
		var textboxWidth = data.textboxWidth = width + padding * 2;
		var textboxHeight = data.textboxHeight = height + padding * 2;

		var outerWidth = data.outerWidth = width + padding * 2 + borderWidth * 2 + shadowWidth;
		var outerHeight = data.outerHeight = height + padding * 2 + borderWidth * 2 + shadowHeight;

		// support for resolution
		width *= PIXI.settings.RESOLUTION;
		height *= PIXI.settings.RESOLUTION;
		padding *= PIXI.settings.RESOLUTION;
		borderRadius *= PIXI.settings.RESOLUTION;
		borderWidth *= PIXI.settings.RESOLUTION;
		textboxTop *= PIXI.settings.RESOLUTION;
		textboxLeft *= PIXI.settings.RESOLUTION;
		textboxWidth *= PIXI.settings.RESOLUTION;
		textboxHeight *= PIXI.settings.RESOLUTION;
		outerWidth *= PIXI.settings.RESOLUTION;
		outerHeight *= PIXI.settings.RESOLUTION;

		// set dimensions
		can.width = outerWidth;
		can.height = outerHeight;

		// setup the box shadow
		ctx.shadowOffsetX = data.boxShadowData.x;
		ctx.shadowOffsetY = data.boxShadowData.y;
		ctx.shadowBlur = data.boxShadowData.blur;
		ctx.shadowColor = data.boxShadowData.color;

		// draw the border
		if (borderWidth > 0)
		{
			ctx.fillStyle = data.borderColor;
			drawRoundedRect(ctx, shadowLeft, shadowTop, outerWidth - shadowWidth, outerHeight - shadowHeight, borderRadius);
			ctx.fill();

			ctx.shadowOffsetX = 0;
			ctx.shadowOffsetY = 0;
			ctx.shadowBlur = 0;
		}

		// draw bg color
		ctx.fillStyle = data.backgroundColor;
		drawRoundedRect(ctx, textboxLeft, textboxTop, textboxWidth, textboxHeight, borderRadius);
		ctx.fill();

		ctx.shadowOffsetX = 0;
		ctx.shadowOffsetY = 0;
		ctx.shadowBlur = 0;

		// draw inner shadow
		var innerShadowData = data.innerShadowData;
		if (innerShadowData.blur > 0)
		{
			shadowContext.clearRect(0, 0, shadowCanvas.width, shadowCanvas.height);
			
			shadowCanvas.width = textboxWidth;
			shadowCanvas.height = textboxHeight;

			shadowContext.shadowBlur = innerShadowData.blur;
			shadowContext.shadowColor = innerShadowData.color;

			// top shadow
			shadowContext.shadowOffsetX = 0;
			shadowContext.shadowOffsetY = innerShadowData.y;
			shadowContext.fillRect(-1 * outerWidth, -100, 3 * outerWidth, 100);

			// right shadow
			shadowContext.shadowOffsetX = innerShadowData.x;
			shadowContext.shadowOffsetY = 0;
			shadowContext.fillRect(shadowCanvas.width, -1 * outerHeight, 100, 3 * outerHeight);

			// bottom shadow
			shadowContext.shadowOffsetX = 0;
			shadowContext.shadowOffsetY = innerShadowData.y;
			shadowContext.fillRect(-1 * outerWidth, shadowCanvas.height, 3 * outerWidth, 100);

			// left shadow
			shadowContext.shadowOffsetX = innerShadowData.x;
			shadowContext.shadowOffsetY = 0;
			shadowContext.fillRect(-100, -1 * outerHeight, 100, 3 * outerHeight);

			// create a clipping mask on the main canvas
			drawRoundedRect(ctx, textboxLeft, textboxTop, textboxWidth, textboxHeight, borderRadius);
			ctx.clip();

			// draw the inner-shadow from the off-DOM canvas
			ctx.drawImage(
				shadowCanvas,
				0, 0, shadowCanvas.width, shadowCanvas.height,
				textboxLeft, textboxTop, textboxWidth, textboxHeight
			);
		}
	};

	/* -------------------------------------------------------------------------------------------------------------------------- */
	// functions for input element
	
	// create input element
	function createInputElement(isTextarea)
	{
		// create html input element
		var input = document.createElement(isTextarea ? "textarea" : "input");
		if (!isTextarea) {
			input.type = "text";
		}
		input.tabindex = -1;
		input.style.position = "absolute";
		input.style.borderStyle = "solid";
		input.style.display = "none";
		if (isTextarea) {
			input.style.resize = "none";
			input.style.overflowY = "scroll";
		}
		
		// add event handlers
		input.addEventListener("blur", function(e)
		{
			if (PIXI.InputObject.currentInput)
			{
				e = e || window.event;
				PIXI.InputObject.currentInput.blur();
			}
		}, false);

		input.addEventListener("input", function(e)
		{
			if (PIXI.InputObject.currentInput)
			{
				e = e || window.event;
				PIXI.InputObject.currentInput.onInput(e);
			}
		});
		
		// workaround window event handlers (add only once)
		if (!PIXI.InputObject.windowEventHandlersInited)
		{
			window.addEventListener("mousedown", function(e)
			{
				if (PIXI.InputObject.currentInput)
				{
					if (!PIXI.InputObject.currentInput.mouseDownOnMe)
					{
						if (e.target != PIXI.InputObject.currentInput.inputElement)
						{
							PIXI.InputObject.currentInput.mouseDownOnMe = false;
							PIXI.InputObject.currentInput.blur();
						}
					}
				}
			});
			window.addEventListener("mouseup", function(e)
			{
				if (PIXI.InputObject.currentInput)
				{
					if (PIXI.InputObject.currentInput.mouseDownOnMe)
					{
						PIXI.InputObject.currentInput.mouseDownOnMe = false;
					}
				}
			});
			window.addEventListener("resize", function(e){
				if (PIXI.InputObject.currentInput)
				{
					PIXI.InputObject.currentInput.setInputElementPosition();
				}
			});
			PIXI.InputObject.windowEventHandlersInited = true;
		}
		
		document.body.appendChild(input);

		return input;
	};
	
	// update input element
	var styleKeys = [ "width", "height", "padding", "borderColor", "borderWidth", "borderRadius", "backgroundColor", "boxShadow", "innerShadow", "outline" ];
	function updateInputElement(input, data)
	{
		// apply styles
		input.style.boxShadow = "";
		for (var i = 0; i < styleKeys.length; i++)
		{
			var key = styleKeys[i];
			var value = data[key];
			if (typeof value !== "undefined")
			{
				if (typeof value === "number")
				{
					value += "px";
				}
				if (key === "boxShadow" || key === "innerShadow")
				{
					if (key === "innerShadow")
					{
						value += " inset";
					}
					if (input.style.boxShadow)
					{
						input.style.boxShadow += ", " + value;
					}
					else
					{
						input.style.boxShadow = value;
					}
				}
				else
				{
					input.style[key] = value;
				}
			}
		}
		
		if (typeof data.text !== "undefined")
		{
			if (typeof data.text.font !== "undefined")
			{
				input.style.font = data.text.font;
			}
			if (typeof data.text.fill !== "undefined")
			{
				input.style.color = data.text.fill;
			}
			if (typeof data.text.align !== "undefined")
			{
				input.style.textAlign = data.text.align;
			}
			if (typeof data.text.lineHeight !== "undefined")
			{
				if (data.isTextarea)
				{
					input.style.lineHeight = data.text.lineHeight + "px";
				}
			}
		}
		
		// apply attributes
		if (!data.isTextarea)
		{
			input.type = data.type;
		}
		input.value = data.value;
		input.placeholder = data.placeholder;
		input.readOnly = data.readonly;
	};

	/* -------------------------------------------------------------------------------------------------------------------------- */
	// core InputObject class

	PIXI.InputObject = function(data)
	{
		this.data = data;

		// call super constructor
		PIXI.Sprite.call(this, PIXI.Texture.EMTPY);

		// create bg sprite
		this.canvas = document.createElement("canvas");
		this.context = this.canvas.getContext("2d");
		this.bgSprite = new PIXI.Sprite(PIXI.Texture.fromCanvas(this.canvas));
		this.addChild(this.bgSprite);

		// create input element
		if (!this.data.isTextarea)
		{
			if (!PIXI.InputObject.inputElement)
			{
				PIXI.InputObject.inputElement = createInputElement(false);
			}
			this.inputElement = PIXI.InputObject.inputElement;
		}
		else
		{
			if (!PIXI.InputObject.textareaElement)
			{
				PIXI.InputObject.textareaElement = createInputElement(true);
			}
			this.inputElement = PIXI.InputObject.textareaElement;
		}
		
		// get scrollbar width
		if (!PIXI.InputObject.scrollbarWidth)
		{
			PIXI.InputObject.scrollbarWidth = getScrollbarWidth();
		}

		// create text
		this.text = new PIXI.Text("", this.data.text);
		if (this.data.isTextarea)
		{
			this.text.style.wordWrap = true;
		}
		this.addChild(this.text);

		if (!this.data.height)
		{
			this.data.height = parseInt(PIXI.TextMetrics.measureFont(this.text.style.font).fontSize, 10);
		}

		// create text mask
		this.textMask = new PIXI.Graphics();
		this.addChild(this.textMask);
		this.text.mask = this.textMask;

		// variables for use
		this.lastRendererView = null; // needed to obtain canvas's absolute position
		this.mouseDownOnMe = false; // needed to detect mousedown on other things

		// set up interaction and add event handlers
		this.interactive = true;
		this.cursor = "text";
		
		this.boundOnMouseDown = this.onMouseDown.bind(this);
		this.boundOnMouseUp = this.onMouseUp.bind(this);
		this.boundOnMouseUpOutside = this.onMouseUpOutside.bind(this);

		this.mousedown = this.touchstart = this.boundOnMouseDown;
		this.mouseup = this.touchend = this.boundOnMouseUp;
		this.mouseupoutside = this.touchendoutside = this.boundOnMouseUpOutside;

		// first rendering
		this._textureNeedsUpdate = true;
		this._textNeedsUpdate = true;
		this.update();
	};

	// inheritance ; use Sprite not Graphics, ref: https://github.com/pixijs/pixi.js/issues/1390
	PIXI.InputObject.prototype = Object.create(PIXI.Sprite.prototype);
	PIXI.InputObject.prototype.constructor = PIXI.InputObject;

	// static variables
	PIXI.InputObject.inputElement = null;
	PIXI.InputObject.textareaElement = null;
	PIXI.InputObject.currentInput = null;
	PIXI.InputObject.windowEventHandlersInited = false;
	PIXI.InputObject.scrollbarWidth = 0;

	/* -------------------------------------------------------------- */
	
	// extend prototype
	extend(PIXI.InputObject.prototype, {

		// focus/blur

		focus: function()
		{
			// is already current input
			if (PIXI.InputObject.currentInput === this)
			{
				return;
			}

			// drop focus
			if (PIXI.InputObject.currentInput)
			{
				PIXI.InputObject.currentInput.blur();
			}

			// hide field graphics
			this.visible = false;
			
			// show and focus input element
			this.preShowInputElement();
			this.inputElement.style.display = "block";
			PIXI.InputObject.currentInput = this;
			this.inputElement.focus();

			// check custom focus event
			this.data.onfocus();
		},

		preShowInputElement: function()
		{
			updateInputElement(this.inputElement, this.data);
			this.setInputElementPosition();
		},

		setInputElementPosition: function(){
			var canvasRect = this.lastRendererView.getBoundingClientRect();
			var inputObjectRect = this.getBounds();
			this.inputElement.style.top = (canvasRect.top + inputObjectRect.y + this.data.shadowTop) + "px";
			this.inputElement.style.left = (canvasRect.left + inputObjectRect.x + this.data.shadowLeft) + "px";
		},

		blur: function()
		{
			if (PIXI.InputObject.currentInput === this)
			{
				PIXI.InputObject.currentInput = null;

				// blur and hide input element
				this.inputElement.blur(); // currentInput must be changed before this
				this.inputElement.style.display = "none";

				// show field graphics
				this.visible = true;

				// check custom blur event
				this.data.onblur();
			}
		},

		/* -------------------------------------------------------------- */

		// event listeners

		onInput: function(e)
		{
			// update the canvas input state information from the hidden input
			var text = this.inputElement.value;
			if (text !== this.data.value)
			{
				this.data.value = text;
				this._textNeedsUpdate = true;
			}
			
			// fire custom user event
			this.data.oninput(e, this);
		},

		onMouseDown: function(e)
		{
			// disable 2=middle and 3=right buttons
			var mouseEvent = e.data.originalEvent;
			if (mouseEvent.which === 2 || mouseEvent.which === 3)
			{
				mouseEvent.preventDefault();
				return;
			}

			// prevent focusing on canvas
			mouseEvent.preventDefault();

			// focus input
			this.focus();

			// set cursor position
			var mouse = e.data.getLocalPosition(e.target || this);
			var pos = this.getTextPosition(mouse.x, mouse.y);
			this.inputElement.selectionStart = pos;
			this.inputElement.selectionEnd = pos;
			if (this.data.text.align === "right")
			{
				this.inputElement.scrollLeft = this.inputElement.scrollWidth;
			}
			else if (this.data.text.align === "center")
			{
				// - scrollLeft/scrollWidth is not supported in ie11/edge
				// - scrollLeftMax is only for firefox
				// - (scrollWidth - clientWidth) has issues with chrome with zoom
				// - obtaining scrollLeftMax by assigning large scrollLeft value and reading the value works for chrome & firefox
				this.inputElement.scrollLeft = 99999;
				var scrollLeftMax = this.inputElement.scrollLeft;
				this.inputElement.scrollLeft = scrollLeftMax / 2;
			}
			else
			{ // align=left
				this.inputElement.scrollLeft = 0;
			}
			
			// set the flag to example in window's mouseup
			this.mouseDownOnMe = true;
		},

		onMouseUp: function(e)
		{
			// this is not always called because of visible = false; use window's mouseup to workaround.
			this.mouseDownOnMe = false;
		},

		onMouseUpOutside: function(e)
		{
			// this is not always called because of visible = false; use window's mouseup to workaround.
			this.mouseDownOnMe = false;
		},

		/* -------------------------------------------------------------- */

		// rendering methods

		update: function()
		{
			if (this._textureNeedsUpdate)
			{
				this.updateTexture();
				this.updateTextAlign();
				this._textureNeedsUpdate = false;
			}

			if (this._textNeedsUpdate)
			{
				var isPlaceholder = (!this.data.value);
				var text = (isPlaceholder ? this.data.placeholder : this.data.value) || "";
				if (this.data.type === "password" && !isPlaceholder)
				{
					text = text.replace(/./g, "*");
				}

				this.text.text = text;
				this.text.style.fill = isPlaceholder ? this.data.placeholderColor : this.data.text.fill;
				if (this.data.isTextarea)
				{
					this.text.style.wordWrap = true;
					this.text.style.wordWrapWidth = this.data.textboxWidth - this.data.padding * 2 - PIXI.InputObject.scrollbarWidth;
				}
				this._textNeedsUpdate = false;
			}
		},

		updateTexture: function()
		{
			this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

			drawInputFieldTexture(this.canvas, this.context, this.data);
			this.data.bgTopOffset = -1 * this.data.textboxTop;
			this.data.bgLeftOffset = -1 * this.data.textboxLeft;
			this.bgSprite.x = this.data.bgLeftOffset;
			this.bgSprite.y = this.data.bgTopOffset;

			var texture = this.bgSprite.texture;
			texture.baseTexture.width = this.canvas.width;
			texture.baseTexture.height = this.canvas.height;
			texture.frame.width = this.canvas.width;
			texture.frame.height = this.canvas.height;
			texture.baseTexture.update();
		},
		
		updateTextAlign: function()
		{
			var textboxTop = this.data.textboxTop + this.data.bgTopOffset;
			var textboxLeft = this.data.textboxLeft + this.data.bgLeftOffset;
			var textboxWidth = this.data.textboxWidth;
			var textboxHeight = this.data.textboxHeight;
			var padding = this.data.padding;
			
			if (this.data.text.align === "right")
			{
				this.text.x = (textboxLeft + textboxWidth - padding) || 0;
				this.text.anchor.x = 1;
			}
			else if (this.data.text.align === "center")
			{
				this.text.x = (textboxLeft + textboxWidth / 2) || 0;
				this.text.anchor.x = 0.5;
			}
			else
			{ // align=left
				this.text.x = (textboxLeft + padding) || 0;
				this.text.anchor.x = 0;
			}
			
			if (this.data.isTextarea)
			{
				this.text.y = (textboxTop + padding) || 0;
			}
			else
			{
				this.text.y = (textboxTop + (textboxHeight - this.text.height) / 2) || 0;
			}
			
			this.textMask.clear();
			this.textMask.beginFill(0x000000);
			this.textMask.drawRect(textboxLeft + padding, textboxTop + padding, textboxWidth - padding * 2, textboxHeight - padding * 2);
			this.textMask.endFill();
			
			this._textNeedsUpdate = true;
		},

		/* -------------------------------------------------------------- */

		// text position calculation

		getTextPosition: function(mouseX, mouseY)
		{
			var textboxLeft = this.data.textboxLeft + this.data.bgLeftOffset;
			var textboxWidth = this.data.textboxWidth;
			var padding = this.data.padding;

			var text = this.text.text;
			var style = this.text.style;
			var minX = textboxLeft + padding;
			var maxX = textboxLeft + textboxWidth - padding;

			if (this.data.text.align === "right")
			{
				var offsetX = maxX - PIXI.TextMetrics.measureText(text, style).width;
				var pos = text.length;
				return this.getTextPositionRightToLeft(text, style, minX, maxX, offsetX, pos, mouseX);
			}
			else if (this.data.text.align === "center")
			{
				var offsetX = textboxLeft + textboxWidth / 2 - PIXI.TextMetrics.measureText(text, style).width / 2;
				var pos = Math.floor(text.length / 2);
				var currX = offsetX + PIXI.TextMetrics.measureText(text.substring(0, pos), style).width;
				if (mouseX < currX) return this.getTextPositionRightToLeft(text, style, minX, maxX, offsetX, pos, mouseX);
				else if (currX < mouseX) return this.getTextPositionLeftToRight(text, style, minX, maxX, offsetX, pos, mouseX);
				else return pos; // mouseX == currX
			}
			else
			{ // align=left
				var offsetX = minX;
				var pos = 0;
				return this.getTextPositionLeftToRight(text, style, minX, maxX, offsetX, pos, mouseX);
			}
		},

		getTextPositionRightToLeft: function(text, style, minX, maxX, offsetX, pos, mouseX)
		{
			var currX = offsetX + PIXI.TextMetrics.measureText(text.substring(0, pos), style).width;
			if (currX <= mouseX) return pos;
			while (true)
			{
				var lastX = currX;
				pos--;
				if (pos < 0) return pos + 1;
				currX = offsetX + PIXI.TextMetrics.measureText(text.substring(0, pos), style).width;
				if (currX < minX) return pos + 1;
				else if (currX <= mouseX)
				{
					if (mouseX - currX < lastX - mouseX) return pos;
					else return pos + 1;
				}
			}
		},

		getTextPositionLeftToRight: function(text, style, minX, maxX, offsetX, pos, mouseX)
		{
			var currX = offsetX + PIXI.TextMetrics.measureText(text.substring(0, pos), style).width;
			if (mouseX <= currX) return pos;
			while (true)
			{
				var lastX = currX;
				pos++;
				if (text.length < pos) return pos - 1;
				currX = offsetX + PIXI.TextMetrics.measureText(text.substring(0, pos), style).width;
				if (maxX < currX) return pos - 1;
				else if (mouseX <= currX)
				{
					if (mouseX - lastX < currX - mouseX) return pos - 1;
					else return pos;
				}
			}
		},

		/* -------------------------------------------------------------- */

		// override pixijs methods

		updateTransform: function()
		{
			this.update();
			PIXI.Sprite.prototype.updateTransform.call(this); // call super method
		},

		renderCanvas: function(renderer)
		{
			this.lastRendererView = renderer.view;
			PIXI.Sprite.prototype.renderCanvas.call(this, renderer); // call super method
		},

		renderWebGL: function(renderer)
		{
			this.lastRendererView = renderer.view;
			PIXI.Sprite.prototype.renderWebGL.call(this, renderer); // call super method
		},

		destroy: function()
		{
			PIXI.Sprite.prototype.destroy.call(this, true); // call super method

			this.interactive = false;
			this.context = null;
			this.canvas = null;
		}

	}, true);

	/* -------------------------------------------------------------- */

	// getters and setters
	Object.defineProperty(PIXI.InputObject.prototype, "x", {
		get: function()
		{
			var propDesc = getPropertyDescriptor(PIXI.Sprite.prototype, "x");
			return propDesc ? propDesc.get.call(this) : undefined;
		},
		set: function(value)
		{
			var propDesc = getPropertyDescriptor(PIXI.Sprite.prototype, "x");
			if (propDesc)
			{
				if (propDesc.get.call(this) != value)
				{
					propDesc.set.call(this, value);
					if (PIXI.InputObject.currentInput === this)
					{
						this.setInputElementPosition();
					}
				}
			}
		}
	});
	
	Object.defineProperty(PIXI.InputObject.prototype, "y", {
		get: function()
		{
			var propDesc = getPropertyDescriptor(PIXI.Sprite.prototype, "y");
			return propDesc ? propDesc.get.call(this) : undefined;
		},
		set: function(value)
		{
			var propDesc = getPropertyDescriptor(PIXI.Sprite.prototype, "y");
			if (propDesc)
			{
				if (propDesc.get.call(this) != value)
				{
					propDesc.set.call(this, value);
					if (PIXI.InputObject.currentInput === this)
					{
						this.setInputElementPosition();
					}
				}
			}
		}
	});
	
	Object.defineProperty(PIXI.InputObject.prototype, "width", {
		get: function()
		{
			return this.data.width;
		},
		set: function(value)
		{
			if (this.data.width != value)
			{
				this.data.width = value;
				this._textureNeedsUpdate = true;
				this._textNeedsUpdate = true;
				this.update();
				if (PIXI.InputObject.currentInput === this)
				{
					updateInputElement(this.inputElement, this.data);
					this.setInputElementPosition();
				}
			}
		}
	});
	
	Object.defineProperty(PIXI.InputObject.prototype, "height", {
		get: function()
		{
			return this.data.height;
		},
		set: function(value)
		{
			if (this.data.height != value)
			{
				this.data.height = value;
				this._textureNeedsUpdate = true;
				this._textNeedsUpdate = true;
				this.update();
				if (PIXI.InputObject.currentInput === this)
				{
					updateInputElement(this.inputElement, this.data);
					this.setInputElementPosition();
				}
			}
		}
	});
	
	Object.defineProperty(PIXI.InputObject.prototype, "value", {
		get: function()
		{
			return this.data.value;
		},
		set: function(value)
		{
			value = value + "";
			if (this.data.value != value)
			{
				this.data.value = value;
				this._textNeedsUpdate = true;
				if (PIXI.InputObject.currentInput === this)
				{
					this.inputElement.value = this.data.value;
				}
			}
		}
	});

	/* -------------------------------------------------------------------------------------------------------------------------- */
	// functions for Input class

	// empty function
	function noop()
	{
	};

	// defaults
	var inputDefaults = {
		type: "text", // text or password
		value: "",
		placeholder: "",
		placeholderColor: "#999", // limitation: use with "::placeholder" css
		readonly: false,
		maxlength: null,
		onfocus: noop,
		onblur: noop,
		oninput: noop
	};
	
	var textareaDefaults = {
		value: "",
		placeholder: "",
		placeholderColor: "#999",
		readonly: false,
		onfocus: noop,
		onblur: noop,
		oninput: noop
	};

	var styleDefaults = {
		width: 200,
		height: null,
		padding: 2,
		borderColor: "#ccc",
		borderWidth: 1,
		borderRadius: 3,
		backgroundColor: "#fff",
		boxShadow: null, // "0px 0px 2px rgba(0, 0, 0, 0.5)",
		innerShadow: null, // "0px 0px 4px rgba(0, 0, 0, 0.5)",
		text: {
			font: "14px Arial",
			fill: "#000",
			align: "left",
			lineHeight: 20
		}
	};
	
	// TODO cross browser & mobile test

	/* -------------------------------------------------------------------------------------------------------------------------- */
	// main Input class
	
	PIXI.Input = function(data)
	{
		data = data || {};
		data = extend(data, { isTextarea: false });
		data = extend(data, inputDefaults);
		data = extend(data, styleDefaults);

		// call super constructor
		PIXI.InputObject.call(this, data);
	};

	// inheritance
	PIXI.Input.prototype = Object.create(PIXI.InputObject.prototype);
	PIXI.Input.prototype.constructor = PIXI.Input;

	/* -------------------------------------------------------------- */

	// extend prototype
	extend(PIXI.Input.prototype, {

		preShowInputElement: function()
		{
			PIXI.InputObject.prototype.preShowInputElement.call(this); // call super method
			
			// check max length
			this.inputElement.removeAttribute("maxLength");
			if (this.data.maxlength)
			{
				this.inputElement.maxLength = this.data.maxlength;
			}
		}

	}, true);
	
	/* -------------------------------------------------------------------------------------------------------------------------- */
	// main Textarea class
	
	PIXI.Textarea = function(data)
	{
		data = data || {};
		data = extend(data, { isTextarea: true });
		data = extend(data, textareaDefaults);
		data = extend(data, styleDefaults);

		// call super constructor
		PIXI.InputObject.call(this, data);
	};

	// inheritance
	PIXI.Textarea.prototype = Object.create(PIXI.InputObject.prototype);
	PIXI.Textarea.prototype.constructor = PIXI.Textarea;
	
})();
/**
 * @classdesc Bean point
 * @author Vincent Audergon
 * @version 1.0
 */
class Point {

    /**
     * Constructeur de l'objet point
     * @param {double} x la composante x du point
     * @param {double} y la composante y du point
     * @param {boolean} [nonconvex=false] indique si l'angle lié au point est convexe ou non (facultatif, faux par défaut)
     */
    constructor(x, y, nonconvex = false) {
        this.x = x;
        this.y = y;
        this.nonconvex = nonconvex;
    }

    /**
     * Additionne les composantes x et y du point avec celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    add(point) {
        return new Point(this.x + point.x, this.y + point.y);
    }

    /**
     * Soustrait les composantes x et y d'un autre point au point pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    sub(point) {
        return new Point(this.x - point.x, this.y - point.y);
    }

    /**
     * Multiplie les composantes x et y du point avec celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    mult(point) {
        return new Point(this.x * point.x, this.y * point.y);
    }

    /**
     * Divise les composantes x et y du point par celles d'un autres pour en recréer un nouveau
     * @param {Point} point le deuxième {@link Point}
     * @return {Point} le nouveau {@link Point}
     */
    div(point) {
        return new Point(this.x / point.x, this.y / point.y);
    }

    /**
     * Rend un nouveau point avec les valeurs absolues du point (par ex. 1;-1 => 1;1)
     * @return {Point} le nouveau {@link Point}
     */
    abs(){
        return new Point(Math.abs(this.x), Math.abs(this.y));
    }

    /**
     * Indique si deux points sont égaux ou non (composantes égales entre elles)
     * @param {Point} point le deuxième {@link Point}
     * @return {boolean} si les points sont égaux
     */
    equals(point) {
        return this.x === point.x && this.y === point.y;
    }

}
class ScrollPane extends Asset {


    constructor(x, y, width, height) {
        super();
        this.x = x | 0;
        this.y = y | 0;
        this.width = width | 0;
        this.height = height | 0;
        this.index = 1;

        this.maxIndex = 0;

        this.touchScrollLimiter = 0;
        this.lastTouch = 0;
        this.data = {};
        this.dragging = false;

        this.elements = [];

        this.graphics = {
            elementContainer: new PIXI.Container(),
            scrollBar: new PIXI.Graphics(),
            scrollButton: new PIXI.Graphics()
        };

        this.graphics.scrollButton
            .on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));


        let canvas = document.getElementById('canvas');
        canvas.addEventListener('wheel', function (event) {
            this.index += (event.deltaY > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length));
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }.bind(this));
        canvas.addEventListener('touchmove', function (event) {
            if (this.lastTouch === 0)
                this.lastTouch = event.touches[0].clientY;
            this.touchScrollLimiter += (this.lastTouch - event.touches[0].clientY);
            this.lastTouch = event.touches[0].clientY;
            if ((this.touchScrollLimiter > (this.maxIndex / this.elements.length))
                || (this.touchScrollLimiter < -(this.maxIndex / this.elements.length))) {
                this.index += this.touchScrollLimiter > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length);
                this.touchScrollLimiter = 0;
                if (this.index > this.height - 31)
                    this.index = (this.height - 31);
                else if (this.index <= 1)
                    this.index = 1;
                this.scroll();
            }
        }.bind(this));
        canvas.addEventListener('touchend', function (event) {
            this.lastTouch = 0;
        }.bind(this));
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.init();
    }

    addElements(...elements) {
        for (let element of elements)
            this.elements.push(element);
    }

    init() {

        this.graphics.scrollBar.clear();
        this.graphics.scrollBar.beginFill(0xDDDDDD);
        this.graphics.scrollBar.drawRect(this.x + this.width - 18, this.y, 18, this.height);
        this.graphics.scrollBar.endFill();

        this.graphics.scrollButton.clear();
        this.graphics.scrollButton.beginFill(0x999999);
        this.graphics.scrollButton.drawRect(this.x + this.width - 17, this.y + 1, 16, 30);
        this.graphics.scrollButton.endFill();

        this.graphics.scrollButton.interactive = true;
        this.graphics.scrollButton.buttonMode = true;

        let y = this.y;
        for (let element of this.elements) {
            if (element instanceof Asset) {
                element.setY(y);
                element.setX((this.width-20) / 2);
                y += element.getHeight();
                element.setVisible(this.checkIsInContainer(element.getY(), element.getHeight() / 2));
                for (let pc of element.getPixiChildren())
                    this.graphics.elementContainer.addChild(pc);
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                element.y = y;
                y += (typeof element.height !== 'undefined' ? element.height : 25);
                element.visible = this.checkIsInContainer(element.y);
                this.graphics.elementContainer.addChild(element);
                element.x = this.width / 2;
            }
            y += 5;
        }
        this.maxIndex = y;

        this.graphics.scrollButton.visible = (this.height < this.maxIndex);
        this.graphics.scrollBar.visible = (this.height < this.maxIndex);

        this.scroll();
    }

    clear() {
        this.elements = [];
    }

    scroll() {
        if (this.elements.length === 0)
            return;

        this.graphics.scrollButton.position.y = this.index;

        let firstDisplayedIndex = Math.floor((this.elements.length - 1) * (this.index / this.height));

        let posY = 1 + this.y + this.elements[firstDisplayedIndex].getHeight()/2;

        for (let i = 0; i < this.elements.length; i++) {
            let element = this.elements[i];
            if (element instanceof Asset) {
                if (i < firstDisplayedIndex)
                    element.setVisible(false);
                else {
                    element.setY(posY);
                    element.setVisible(this.checkIsInContainer(element.getY()));
                    posY += element.getHeight() + 5;
                }
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                if (i < firstDisplayedIndex)
                    element.visible = false;
                else {
                    element.y = posY;
                    element.visible = this.checkIsInContainer(element.y);
                    posY += element.height + 5;
                }
            }
            if(element instanceof ToggleButton)
                element.updateView();
        }
    }


    checkIsInContainer(posY, compensation = 0) {
        return (posY < (this.y + this.height) && (posY > (this.y + compensation)));
    }

    getPixiChildren() {
        let elements = [];
        for (let e in this.graphics)
            if (this.graphics[e] instanceof Asset)
                for (let element of this.graphics[e].getPixiChildren())
                    elements.push(element);
            else
                elements.push(this.graphics[e]);
        return elements;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {

    }

    onDragStart(event) {
        this.data = event.data;
        this.graphics.scrollButton.alpha = 0.5;
        this.dragging = true;
    }

    onDragEnd() {
        this.graphics.scrollButton.alpha = 1;
        this.dragging = false;
        this.data = null;
    }

    onDragMove() {
        if (this.dragging) {
            let newPosition = this.data.getLocalPosition(this.graphics.scrollButton.parent);
            this.index = newPosition.y - this.y;
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }
    }
}
class Select extends Asset{


    constructor(height, width, x, y) {
        super();

        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;

        this.button = new Button(x,y,'SELECT',0xFFFFFF,0x000000, false,width);
        this.container = new PIXI.Container();

        this.currentIndex = 0;
        this.elements = [];

        this.button.setOnClick(function () {
            this.container.visible = !this.container.visible;
        }.bind(this));

        this.container.visible = true;
    }

    addElement(element){
        element.setSelect(this);
        this.elements.push(element);
        this.build();
    }


    onClick(data){
        console.log(data);
    }

    build(){
        this.container.removeChildren(0);
        this.container.width = this.width;
        this.container.height = this.height;
        this.container.x = this.x-this.width/2;
        this.container.y = this.y+30;


        let currentY = 0;
        for(let element of this.elements){
            element.build(this.width/2, currentY, this.width);
            currentY+=element.getHeight();
            for(let child of element.getPixiChildren()){
                this.container.addChild(child);
            }
        }


    }


    resize(height, width){
        this.height = height;
        this.width = width;
        this.build();
    }

    setPosition(x,y){
        this.x = x;
        this.y = y;
        this.build();
    }

    getPixiChildren() {
        let elements = [];
        for (let e of this.button.getPixiChildren()){
            elements.push(e);
        }
        elements.push(this.container);
        return elements;
    }



    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        this.button.setVisible(visible);
        this.container.visible = visible;
    }
}
class SelectItem extends Asset{

    constructor(text, data=null) {
        super();
        this.selectRef = null;
        this.button = null;
        this.text = text;
        this.data = data;
        this.build(0,0,100);
    }


    build(x,y,width){
        this.button = new Button(x,y,this.text,0xFFFFFF,0x000000, false,width);
        this.button.setOnClick(function () {
            this.selectRef.onClick(this.data);
        }.bind(this));
    }


    getPixiChildren() {
        return this.button ? this.button.getPixiChildren():[];
    }

    setText(text){
        this.text = text;
    }

    setSelect(ref){
        this.selectRef = ref;
    }

    getData(){
        return data;
    }

    setData(data){
        this.data = data;
    }

    getHeight(){
        return this.button != null ? this.button.getHeight() : 0;
    }


    getY() {
        this.button.getY();
    }

    getX() {
        this.button.getX();
    }

    setVisible(visible) {
        this.button.setVisible(visible);
    }
}
(function (pkg){

const PIXI = pkg.PIXI

class TextInput extends PIXI.Container{
  constructor(styles){
    super()
    this._input_style = Object.assign(
      {
        position: 'absolute',
        background: 'none',
        border: 'none',
        outline: 'none',
        transformOrigin: '0 0',
        lineHeight: '1'
      },
      styles.input
    )

    if(styles.box)
      this._box_generator = typeof styles.box === 'function' ? styles.box : new DefaultBoxGenerator(styles.box)
    else
      this._box_generator = null

    if(this._input_style.hasOwnProperty('multiline')){
      this._multiline = !!this._input_style.multiline
      delete this._input_style.multiline
    }else
      this._multiline = false

    this._box_cache = {}
    this._previous = {}
    this._dom_added = false
    this._dom_visible = true
    this._placeholder = ''
    this._placeholderColor = 0xa9a9a9
    this._selection = [0,0]
    this._restrict_value = ''
    this._createDOMInput()
    this.substituteText = true
    this._setState('DEFAULT')
    this._addListeners()
  }


  // GETTERS & SETTERS

  get substituteText(){
    return this._substituted
  }

  set substituteText(substitute){
    if(this._substituted==substitute)
      return

    this._substituted = substitute

    if(substitute){
      this._createSurrogate()
      this._dom_visible = false
    }else{
      this._destroySurrogate()
      this._dom_visible = true
    }
    this.placeholder = this._placeholder
    this._update()
  }

  get placeholder(){
    return this._placeholder
  }

  set placeholder(text){
    this._placeholder = text
    if(this._substituted){
      this._updateSurrogate()
      this._dom_input.placeholder = ''
    }else{
      this._dom_input.placeholder = text
    }
  }

  get disabled(){
    return this._disabled
  }

  set disabled(disabled){
    this._disabled = disabled
    this._dom_input.disabled = disabled
    this._setState(disabled ? 'DISABLED' : 'DEFAULT')
  }

  get maxLength(){
    return this._max_length
  }

  set maxLength(length){
    this._max_length = length
    this._dom_input.setAttribute('maxlength', length)
  }

  get restrict(){
    return this._restrict_regex
  }

  set restrict(regex){
    if(regex instanceof RegExp){
      regex = regex.toString().slice(1,-1)

      if(regex.charAt(0) !== '^')
        regex = '^'+regex

      if(regex.charAt(regex.length-1) !== '$')
        regex = regex+'$'

      regex = new RegExp(regex)
    }else{
      regex = new RegExp('^['+regex+']*$')
    }

    this._restrict_regex = regex
  }

  get text(){
    return this._dom_input.value
  }

  set text(text){
    this._dom_input.value = text
    if(this._substituted) this._updateSurrogate()
  }

  get htmlInput(){
    return this._dom_input
  }

  focus(){
    if(this._substituted && !this.dom_visible)
      this._setDOMInputVisible(true)

    this._dom_input.focus()

  }

  blur(){
    this._dom_input.blur()
  }

  select(){
    this.focus()
    this._dom_input.select()
  }

  setInputStyle(key,value){
    this._input_style[key] = value
    this._dom_input.style[key] = value

    if(this._substituted && (key==='fontFamily' || key==='fontSize'))
      this._updateFontMetrics()

    if(this._last_renderer)
      this._update()
  }

  destroy(options){
    this._destroyBoxCache()
    super.destroy(options)
  }


  // SETUP

  _createDOMInput(){
    if(this._multiline){
      this._dom_input = document.createElement('textarea')
      this._dom_input.style.resize = 'none'
    }else{
      this._dom_input = document.createElement('input')
      this._dom_input.type = 'text'
    }

    for(let key in this._input_style){
      this._dom_input.style[key] = this._input_style[key]
    }
  }

  _addListeners(){
    this.on('added',this._onAdded.bind(this))
    this.on('removed',this._onRemoved.bind(this))
    this._dom_input.addEventListener('keydown', this._onInputKeyDown.bind(this))
    this._dom_input.addEventListener('input', this._onInputInput.bind(this))
    this._dom_input.addEventListener('keyup', this._onInputKeyUp.bind(this))
    this._dom_input.addEventListener('focus', this._onFocused.bind(this))
    this._dom_input.addEventListener('blur', this._onBlurred.bind(this))
  }

  _onInputKeyDown(e){
    this._selection = [
      this._dom_input.selectionStart,
      this._dom_input.selectionEnd
    ]

    this.emit('keydown',e.keyCode)
  }

  _onInputInput(e){
    if(this._restrict_regex)
      this._applyRestriction()

    if(this._substituted)
      this._updateSubstitution()

    this.emit('input',this.text)
  }

  _onInputKeyUp(e){
    this.emit('keyup',e.keyCode)
  }

  _onFocused(){
    this._setState('FOCUSED')
    this.emit('focus')
  }

  _onBlurred(){
    this._setState('DEFAULT')
    this.emit('blur')
  }

  _onAdded(){
    document.body.appendChild(this._dom_input)
    this._dom_input.style.display = 'none'
    this._dom_added = true
  }

  _onRemoved(){
    document.body.removeChild(this._dom_input)
    this._dom_added = false
  }

  _setState(state){
    this.state = state
    this._updateBox()
    if(this._substituted)
      this._updateSubstitution()
  }



  // RENDER & UPDATE

  // for pixi v4
  renderWebGL(renderer){
    super.renderWebGL(renderer)
    this._renderInternal(renderer)
  }

  // for pixi v4
  renderCanvas(renderer){
    super.renderCanvas(renderer)
    this._renderInternal(renderer)
  }

  // for pixi v5
  render(renderer){
    super.render(renderer)
    this._renderInternal(renderer)
  }

  _renderInternal(renderer){
    this._resolution = renderer.resolution
    this._last_renderer = renderer
    this._canvas_bounds = this._getCanvasBounds()
    if(this._needsUpdate())
      this._update()
  }

  _update(){
    this._updateDOMInput()
    if(this._substituted) this._updateSurrogate()
    this._updateBox()
  }

  _updateBox(){
    if(!this._box_generator)
      return

    if(this._needsNewBoxCache())
      this._buildBoxCache()

    if(this.state==this._previous.state
      && this._box==this._box_cache[this.state])
      return

    if(this._box)
      this.removeChild(this._box)

    this._box = this._box_cache[this.state]
    this.addChildAt(this._box,0)
    this._previous.state = this.state
  }

  _updateSubstitution(){
    if(this.state==='FOCUSED'){
      this._dom_visible = true
      this._surrogate.visible = this.text.length===0
    }else{
      this._dom_visible = false
      this._surrogate.visible = true
    }
    this._updateDOMInput()
    this._updateSurrogate()
  }

  _updateDOMInput(){
    if(!this._canvas_bounds)
      return

    this._dom_input.style.top = (this._canvas_bounds.top || 0)+'px'
    this._dom_input.style.left = (this._canvas_bounds.left || 0)+'px'
    this._dom_input.style.transform = this._pixiMatrixToCSS(this._getDOMRelativeWorldTransform())
    this._dom_input.style.opacity = this.worldAlpha
    this._setDOMInputVisible(this.worldVisible && this._dom_visible)

    this._previous.canvas_bounds = this._canvas_bounds
    this._previous.world_transform = this.worldTransform.clone()
    this._previous.world_alpha = this.worldAlpha
    this._previous.world_visible = this.worldVisible
  }

  _applyRestriction(){
    if(this._restrict_regex.test(this.text)){
      this._restrict_value = this.text
    }else{
      this.text = this._restrict_value
      this._dom_input.setSelectionRange(
        this._selection[0],
        this._selection[1]
      )
    }
  }


  // STATE COMPAIRSON (FOR PERFORMANCE BENEFITS)

  _needsUpdate(){
    return (
      !this._comparePixiMatrices(this.worldTransform,this._previous.world_transform)
      || !this._compareClientRects(this._canvas_bounds,this._previous.canvas_bounds)
      || this.worldAlpha != this._previous.world_alpha
      || this.worldVisible != this._previous.world_visible
    )
  }

  _needsNewBoxCache(){
    let input_bounds = this._getDOMInputBounds()
    return (
      !this._previous.input_bounds
      || input_bounds.width != this._previous.input_bounds.width
      || input_bounds.height != this._previous.input_bounds.height
    )
  }


  // INPUT SUBSTITUTION

  _createSurrogate(){
    this._surrogate_hitbox = new PIXI.Graphics()
    this._surrogate_hitbox.alpha = 0
    this._surrogate_hitbox.interactive = true
    this._surrogate_hitbox.cursor = 'text'
    this._surrogate_hitbox.on('pointerdown',this._onSurrogateFocus.bind(this))
    this.addChild(this._surrogate_hitbox)

    this._surrogate_mask = new PIXI.Graphics()
    this.addChild(this._surrogate_mask)

    this._surrogate = new PIXI.Text('',{})
    this.addChild(this._surrogate)

    this._surrogate.mask = this._surrogate_mask

    this._updateFontMetrics()
    this._updateSurrogate()
  }

  _updateSurrogate(){
    let padding = this._deriveSurrogatePadding()
    let input_bounds = this._getDOMInputBounds()

    this._surrogate.style = this._deriveSurrogateStyle()
    this._surrogate.style.padding = Math.max.apply(Math,padding)
    this._surrogate.y = this._multiline ? padding[0] : (input_bounds.height-this._surrogate.height)/2
    this._surrogate.x = padding[3]
    this._surrogate.text = this._deriveSurrogateText()

    switch (this._surrogate.style.align){
      case 'left':
        this._surrogate.x = padding[3]
        break

      case 'center':
        this._surrogate.x = input_bounds.width * 0.5 - this._surrogate.width * 0.5
        break

      case 'right':
        this._surrogate.x = input_bounds.width - padding[1] - this._surrogate.width
        break
    }

    this._updateSurrogateHitbox(input_bounds)
    this._updateSurrogateMask(input_bounds,padding)
  }

  _updateSurrogateHitbox(bounds){
    this._surrogate_hitbox.clear()
    this._surrogate_hitbox.beginFill(0)
    this._surrogate_hitbox.drawRect(0,0,bounds.width,bounds.height)
    this._surrogate_hitbox.endFill()
    this._surrogate_hitbox.interactive = !this._disabled
  }

  _updateSurrogateMask(bounds,padding){
    this._surrogate_mask.clear()
    this._surrogate_mask.beginFill(0)
    this._surrogate_mask.drawRect(padding[3],0,bounds.width-padding[3]-padding[1],bounds.height)
    this._surrogate_mask.endFill()
  }

  _destroySurrogate(){
    if(!this._surrogate) return

    this.removeChild(this._surrogate)
    this.removeChild(this._surrogate_hitbox)

    this._surrogate.destroy()
    this._surrogate_hitbox.destroy()

    this._surrogate = null
    this._surrogate_hitbox = null
  }

  _onSurrogateFocus(){
    this._setDOMInputVisible(true)
    //sometimes the input is not being focused by the mouseclick
    setTimeout(this._ensureFocus.bind(this),10)
  }

  _ensureFocus(){
    if(!this._hasFocus())
      this.focus()
  }

  _deriveSurrogateStyle(){
    let style = new PIXI.TextStyle()

    for(var key in this._input_style){
      switch(key){
        case 'color':
          style.fill = this._input_style.color
          break

        case 'fontFamily':
        case 'fontSize':
        case 'fontWeight':
        case 'fontVariant':
        case 'fontStyle':
          style[key] = this._input_style[key]
          break

        case 'letterSpacing':
          style.letterSpacing = parseFloat(this._input_style.letterSpacing)
          break

        case 'textAlign':
          style.align = this._input_style.textAlign
          break
      }
    }

    if(this._multiline){
      style.lineHeight = parseFloat(style.fontSize)
      style.wordWrap = true
      style.wordWrapWidth = this._getDOMInputBounds().width
    }

    if(this._dom_input.value.length === 0)
      style.fill = this._placeholderColor

    return style
  }

  _deriveSurrogatePadding(){
    let indent = this._input_style.textIndent ? parseFloat(this._input_style.textIndent) : 0

    if(this._input_style.padding && this._input_style.padding.length>0){
      let components = this._input_style.padding.trim().split(' ')

      if(components.length==1){
        let padding = parseFloat(components[0])
        return [padding,padding,padding,padding+indent]
      }else if(components.length==2){
        let paddingV = parseFloat(components[0])
        let paddingH = parseFloat(components[1])
        return [paddingV,paddingH,paddingV,paddingH+indent]
      }else if(components.length==4){
        let padding = components.map(component => {
          return parseFloat(component)
        })
        padding[3] += indent
        return padding
      }
    }

    return [0,0,0,indent]
  }

  _deriveSurrogateText(){
    if(this._dom_input.value.length === 0)
      return this._placeholder

    if(this._dom_input.type == 'password')
      return '•'.repeat(this._dom_input.value.length)

    return this._dom_input.value
  }

  _updateFontMetrics(){
    const style = this._deriveSurrogateStyle()
    const font = style.toFontString()

    this._font_metrics = PIXI.TextMetrics.measureFont(font)
  }


  // CACHING OF INPUT BOX GRAPHICS

  _buildBoxCache(){
    this._destroyBoxCache()

    let states = ['DEFAULT','FOCUSED','DISABLED']
    let input_bounds = this._getDOMInputBounds()

    for(let i in states){
      this._box_cache[states[i]] = this._box_generator(
        input_bounds.width,
        input_bounds.height,
        states[i]
      )
    }

    this._previous.input_bounds = input_bounds
  }

  _destroyBoxCache(){
    if(this._box){
      this.removeChild(this._box)
      this._box = null
    }

    for(let i in this._box_cache){
      this._box_cache[i].destroy()
      this._box_cache[i] = null
      delete this._box_cache[i]
    }
  }


  // HELPER FUNCTIONS

  _hasFocus(){
    return document.activeElement===this._dom_input
  }

  _setDOMInputVisible(visible){
    this._dom_input.style.display = visible ? 'block' : 'none'
  }

  _getCanvasBounds(){
    let rect = this._last_renderer.view.getBoundingClientRect()
    let bounds = {top: rect.top, left: rect.left, width: rect.width, height: rect.height}
    bounds.left += window.scrollX
    bounds.top += window.scrollY
    return bounds
  }

  _getDOMInputBounds(){
    let remove_after = false

    if(!this._dom_added){
      document.body.appendChild(this._dom_input)
      remove_after = true
    }

    let org_transform = this._dom_input.style.transform
    let org_display = this._dom_input.style.display
    this._dom_input.style.transform = ''
    this._dom_input.style.display = 'block'
    let bounds = this._dom_input.getBoundingClientRect()
    this._dom_input.style.transform = org_transform
    this._dom_input.style.display = org_display

    if(remove_after)
      document.body.removeChild(this._dom_input)

    return bounds
  }

  _getDOMRelativeWorldTransform(){
    let canvas_bounds = this._last_renderer.view.getBoundingClientRect()
    let matrix = this.worldTransform.clone()

    matrix.scale(this._resolution,this._resolution)
    matrix.scale(canvas_bounds.width/this._last_renderer.width,
           canvas_bounds.height/this._last_renderer.height)
    return matrix
  }

  _pixiMatrixToCSS(m){
    return 'matrix('+[m.a,m.b,m.c,m.d,m.tx,m.ty].join(',')+')'
  }

  _comparePixiMatrices(m1,m2){
    if(!m1 || !m2) return false
    return (
      m1.a == m2.a
      && m1.b == m2.b
      && m1.c == m2.c
      && m1.d == m2.d
      && m1.tx == m2.tx
      && m1.ty == m2.ty
    )
  }

  _compareClientRects(r1,r2){
    if(!r1 || !r2) return false
    return (
      r1.left == r2.left
      && r1.top == r2.top
      && r1.width == r2.width
      && r1.height == r2.height
    )
  }
}


function DefaultBoxGenerator(styles){
  styles = styles || {fill: 0xcccccc}

  if(styles.default){
    styles.focused = styles.focused || styles.default
    styles.disabled = styles.disabled || styles.default
  }else{
    let temp_styles = styles
    styles = {}
    styles.default = styles.focused = styles.disabled = temp_styles
  }

  return function(w,h,state){
    let style = styles[state.toLowerCase()]
    let box = new PIXI.Graphics()

    if(style.fill)
      box.beginFill(style.fill)

    if(style.stroke)
      box.lineStyle(
        style.stroke.width || 1,
        style.stroke.color || 0,
        style.stroke.alpha || 1
      )

    if(style.rounded)
      box.drawRoundedRect(0,0,w,h,style.rounded)
    else
      box.drawRect(0,0,w,h)

    box.endFill()
    box.closePath()

    return box
  }
}

pkg.exportTo[0][pkg.exportTo[1]] = TextInput

})(
  typeof PIXI === 'object'
  ? { PIXI: PIXI, exportTo: [PIXI,'TextInput'] }
  : (
    typeof module === 'object'
    ? { PIXI: require('pixi.js'), exportTo: [module,'exports'] }
    : console.warn('[PIXI.TextInput] could not attach to PIXI namespace. Make sure to include this plugin after pixi.js') || {}
  )
)
/**
 * @classdesc Asset bouton
 * @author Vincent Audergon
 * @version 1.0
 */
class ToggleButton extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {boolean} fitToText Taille du bouton en fonction du texte
     * @param {number} width Taille forcée du bouton
     */
    constructor(x, y, label, fitToText=false, width = 150) {
        super();

        this.fitToText = fitToText;
        this.refToggleGroup = null;
        this.width = width;
        this.height = 0;
        this._data = null;
        this.currentState = 'inactive';
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;

        this.lastOnClick = function(){
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };

        this.states = {
            active: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0xFFFFFF, align: 'center' })
            },
            inactive: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x000000, align: 'center' })
            },
            disabled: {
                /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
                graphics:new PIXI.Graphics(),
                /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
                lbl: new PIXI.Text(this.text, { fontFamily: 'Arial', fontSize: 18, fill: 0x666666, align: 'center' })
            }
        };

        this.onClick = this.lastOnClick;
        this.init();
        this.updateView();

    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        for (let state of Object.keys(this.states)) {
            this.states[state].graphics.interactive = true;
            this.states[state].graphics.buttonMode = true;
            this.states[state].graphics.on('pointerdown', function () {
                this.onClick()
            }.bind(this));
        }
    }

    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        for (let state of Object.keys(this.states)){
            this.states[state].lbl.text = text;
        }
        this.update();
    }


    update(){
        for (let state of Object.keys(this.states)){
            let buttonWidth = this.fitToText ? (20 + this.states[state].lbl.width): this.width;
            let buttonHeight = this.states[state].lbl.height * 1.5;
            this.height = buttonHeight;
            this.states[state].graphics.clear();
            if(state === 'active')
                this.states[state].graphics.beginFill(0x00AA00);
            else if(state === 'inactive')
                this.states[state].graphics.beginFill(0xDDDDDD);
            else
                this.states[state].graphics.beginFill(0xCCCCCC);
            this.states[state].graphics.drawRect(this.x - buttonWidth / 2, this.y - buttonHeight / 2, buttonWidth, buttonHeight);
            this.states[state].graphics.endFill();
            this.states[state].lbl.anchor.set(0.5);
            this.states[state].lbl.x = this.x;
            this.states[state].lbl.y = this.y;
        }
    }

    toggle(){
        this.currentState = (this.currentState === 'active') ? 'inactive' : 'active';
        this.updateView();
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = function () {
            onClick(this);
            this.toggle();
            if(this.refToggleGroup != null){
                this.refToggleGroup.updateList(this);
            }
        };
        this.lastOnClick = this.onClick;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.states.inactive.graphics,
            this.states.inactive.lbl,
            this.states.active.graphics,
            this.states.active.lbl,
            this.states.disabled.graphics,
            this.states.disabled.lbl];
    }

    updateView(){
        for (let state of Object.keys(this.states)){
            if(state === this.currentState){
                this.states[state].lbl.visible = true;
                this.states[state].graphics.visible = true;
            } else{
                this.states[state].lbl.visible = false;
                this.states[state].graphics.visible = false;
            }
        }
    }

    isActive(){
        return this.currentState === 'active';
    }

    isEnabled(){
        return this.currentState !== 'disabled';
    }

    set data(data){
        this._data = data;
    }

    get data(){
        return this._data;
    }


    show(){
        this.updateView();
    }

    hide(){
        for (let element of this.getPixiChildren()){
            element.visible = false;
        }
    }

    disable(){
        this.currentState = 'disabled';
        this.onClick = function () {};
        this.updateView();
    }

    enable(){
        this.currentState = 'inactive';
        this.onClick = this.lastOnClick;
        this.updateView();
    }

    setRefToggleGroup(ref){
        this.refToggleGroup = ref;
    }

    updateFont(font){
        for(let state of Object.keys(this.states)){
            this.states[state].lbl.style.fontFamily = font;
        }
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
        visible = true;
        console.log(this.currentState);
        for (let element of this.getPixiChildren()){
            element.visible = visible;
        }
    }


    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getHeight() {
        return this.height;
    }

    getWidth() {
        return this.width;
    }
}
class ToggleGroup extends Asset {

    /**
     *
     * @param mode
     * @param buttons
     */
    constructor(mode = 'single', buttons = []) {
        super();
        this.mode = mode;
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    addButton(button) {
        if (button != null) {
            button.setRefToggleGroup(this);
            this.buttons.push(button);
        }
    }

    setButtons(buttons = []) {
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    getButtons() {
        return this.buttons;
    }

    getActives() {
        let actives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'active')
                actives.push(btn);
        return actives;
    }

    getInactives() {
        let inactives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'inactive')
                inactives.push(btn);
        return inactives;
    }

    getPixiChildren() {
        let elements = [];
        for (let toggleButton of this.buttons)
            for (let element of toggleButton.getPixiChildren())
                elements.push(element);
        return elements;
    }

    updateList(button) {
        if (this.mode === 'single' && button.isActive()) {
            for (let btn of this.buttons) {
                if (btn !== button && btn.isActive()) {
                    btn.toggle();
                }
            }
        }
    }

    reset(){
        for (let btn of this.buttons){
            if(btn.isActive())
                btn.toggle();
        }
    }

    show() {
        for (let btn of this.buttons)
            btn.show();
    }

    hide() {
        for (let btn of this.buttons)
            btn.hide();
    }

    updateFont(font){
        for (let btn of this.buttons) {
            btn.updateFont(font);
        }
    }

    clearButtons(){
        this.buttons = [];
    }


    getY() {
        return 0;
    }

    getX() {
        return 0;
    }

    setVisible(visible) {
        for (let btn of this.buttons)
            btn.setVisible(visible);
    }
}

/**
 * @classdesc Définition d'une interface (ihm)
 * @author Vincent Audergon
 * @version 1.0
 */
const VERT = 0x71db4b;
const ROUGE = 0xed0e0e;
const BLEU = "#3085d6";
const ORANGE = 0xf5ab16;
class Interface {


    /**
     * Constructeur d'une ihm
     * @param {Game} refGame la référence vers la classe de jeu
     * @param {string} scene le nom de la scène utilisée par l'ihm
     */
    constructor(refGame, scene) {
        this.refGame = refGame;
        /** @type {PIXI.Container} la scène PIXI sur laquelle dessiner l'ihm */
        this.scene = refGame.scenes[scene];
        /** @type {string[]} les textes de l'interface dans toutes les langues */
        this.texts = [];
        /** @type {Object[]} les éléments qui composent l'ihm */
        this.elements = [];
    }

    /**
     * Défini la scène de l'ihm
     * @param {PIXI.Container} scene la scène PIXI
     */
    setScene(scene) {
        this.scene = scene;
    }

    /**
     * Définis les éléments PIXI contenus dans la scène
     * @param {Objects} elements la liste des éléments PIXI
     */
    setElements(elements) {
        this.elements = elements;
    }

    /**
     * Défini la liste des textes de l'ihm dans la langue courante
     * @param {string[]} texts la liste des textes
     */
    setTexts(texts) {
        this.texts = texts;
    }

    /**
     * Retourne un texte de l'ihm dans la langue courante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key) {
        return this.texts[key];
    }

    /**
     * Fonction de callback appelée lors d'un changement de langue
     * @param {string} lang
     */
    refreshLang(lang) { }

    /**
     * Retire tous les éléments de l'ihm de la scène PIXI
     */
    clear() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.removeChild(c);
                }
            } else {
                this.scene.removeChild(el);
            }
        }
    }

    /**
     * Ajoute tous les éléments de l'ihm dans la scène PIXI
     */
    init() {
        for (let element in this.elements) {
            let el = this.elements[element];
            if (el instanceof Asset) {
                for (let c of el.getPixiChildren()) {
                    this.scene.addChild(c);
                }
            } else {
                this.scene.addChild(el);
            }
        }
        this.refGame.showScene(this.scene);
    }

}
// const { MultiStyleText } = require("pixi-multistyle-text");
class Create extends Interface {

    constructor(refGame) {
        super(refGame, "create");
        this.refGame = refGame;

        this.initializeVarThis();

        // grid implmentation d'image
        this.grid = new DrawingGrid(5, 4, 100, this.scene);

        // grid implémentation des lignes
        this.gridEnd = new DrawingGrid(11, 9, 50, this.scene);
    }

    initializeVarThis(){
        this.gameName = [];
        this.tableauGridLogo = [];
        this.tableauPoint = [];
        this.langue = {};
        this.traduction = [];
        this.listTrad = [];
        this.nbrePointToCheck = -1;
        this.finishEnable = false;
        this.removeGrid = false;
        this.helpWays = false;
        this.nbreLangueCheck = 1;
        this.nbreLangue = 4;
    }

    show() {
        this.clear();
        this.elements = [];

        this.grid.init();

        this.refGame.global.util.hideTutorialInputs('next', 'btnReset', 'tuto1', 'tuto2', 'tuto3', "btnLockWays", 'btnValider', 'btnResetTr', "btnConfiguration");
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('welcomeCreate'));
        this.mainTitle = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
        this.startButton = new Button(300, 400, '', 0x32CD32, 0x000000, true);

        // Configuration de la partie des assets
        this.mainTitle.anchor.set(0.5);
        this.mainTitle.x = 300;
        this.mainTitle.y = 150;
        this.startButton.setOnClick(this.startCreateMode.bind(this));
        this.mainTitle.text = this.refGame.global.resources.getOtherText("welcomeCreate");
        this.startButton.setText(this.refGame.global.resources.getOtherText("btnStartCreate"));


        this.elements.push(this.mainTitle);
        this.elements.push(this.startButton);
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    refreshLang(lang) {
    }

    refreshFont(isOpenDyslexic) {
    }

    /** Partie des fonctions utilités */

    /**
     * Méthode qui permet l'affichage de la page "createMap"
     */
    startCreateMode() {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('step2'));
        // Suppresion des éléments d'avant
        this.clear();
        this.elements = [];
        this.saveNextButtonTranslate = new Button(475, 550, '', 0x32CD32, 0x000000, true);
        let backgroundImage = this.refGame.global.resources.getImage('backgroundCreate').image.src;
        let insertBack = new ImgBack(300, 202, 509, 409, backgroundImage, '');


        // Configuration des assets
        this.saveNextButtonTranslate.setOnClick(this.startTranslate.bind(this));
        this.saveNextButtonTranslate.setText(this.refGame.global.resources.getOtherText("saveNext"));

        this.grid.setVisiblePoint(false);
        this.grid.setModeCreate(true);
        this.grid.setRefGameDG(this.refGame);
        this.grid.setRefCreate(this);

        // on implémente les éléments crée dans l'affichage
        this.elements.push(insertBack);
        this.elements.push(this.grid);
        this.elements.push(this.saveNextButtonTranslate);

        // on remet les images si il y a déjà des images d'implenter
        if (this.tableauGridLogo.length !== 0) {
            this.grid.setLogoCreate(this.tableauGridLogo);
            let tableTemp = this.grid.getLogoCreate();
            this.grid.setBlockNode(false);
            this.grid.setBlockLogo(false);
            for (let i = 0; i < tableTemp.length; i++) {
                if (tableTemp[i] !== null) {
                    this.elements.push(tableTemp[i]);
                }
            }
        }
        this.init();
    }

    /**
     * Méthode qui permet l'affichage de la page "Translate"
     */
    startTranslate() {
        this.tableauGridLogo = this.grid.getLogoCreate();
        this.grid.setBlockNode(true);
        this.grid.setBlockLogo(true);

        //option pour la textarea
        var options = {
            value: "",
            placeholder: "",
            placeholderColor: "#999",
            width: 400,
            height: 85,
            padding: 5,
            borderColor: "black",
            borderWidth: 2,
            borderRadius: 5,
            backgroundColor: "#fff",
            boxShadow: null,
            innerShadow: null,
            text: {
                font: "14px Arial",
                fill: "#000",
                align: "left",
                lineHeight: 20
            }
        };

        // On crée les éléments dont nous avons besoin
        this.btnReturn = new Button(150, 550, '', 0x32CD32, 0x000000, true);
        this.translate = new PIXI.Textarea(options);

        // Configuration des éléments
        this.saveNextButtonTranslate.setOnClick(this.nextLangage.bind(this));
        this.saveNextButtonTranslate.setText(this.refGame.global.resources.getOtherText("saveNext"));
        this.btnReturn.setOnClick(this.returnLangage.bind(this));
        this.btnReturn.setText(this.refGame.global.resources.getOtherText("return"));
        this.translate.y = 420;
        this.translate.x = 100;

        // On enlève tous et on clear l'interface
        this.clear();
        this.elements = [];

        // On remet le tout
        let backgroundImage = this.refGame.global.resources.getImage('backgroundCreate').image.src;
        let insertBack = new ImgBack(300, 202, 509, 409, backgroundImage, '');
        this.elements.push(insertBack);
        this.elements.push(this.grid);

        for (let i = 0; i < this.tableauGridLogo.length; i++) {
            if (this.tableauGridLogo[i] !== null) {
                this.elements.push(this.tableauGridLogo[i]);
            }
        }
        this.elements.push(this.saveNextButtonTranslate);
        this.elements.push(this.btnReturn);
        this.elements.push(this.translate);

        this.retourLangues = false
        this.changeLanguage(this.nbreLangueCheck);
        // on regarde s'il n'est pas déjà passé dans ce mode
        if (this.listTrad.length !== 0) this.translate.value = this.listTrad[0].text;

        this.init();
    }

    removeCarriageReturnsInText = () => {
        const array = Array.from(this.translate.value)
        const arrayWithoutCarriageReturn = array.map((char) => {
            return char === '\n' ? 'RETURN' : char
        })

        this.translate.value = arrayWithoutCarriageReturn.join('');
        this.translate.array = arrayWithoutCarriageReturn;
    }

    addCarriageReturnInText = () => {
        let result = "";
        if (this.translate.array) {
            const arrayWithCarriageReturn = this.translate.array.map((char) => {
                return char === 'RETURN' ? '\\n' : char
            })
            result = arrayWithCarriageReturn.join('');
        }
        return result;
    }

    /**
     * Bouton qui permet d'avancer dans la page "Translate"
     */
    nextLangage() {
        this.removeCarriageReturnsInText();
        this.retourLangues = false;
        if (this.nbreLangueCheck !== 5) this.nbreLangueCheck += 1;
        this.changeLanguage(this.nbreLangueCheck);
        var valueLangue;

        if (this.nbreLangueCheck - 2 === this.listTrad.length) {
            valueLangue = JSON.parse(this.langue);
            this.listTrad.push(valueLangue);
            this.translate.value = "";
        } else if (this.nbreLangueCheck - 1 === this.listTrad.length) {
            this.translate.value = "";
        } else {
            valueLangue = JSON.parse(this.langue);
            this.listTrad[this.nbreLangueCheck - 2] = valueLangue;
            this.translate.value = this.listTrad[this.nbreLangueCheck - 1].text;
        }
        if (this.nbreLangueCheck === 5){
            valueLangue = JSON.parse(this.langue);
            this.listTrad[this.nbreLangueCheck - 2] = valueLangue;
            this.translate.value = this.listTrad[this.nbreLangueCheck - 2].text;
        }
    }

    /**
     * Bouton qui permet de reculer dans la page "Translate"
     */
    returnLangage() {
        this.nbreLangueCheck--;

        if (this.nbreLangueCheck === 0) {
            // on désactive le textarea
            this.translate.blur();
            this.translate.value = "";
            this.nbreLangueCheck = 1;
            this.changeLanguage(this.nbreLangueCheck);
            this.startCreateMode();
            this.grid.setBlockNode(false);
        } else {

            this.changeLanguage(this.nbreLangueCheck);
            this.translate.value = this.listTrad[this.nbreLangueCheck - 1].text;
        }
    }

    /**
     * Méthode qui permet l'affichage de la page "Options"
     */
    optionsPage() {
        // on change le texte en haut à gauche
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInForm'));

        // On enlève tous et on clear l'interface
        this.clear();
        this.elements = [];
        console.log(this.nbreLangueCheck);

        if (typeof this.lblNbrePointToCheck === "undefined") {
            // On crée les éléments de l'interface
            this.btnSaveNextButtonOptions = new Button(475, 550, '', 0x32CD32, 0x000000, true);
            this.btnReturnLangage = new Button(150, 550, '', 0x32CD32, 0x000000, true);
            this.lblNbrePointToCheck = new PIXI.Text('', {
                fontFamily: 'Arial',
                fontSize: 16,
                fill: 0x000000,
                align: 'center'
            });
            this.lblNbrePointToCheck2 = new PIXI.Text('', {
                fontFamily: 'Arial',
                fontSize: 16,
                fill: 0x000000,
                align: 'center'
            });
            this.txtNbrePoint = new PIXI.TextInput({
                input: {
                    fontSize: '16px',
                    padding: '6px',
                    width: '40px',
                    color: '#26272E'
                },
                box: {
                    default: {fill: 0xE8E9F3, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                    focused: {fill: 0xE1E3EE, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                    disabled: {fill: 0xDBDBDB, rounded: 12}
                }
            });

            this.cbxActiveHelpWays = new CheckBox(400, 500, "", 1);
            this.cbxActiveRemoveGrid = new CheckBox(400, 450, "", 1);

            // Configuration des éléments
            this.btnSaveNextButtonOptions.setOnClick(this.endPage.bind(this));
            this.btnSaveNextButtonOptions.setText(this.refGame.global.resources.getOtherText("saveNext"));
            this.btnReturnLangage.setOnClick(this.returnOptions.bind(this));
            this.btnReturnLangage.setText(this.refGame.global.resources.getOtherText("return"));
            this.lblNbrePointToCheck.anchor.set(0.5);
            this.lblNbrePointToCheck.x = 275;
            this.lblNbrePointToCheck.y = 115;
            this.lblNbrePointToCheck2.anchor.set(0.5);
            this.lblNbrePointToCheck2.x = 255;
            this.lblNbrePointToCheck2.y = 135;

            let tradNbrePoint = this.refGame.global.resources.getOtherText("txtNbrePoint");
            let tradDiv = tradNbrePoint.split("*sup*");
            this.lblNbrePointToCheck.text = tradDiv[0];
            this.lblNbrePointToCheck2.text = tradDiv[1];

            this.cbxActiveHelpWays.setX(75);
            this.cbxActiveHelpWays.setY(250);
            this.cbxActiveHelpWays.setText(this.refGame.global.resources.getOtherText("activeHelpWays"));
            this.cbxActiveHelpWays.select(false);
            this.cbxActiveRemoveGrid.setX(75);
            this.cbxActiveRemoveGrid.setY(350);
            this.cbxActiveRemoveGrid.setText(this.refGame.global.resources.getOtherText("activeRemoveGrid"));
            this.cbxActiveRemoveGrid.select(false);
            this.txtNbrePoint.x = 250;
            this.txtNbrePoint.y = 160;
        }

        /** On implémente dans l'interface */
        this.elements.push(this.btnSaveNextButtonOptions);
        this.elements.push(this.btnReturnLangage);
        this.elements.push(this.lblNbrePointToCheck);
        this.elements.push(this.lblNbrePointToCheck2);
        this.elements.push(this.txtNbrePoint);
        this.elements.push(this.cbxActiveHelpWays);
        this.elements.push(this.cbxActiveRemoveGrid);
        //this.elements.push(text);

        // Affichage
        this.init();
    }

    /**
     * Bouton qui permet de d'aller à la page "endPage"
     */
    endPage() {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo1'));
        // On récupère les valeurs de la page précédente
        this.removeGrid = this.cbxActiveRemoveGrid.isChecked();
        this.helpWays = this.cbxActiveHelpWays.isChecked();
        this.nbrePointToCheck = parseInt(this.txtNbrePoint.text);

        // Initialisation des variables
        this.nbreEnd = 0;
        this.tableauPointToJSON = [];

        if (this.nbrePointToCheck > 0 && this.nbrePointToCheck < 11) {
            // Suppresion des éléments d'avant
            this.clear();
            this.elements = [];

            // on crée un nouveau grid pour les traits
            this.gridEnd.setVisiblePoint(false);
            this.gridEnd.setModeCreate(false);
            this.gridEnd.setRefGameDG(this.refGame);
            this.gridEnd.setRefCreate(this);
            this.gridEnd.setCreateWays(true);

            // option pour la textarea
            var options = {
                value: "",
                placeholder: "",
                placeholderColor: "#999",
                width: 450,
                height: 60,
                padding: 5,
                borderColor: "black",
                borderWidth: 2,
                borderRadius: 5,
                backgroundColor: "#fff",
                boxShadow: null,
                innerShadow: null,
                text: {
                    font: "14px Arial",
                    fill: "#000",
                    align: "left",
                    lineHeight: 20
                }
            };

            // on ajoute dans l'interface les images qu'on a ajouté précédemment
            for (let i = 0; i < this.tableauGridLogo.length; i++) {
                if (this.tableauGridLogo[i] !== null) {
                    this.elements.push(this.tableauGridLogo[i]);
                }
            }

            console.log("allo ?");


            // On crée les éléments dont nous avons besoin
            this.returnEndPage = new Button(75, 550, '', 0x32CD32, 0x000000, true);
            this.btnReset = new Button(260, 550, '', 0x32CD32, 0x000000, true);
            this.saveAll = new Button(475, 550, '', 0x32CD32, 0x000000, true);
            this.saveNextButtonEndPage = new Button(475, 550, '', 0x32CD32, 0x000000, true);
            this.txtTranslateHelp = new PIXI.Textarea(options);

            // on configure les éléments crée
            this.returnEndPage.setOnClick(this.returnEndPageF.bind(this));
            this.returnEndPage.setText(this.refGame.global.resources.getOtherText("return"));
            this.btnReset.setOnClick(this.resetGridEnd.bind(this));
            this.btnReset.setText(this.refGame.global.resources.getOtherText("btnResetCreate"));
            this.saveNextButtonEndPage.setOnClick(this.saveNextEndPage.bind(this));
            this.saveNextButtonEndPage.setText(this.refGame.global.resources.getOtherText("saveNext"));
            this.saveAll.setOnClick(this.namePage.bind(this));
            this.saveAll.setText(this.refGame.global.resources.getOtherText("saveNext"));

            // on regarde la langue sélectionne
            switch (this.refGame.global.resources.getLanguage()) {
                case "fr" :
                    this.langCurrent = 1;
                    break;
                case "de" :
                    this.langCurrent = 2;
                    break;
                case "it" :
                    this.langCurrent = 3;
                    break;
                case "en" :
                    this.langCurrent = 4;
                    break;
            }
            this.txtTranslateHelp.value = this.listTrad[this.langCurrent - 1].text;
            this.txtTranslateHelp.y = 450;
            this.txtTranslateHelp.x = 50;
            this.txtTranslateHelp.interactive = false;

            // on implémente dans l'interface
            this.elements.push(this.gridEnd);
            this.elements.push(this.returnEndPage);
            this.elements.push(this.txtTranslateHelp);
            this.elements.push(this.saveNextButtonEndPage);
            this.elements.push(this.saveAll);
            this.elements.push(this.btnReset);

            this.saveAll.setVisible(false);

            // on affiche l'interface
            this.init();
        } else {
            Swal.fire({
                icon: 'error',
                text: this.refGame.global.resources.getOtherText('notCorrectOptions')
            });
        }
    }

    /**
     * Bouton qui permet de revenir à la page "Translate"
     */
    returnOptions() {
        // On enlève tous et on clear l'interface
        this.clear();
        this.elements = [];

        this.elements.push(this.grid);
        for (let i = 0; i < this.tableauGridLogo.length; i++) {
            if (this.tableauGridLogo[i] !== null) {
                this.elements.push(this.tableauGridLogo[i]);
            }
        }

        this.elements.push(this.saveNextButtonTranslate);
        this.elements.push(this.btnReturn);
        this.elements.push(this.translate);
        this.nbreLangueCheck = 4;
        this.retourLangues = true;
        this.changeLanguage(this.nbreLangueCheck);

        this.init();
    }

    /**
     * Bouton qui permet de revenir à la page "Options"
     */
    returnEndPageF() {
        if (this.nbreEnd >= 1) {
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo' + (this.nbreEnd + 2)));
            this.nbreEnd--;
            this.changeDrawingLine();
        } else {
            this.gridEnd.reset();
            this.optionsPage();
        }
    }

    /**
     * Bouton qui terminer la end page
     */
    async saveNextEndPage() {
        this.tableauPoint = this.gridEnd.getPoint();
        this.grid.setBlockNode(true);
        if (this.tableauPoint.length !== 3 && this.tableauPoint.length !== 1) {
            Swal.fire({
                icon: 'error',
                text: this.refGame.global.resources.getOtherText('notCorrectPoint')
            });
            this.resetGridEnd();
        } else {
            if (this.nbrePointToCheck >= this.nbreEnd + 1) {
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo' + (this.nbreEnd + 2)));
                /** On vérifie que l'élève n'a pas annulé l'action */
                let result = await this.popupTextEndPage();

                if(typeof result !== "undefined"){
                    console.log("undifined state");
                    this.traduction[this.nbreEnd] = result;
                    this.nbreEnd++;
                    this.changeDrawingLine();
                } else {
                    if(this.nbreEnd === 0){
                        console.log("number = 0 state");
                        this.changeDrawingLine();
                    } else {
                        console.log("else state");
                        this.nbreEnd--;
                        this.changeDrawingLine();
                    }
                }
            }
        }
    }

    /**
     * la page de nommage du niveau
     */
    namePage(){
        //Clear du canvas
        this.clear();
        this.elements = [];


        //variables d'options pour les différents élémets

        let otpionsLBL = {
            fontFamily: 'Arial',
            fontSize: 16,
            fill: 0x000000,
            align: 'center'
        };


        //Création des éléments
        this.btnReturn = new Button(150, 550, '', 0x32CD32, 0x000000, true);
        this.btnsaveAll = new Button(475, 550, '', 0x32CD32, 0x000000, true);

        this.btnsaveNextNamePage = new Button(475, 550, '', 0x32CD32, 0x000000, true);

        this.txaTitleFr = new PIXI.Textarea({
                value: "",
                placeholder: "",
                placeholderColor: "#999",
                width: 200,
                height: 25,
                padding: 5,
                borderColor: "black",
                borderWidth: 2,
                borderRadius: 3,
                backgroundColor: "#fff",
                boxShadow: null,
                innerShadow: null,
                text: {
                    font: "14px Arial",
                    fill: "#000",
                    align: "left",
                    lineHeight: 20
                }
            }
            );
        this.lblTitleFr = new PIXI.Text('Nom du jeux en Français :', otpionsLBL);

        this.txaTitleDe = new PIXI.Textarea({
            value: "",
            placeholder: "",
            placeholderColor: "#999",
            width: 200,
            height: 25,
            padding: 5,
            borderColor: "black",
            borderWidth: 2,
            borderRadius: 3,
            backgroundColor: "#fff",
            boxShadow: null,
            innerShadow: null,
            text: {
                font: "14px Arial",
                fill: "#000",
                align: "left",
                lineHeight: 20
            }
        });
        this.lblTitleDe = new PIXI.Text('Nom du jeux en Allemand :', otpionsLBL);

        this.txaTitleIt = new PIXI.Textarea({
            value: "",
            placeholder: "",
            placeholderColor: "#999",
            width: 200,
            height: 25,
            padding: 5,
            borderColor: "black",
            borderWidth: 2,
            borderRadius: 3,
            backgroundColor: "#fff",
            boxShadow: null,
            innerShadow: null,
            text: {
                font: "14px Arial",
                fill: "#000",
                align: "left",
                lineHeight: 20
            }
        });
        this.lblTitleIt = new PIXI.Text('Nom du jeux en Italien :', otpionsLBL);

        this.txaTitleEn = new PIXI.Textarea({
            value: "",
            placeholder: "",
            placeholderColor: "#999",
            width: 200,
            height: 25,
            padding: 5,
            borderColor: "black",
            borderWidth: 2,
            borderRadius: 3,
            backgroundColor: "#fff",
            boxShadow: null,
            innerShadow: null,
            text: {
                font: "14px Arial",
                fill: "#000",
                align: "left",
                lineHeight: 20
            }
        });
        this.lblTitleEn = new PIXI.Text('Nom du jeux en Anglais :', otpionsLBL);
        console.log(this.txaTitleFr === this.txaTitleIt);

        //Paramétrage des éléments
        this.btnReturn.setOnClick(this.returntoEndPage.bind(this));
        this.btnReturn.setText(this.refGame.global.resources.getOtherText("return"));
        this.btnsaveAll.setOnClick(this.saveNextNamePage.bind(this));
        this.btnsaveAll.setText(this.refGame.global.resources.getOtherText("finish"));
        this.refGame.global.util.setTutorialText("Entrer le nom de votre niveau. (Français obligatoire)");

        this.lblTitleFr.x = 75;
        this.lblTitleFr.y = 125;
        this.txaTitleFr.x = 275;
        this.txaTitleFr.y = 115;

        this.lblTitleDe.x = 70;
        this.lblTitleDe.y = 205;
        this.txaTitleDe.x = 275;
        this.txaTitleDe.y = 195;

        this.lblTitleIt.x = 93;
        this.lblTitleIt.y = 285;
        this.txaTitleIt.x = 275;
        this.txaTitleIt.y = 275;

        this.lblTitleEn.x = 85;
        this.lblTitleEn.y = 365;
        this.txaTitleEn.x = 275;
        this.txaTitleEn.y = 355;

        this.txaTitleFr.autofocus = false;
        this.txaTitleDe.autofocus = false;
        this.txaTitleEn.autofocus = false;
        this.txaTitleIt.autofocus = false;

        //Ajout des éléments
        this.elements.push(this.btnsaveAll);
        this.elements.push(this.btnReturn);

        this.elements.push(this.txaTitleFr);
        this.elements.push(this.lblTitleFr);

        this.elements.push(this.txaTitleDe);
        this.elements.push(this.lblTitleDe);

        this.elements.push(this.txaTitleIt);
        this.elements.push(this.lblTitleIt);

        this.elements.push(this.txaTitleEn);
        this.elements.push(this.lblTitleEn);


        // Affichage
        this.init();

    }

    /**
     * Méthode sauvegardant le nom du niveau
     */
    saveNextNamePage(){
        if ((this.txaTitleFr.value != null) && (this.txaTitleFr.value != "")){
            if(this.isValidAttribute(this.txaTitleFr.value)&&this.isValidAttribute(this.txaTitleDe.value)&&this.isValidAttribute(this.txaTitleIt.value)&&this.isValidAttribute(this.txaTitleEn.value)){
                this.gameName[0] = this.txaTitleFr.value;
                this.gameName[1] = this.txaTitleDe.value;
                this.gameName[2] = this.txaTitleIt.value;
                this.gameName[3] = this.txaTitleEn.value;

                this.saveAllF();
            } else {
                Swal.fire({
                    icon: 'error',
                    text: 'L\'un des champs n\'est pas valide !'
                });
            }

        }else{
            Swal.fire({
                icon: 'error',
                text: 'Le nom du jeux en Français est obligatoire !'
            });
        }
    }

    /**
     * retourne à la end page
     */
    returntoEndPage(){
        this.endPage();
    }

    /**
     * controle si un sting ne contiens pas de caractères sensible. ( ", /, ')
     *
     * @param string : le string a vérifier
     * @returns {boolean} : si le string contiens un caractère sensible, retourne false, sinon retourne true.
     */
    isValidAttribute(string){
        let validate;
        if (string.includes('"') || string.includes("/") || string.includes("'")){
            validate = false;
        }else{
            validate = true;
        }
        return validate;
    }

    /**
     * Méthode qui réinitilise l'interface
     */
    resetGridEnd() {
        this.gridEnd.reset();
    }

    /**
     * Bouton qui permet de créer le JSON pour l'exercice
     */
    saveAllF() {
        this.arrayStartEnd = this.grid.getPointStartEnd();
        if ((this.arrayStartEnd.length === 2) || (this.arrayStartEnd.length === 1) || (this.arrayStartEnd.length === 0)) {
            let coordStart = this.arrayStartEnd[0];
            let coordEnd = this.arrayStartEnd[1];
            // JSON de fin pour envoyer dans la base de données
            var JSONNewExercice = {};

            /**  Pour donner un nom au niveau dans le Json*/
            JSONNewExercice= '{"name":{';
            JSONNewExercice += '"fr" : "'+this.gameName[0]+'",';
            JSONNewExercice += '"de" : "'+this.gameName[1]+'",';
            JSONNewExercice += '"en" : "'+this.gameName[2]+'",';
            JSONNewExercice += '"it" : "'+this.gameName[3]+'"';
            JSONNewExercice += '},';

            /** La partie verif du JSON */
            JSONNewExercice += '"verif" : {' +
                '"debut" : {' +
                '"1" : "'+ coordStart +'"' +
                '},';
            JSONNewExercice += '"fin" : {' +
                '"1" : "'+ coordEnd +'"' +
                '},';
            /** Les points obligatoires à passer */
            JSONNewExercice += '"segments" : {';
            for (let i = 0; i < this.tableauPointToJSON.length ; i++) {
                i++;
                if(i !== this.tableauPointToJSON.length){
                    JSONNewExercice += '"'+ i +'" : "'+ this.tableauPointToJSON[i-1] +'",'
                } else {
                    JSONNewExercice += '"'+ i +'" : "'+ this.tableauPointToJSON[i-1] +'"';
                }
                i--;
            }
            JSONNewExercice += '}';
            // on ferme celui du verif
            JSONNewExercice += '},';

            /** la partie des options */
            JSONNewExercice += '"enableLockWays" : ' + this.helpWays + ',';
            JSONNewExercice += '"enableLockMap" : ' + this.removeGrid + ',';


            /* la partie background
                JSONNewExercice["background"] = ;
             */

            /** la partie erreur */
            JSONNewExercice += '"error" : {';
            for (let i = 0; i < this.traduction.length ; i++) {
                if((i+1) !== this.traduction.length){
                    JSONNewExercice += '"'+i+'" : {';
                    JSONNewExercice += '"fr" : "'+this.traduction[i][0]+'",';
                    JSONNewExercice += '"de" : "'+this.traduction[i][1]+'",';
                    JSONNewExercice += '"en" : "'+this.traduction[i][2]+'",';
                    JSONNewExercice += '"it" : "'+this.traduction[i][3]+'"';
                    JSONNewExercice += '},';
                } else {
                    JSONNewExercice += '"'+i+'" : {';
                    JSONNewExercice += '"fr" : "'+this.traduction[i][0]+'",';
                    JSONNewExercice += '"de" : "'+this.traduction[i][1]+'",';
                    JSONNewExercice += '"en" : "'+this.traduction[i][2]+'",';
                    JSONNewExercice += '"it" : "'+this.traduction[i][3]+'"';
                    JSONNewExercice += '}';
                }
            }
            JSONNewExercice += '},';

            /** la partie background */
            JSONNewExercice += '"translate" : {';
            for (let i = 0; i < this.nbreLangue; i++) {
                switch(i+1) {
                    case 1 :
                        JSONNewExercice += '"fr" : "' + JSON.parse(this.fr).text + '",';
                        break;
                    case 2 :
                        JSONNewExercice += '"de" : "' + JSON.parse(this.de).text + '",';
                        break;
                    case 3 :
                        JSONNewExercice += '"en" : "' + JSON.parse(this.en).text + '",';
                        break;
                    case 4 :
                        JSONNewExercice += '"it" : "' + JSON.parse(this.it).text + '"';
                        break;
                }
            }
            JSONNewExercice += '},';


            /** la partie des images */
            JSONNewExercice += '"pictures" : {';
            for (let i = 0; i < this.tableauGridLogo.length; i++) {
                i++;
                if(i !== this.tableauGridLogo.length){
                    JSONNewExercice += '"'+ i +'" : {' +
                        '"nomFichier" : "'+  this.tableauGridLogo[i-1].name +'",' +
                        '"x" : "'+ this.tableauGridLogo[i-1].x +'",' +
                        '"y" : "'+ this.tableauGridLogo[i-1].y +'",' +
                        '"width" : "'+ this.tableauGridLogo[i-1].width +'",' +
                        '"nom" : {';

                    let listTrad = this.grid.getTradImage();
                    for (let j = 0; j < listTrad[i-1].length; j++) {
                        if(j !== listTrad[i-1].length-1){
                            JSONNewExercice += '"'+ j +'" : "'+ listTrad[i-1][j] +'",';
                        } else {
                            JSONNewExercice += '"'+ j +'" : "'+ listTrad[i-1][j] +'"';
                        }
                    }

                    JSONNewExercice += '}';
                    JSONNewExercice += '},';
                } else {
                    JSONNewExercice += '"'+ i +'" : {' +
                        '"nomFichier" : "'+  this.tableauGridLogo[i-1].name +'",' +
                        '"x" : "' + this.tableauGridLogo[i-1].x + '",' +
                        '"y" : "' + this.tableauGridLogo[i-1].y + '",' +
                        '"width" : "' + this.tableauGridLogo[i-1].width + '",' +
                        '"nom" : {';

                    let listTrad = this.grid.getTradImage();
                        for (let j = 0; j < listTrad[i-1].length; j++) {
                            if(j !== listTrad[i-1].length-1){
                                JSONNewExercice += '"'+ j +'" : "'+ listTrad[i-1][j] +'",';
                            } else {
                                JSONNewExercice += '"'+ j +'" : "'+ listTrad[i-1][j] +'"';
                            }
                        }

                    JSONNewExercice += '}';
                    JSONNewExercice += '}';

                }
                i--;
            }
            JSONNewExercice += '}';

            /** Pour fermer l'ensemble du JSON */
            JSONNewExercice += '}';

            console.log(JSONNewExercice);

            /** J'envoie dans la base de données le JSON et j'affiche un message de reception */
            this.refGame.global.resources.saveExercice(JSONNewExercice, function (result) {
                Swal.fire(
                    result.message,
                    undefined,
                    result.type
                );
                if(result){
                    // On réinitialise toute la page
                    this.initializeVarThis();
                     this.grid.setLogoCreate(this.tableauGridLogo);
                     this.grid.setBlockNode(false);
                    this.txtNbrePoint.text = "";
                    this.clear();
                    this.elements = [];
                    this.show();
                }
            }.bind(this));


        } else {
            /** On affiche un message d'erreur car l'élève n'a pas mis un point de départ (start) et un point de fin (end) */
            Swal.fire({
                icon: 'error',
                text: this.refGame.global.resources.getOtherText('notSelectStartEnd')
            });
        }
    }

    /** Méthode intraClass utilisé seulement ici */

    /**
     * Fonction qui permet de mettre l'information dans langue et de changer les traductions
     * @param nbre le nbre en langue (1 = FR, 2 = DE, 3 = IT, 4 = EN)
     */
    changeLanguage(nbre) {
        const text = this.addCarriageReturnInText();
        switch (nbre - 1) {
            case 0 :
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('translateFR'));
                this.langue = '{"langue" : ' + nbre + ', "text" : "' + text + '"}';
                if (this.retourLangues) this.de = '{"text" : "' + text + '"}';
                break;
            case 1 :
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('translateDE'));
                this.langue = '{"langue" : ' + nbre + ', "text" : "' + text + '"}';
                this.retourLangues ? this.it = '{"text" : "' + text + '"}' : this.fr = '{"text" : "' + text + '"}';
                break;
            case 2 :
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('translateIT'));
                this.langue = '{"langue" : ' + nbre + ', "text" : "' + text + '"}';
                this.retourLangues ? this.en = '{"text" : "' + text + '"}' : this.de = '{"text" : "' + text + '"}';
                break;
            case 3 :
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('translateEN'));
                this.langue = '{"langue" : ' + nbre + ', "text" : "' + text + '"}';
                if (!this.retourLangues) this.it = '{"text" : "' + text + '"}';
                break;
            case 4 :
                this.langue = '{"langue" : ' + nbre + ', "text" : "' + text + '"}';
                this.en = '{"text" : "' + text + '"}';
                // Vérifier que le français a bien été rempli
                if (typeof this.listTrad[0].text !== "undefined" && this.listTrad[0].text !== "") {
                    // on désactive le textarea
                    this.translate.blur();
                    this.optionsPage();
                } else {
                    Swal.fire({
                        icon: 'error',
                        text: this.refGame.global.resources.getOtherText('missingInfoLangage')
                    });
                }
                break;
        }
    }

    /**
     * Fonction qui permet d'afficher le tableau demandé de la page "createMap"
     * @param listLogo la liste des images à implenter
     */
    displayLogo(listLogo) {
        this.clear();
        this.elements = [];

        let backgroundImage = this.refGame.global.resources.getImage('backgroundCreate').image.src;
        let insertBack = new ImgBack(300, 202, 509, 409, backgroundImage, '');
        this.elements.push(insertBack);

        this.elements.push(this.grid);
        this.elements.push(this.saveNextButtonTranslate);

        for (let i = 0; i < listLogo.length; i++) {
            if (listLogo[i] !== null) {
                this.elements.push(listLogo[i]);
            }
        }
        this.init();
    }

    /**
     * Méthode qui permet de supprimer l'image donnée dans l'affichage
     * @param image est l'image qui sera supprimé
     */
    takeOffLogo(image) {
        let listLogo = this.grid.getLogoCreate();

        for (let i = 0; i < listLogo.length; i++) {
            if (listLogo[i] === image) {
                listLogo[i] = null;
            }
        }
        this.displayLogo(listLogo);
    }

    /**
     * Déffinit la in de l'action end page
     */
    displayNext(boolean) {
        this.finishEnable = boolean;
        if (boolean) {
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo' + (this.nbreEnd + 1)));
        } else {
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo' + (this.nbreEnd + 1)));
            this.gridEnd.reset();
        }
        this.btnReset.setVisible(!boolean);
        this.saveNextButtonEndPage.setVisible(!boolean);
        this.saveAll.setVisible(boolean);

    }

    /**
     * Permet de changer l'interface en fonction de la variable "nbreEnd"
     */
    changeDrawingLine() {
        if (this.finishEnable) this.displayNext(!this.finishEnable);
        if(this.nbreEnd === 0){
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo' + (this.nbreEnd + 1)));
            this.gridEnd.reset();
        }
        else if(this.nbreEnd === 10){
            if(this.tableauPoint[2] === undefined){
                this.tableauPointToJSON.push(this.tableauPoint[0].name + "/" + this.tableauPoint[0].name);
            } else {
                this.tableauPointToJSON.push(this.tableauPoint[0].name + "/" + this.tableauPoint[2].name);
            }
            this.gridEnd.reset();
            this.displayNext(true);
        } else {
            if(this.tableauPoint[2] === undefined){
                this.tableauPointToJSON.push(this.tableauPoint[0].name + "/" + this.tableauPoint[0].name);
            } else {
                this.tableauPointToJSON.push(this.tableauPoint[0].name + "/" + this.tableauPoint[2].name);
            }
            this.gridEnd.reset();
            if (this.nbreEnd === this.nbrePointToCheck)
                this.namePage();
            else
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo' + (this.nbreEnd + 1)));
        }
    }

    /**
     * Popup pour que l'élève puisse dire ou est/sont les fautes
     */
    async popupTextEndPage() {
        const {value: text} = await Swal.fire({
            title: this.refGame.global.resources.getOtherText('introTrad'),
            html:
                '<textarea name="textarea" rows="4" cols="40" id="txtFR">'+JSON.parse(this.fr).text+'</textarea>' +
                '<br>' +
                '<textarea name="textarea" rows="4" cols="40" id="txtDE">'+JSON.parse(this.de).text+'</textarea>' +
                '<br>' +
                '<textarea name="textarea" rows="4" cols="40" id="txtIT">'+JSON.parse(this.it).text+'</textarea>' +
                '<br>' +
                '<textarea name="textarea" rows="4" cols="40" id="txtEN">'+JSON.parse(this.en).text+'</textarea>'
            ,
            focusConfirm: false,
            showCancelButton: true,
            preConfirm: () => {
                return [
                    document.getElementById('txtFR').value,
                    document.getElementById('txtDE').value,
                    document.getElementById('txtIT').value,
                    document.getElementById('txtEN').value
                ]
            }
        });
        return text;
    }

    padTo2Digits(num) {
        return num.toString().padStart(2, '0');
    }

    /**
     * formatte unedate en format dd/mm/yy
     * @param date la date a formaté
     * @returns {string} la date formatée
     */
    formatDate(date) {
        return [
            this.padTo2Digits(date.getDate()),
            this.padTo2Digits(date.getMonth() + 1),
            date.getFullYear(),
        ].join('/');
    }

}
class Explore extends Interface {
    constructor(refGame) {
        super(refGame, "explore");
        this.tuto = true;
        this.maplock = false;
        this.refGame = refGame;
        this.exercice = JSON.parse(this.refGame.global.resources.getScenario());
    }

    show() {
        console.log(this.exercice);
        this.clear();
        //permet de créer la grille puis de la mettre dans le canvas et on désactive le tracé
        this.grid = new DrawingGrid(11, 9, 50, this.scene);
        this.grid.setRefGameDG(this.refGame);
        this.grid.reset();
        this.counterSteps = 1;
        this.elements.push(this.grid);
        this.grid.setBlockNode(true);

        this.dys = true;
        let langue = this.refreshLang(this.refGame.global.resources.getLanguage(), 3);
        let lang = this.refGame.global.resources.getLanguage();
        this.refGame.global.util.hideTutorialInputs('btnValider', 'btnResetTr', "btnLockWays", "btnLockMap", "btnAudioGame");

        //permet l'affichage des checkBox pour cacher la grille et de desactivation de suivie
        if (this.exercice.exerciceDeBase.enableLockWays) {
            this.refGame.global.util.showTutorialInputs("btnLockWays");
            this.refGame.global.util.showTutorialInputs("btnLockWaysid");
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnLockWays'), ['click'], this.handleLockWays.bind(this));
        } else {
            this.refGame.global.util.hideTutorialInputs("btnLockWays");
            this.refGame.global.util.hideTutorialInputs("btnLockWaysid");
        }
        if (this.exercice.exerciceDeBase.enableLockMap) {
            this.refGame.global.util.showTutorialInputs("btnLockMap");
            this.refGame.global.util.showTutorialInputs("btnLockMapid");
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnLockMap'), ['click'], this.handleLockMap.bind(this));
        } else {
            this.refGame.global.util.hideTutorialInputs("btnLockMap");
            this.refGame.global.util.hideTutorialInputs("btnLockMapid");
        }

        //montre le premier texte avec la consigne en entier
        this.refGame.global.util.showTutorialInputs('next', 'btnReset', 'tuto1', 'tuto2', 'tuto3');
        this.refGame.global.util.setTutorialText("<p class='scroller'>" + "Bienvenue dans Par ici ou par-là, le texte ci-dessous t'aideras à savoir où Jean est allé." + "</p>" + '<p style="font-size:20px;" class="scroller">' + langue + "</p>");

        document.getElementById("tutorial").className = 'smoothie ';
        // document.getElementById("tutorial").style.overflowY='scroll';


        //this.refGame.global.listenerManager.addListenerOn(document.getElementById('next'), ['click'], this.handleNext.bind(this));
        document.getElementById('next').addEventListener("click", this.handleNext.bind(this));


        //création des flèches pour le tuto

        //flèches pour le point de départ
        this.fleche = new Arrow(505, 367.5, 482, 367.5, 0x30B21A, false, [505, 385, 482, 385]);
        this.flechePoint1 = new Arrow(505, 368, 482, 368, 0x30B21A, false, [505, 350, 482, 350]);

        //flèche pour le premier point de passage obligatoire
        this.fleche2 = new Arrow(348, 317.5, 238, 317.5, 0x0000ff, false, [348, 317.5, 238, 317.5]);
        //flèche pour le deuxième point de passage obligatoire
        this.fleche3 = new Arrow(148, 417.5, 38, 417.5, 0x2FFF9F, false, [148, 417.5, 38, 417.5]);
        //flèche pour le troisième point de passage obligatoire
        this.fleche4 = new Arrow(298, 67.5, 290, 67.5, 0xff0000, false, [298, 67.5, 290, 67.5]);
        //flèches pour le point d'arrivé
        this.fleche5 = new Arrow(505, 67.5, 482, 67.5, 0xff0000, false, [505, 52, 482, 52]);
        this.flechPoint2 = new Arrow(505, 66, 482, 66, 0xff0000, false, [505, 82, 482, 82]);


        //on cache toutes les flèches
        this.fleche.setVisible(false);
        this.flechePoint1.setVisible(false);
        this.fleche2.setVisible(false);
        this.fleche3.setVisible(false);
        this.fleche4.setVisible(false);
        this.fleche5.setVisible(false);
        this.flechPoint2.setVisible(false);
        //###########################IMAGES###########################
        //= '{\n' + '  "exerciceDeBase": {\n' + '    "listeBoutons": {\n' + '      "boutons": {\n' + '        "1": {\n' + '          "zone": {\n' + '            "x": "473",\n' + '            "y": "484",\n' + '            "w": "188",\n' + '            "h": "15"\n' + '          },\n' + '          "x": "473",\n' + '          "y": "484",\n' + '          "w": "188",\n' + '          "h": "15"\n' + '        },\n' + '        "2": {\n' + '          "zone": {\n' + '            "x": "40",\n' + '            "y": "500",\n' + '            "w": "40",\n' + '            "h": "15"\n' + '          },\n' + '          "x": "40",\n' + '          "y": "500",\n' + '          "w": "40",\n' + '          "h": "15"\n' + '        },\n' + '        "3": {\n' + '          "zone": {\n' + '            "x": "191",\n' + '            "y": "500",\n' + '            "w": "255",\n' + '            "h": "15"\n' + '          },\n' + '          "x": "191",\n' + '          "y": "500",\n' + '          "w": "255",\n' + '          "h": "15"\n' + '        },\n' + '        "4": {\n' + '          "zone": {\n' + '            "x": "543",\n' + '            "y": "500",\n' + '            "w": "70",\n' + '            "h": "15"\n' + '          },\n' + '          "x": "543",\n' + '          "y": "500",\n' + '          "w": "70",\n' + '          "h": "15"\n' + '        },\n' + '        "5": {\n' + '          "zone": {\n' + '            "x": "90",\n' + '            "y": "517",\n' + '            "w": "140",\n' + '            "h": "15"\n' + '          },\n' + '          "x": "90",\n' + '          "y": "517",\n' + '          "w": "140",\n' + '          "h": "15"\n' + '        }\n' + '      }\n' + '    },\n' + '    "background": {\n' + '      "x": "300",\n' + '      "y": "220",\n' + '      "width": "507",\n' + '      "height": "419"\n' + '    },\n' + '    "pictures": {\n' + '      "1": {\n' + '        "nomFichier": "apartment",\n' + '        "x": "480",\n' + '        "y": "170",\n' + '        "width": "90",\n' + '        "nom": ""\n' + '      },\n' + '      "2": {\n' + '        "nomFichier": "appleTree",\n' + '        "x": "480",\n' + '        "y": "280",\n' + '        "width": "90",\n' + '        "nom": "Pommier"\n' + '      },\n' + '      "3": {\n' + '        "nomFichier": "largeTree",\n' + '        "x": "360",\n' + '        "y": "200",\n' + '        "width": "90",\n' + '        "nom": "Arbre geant"\n' + '      },\n' + '      "4": {\n' + '        "nomFichier": "house",\n' + '        "x": "290",\n' + '        "y": "360",\n' + '        "width": "90",\n' + '        "nom": "Ecole"\n' + '      },\n' + '      "5": {\n' + '        "nomFichier": "everGreen",\n' + '        "x": "210",\n' + '        "y": "362",\n' + '        "width": "64",\n' + '        "nom": ""\n' + '      },\n' + '      "6": {\n' + '        "nomFichier": "company",\n' + '        "x": "110",\n' + '        "y": "270",\n' + '        "width": "90",\n' + '        "nom": "Usine"\n' + '      },\n' + '      "7": {\n' + '        "nomFichier": "barn",\n' + '        "x": "140",\n' + '        "y": "110",\n' + '        "width": "90",\n' + '        "nom": "Ferme"\n' + '      },\n' + '      "8": {\n' + '        "nomFichier": "home96",\n' + '        "x": "480",\n' + '        "y": "60",\n' + '        "width": "90",\n' + '        "nom": ""\n' + '      },\n' + '      "9": {\n' + '        "nomFichier": "museum",\n' + '        "x": "270",\n' + '        "y": "45",\n' + '        "width": "90",\n' + '        "nom": "musee"\n' + '      },\n' + '      "10": {\n' + '        "nomFichier": "building",\n' + '        "x": "220",\n' + '        "y": "200",\n' + '        "width": "90",\n' + '        "nom": ""\n' + '      },\n' + '      "11": {\n' + '        "nomFichier": "son",\n' + '        "x": "550",\n' + '        "y": "200",\n' + '        "width": "40",\n' + '        "nom": "Pierre"\n' + '      },\n' + '      "12": {\n' + '        "nomFichier": "userFemaleRedHair",\n' + '        "x": "550",\n' + '        "y": "150",\n' + '        "width": "40",\n' + '        "nom": "Jenifer"\n' + '      },\n' + '      "13": {\n' + '        "nomFichier": "boy",\n' + '        "x": "550",\n' + '        "y": "60",\n' + '        "width": "40",\n' + '        "nom": "Tom"\n' + '      },\n' + '      "14": {\n' + '        "nomFichier": "businessWoman",\n' + '        "x": "280",\n' + '        "y": "200",\n' + '        "width": "40",\n' + '        "nom": "Laure"\n' + '      },\n' + '      "15": {\n' + '        "nomFichier": "caroussel",\n' + '        "x": "360",\n' + '        "y": "60",\n' + '        "width": "40",\n' + '        "nom": "Place de jeu"\n' + '      },\n' + '      "16": {\n' + '        "nomFichier": "standingMan",\n' + '        "x": "450",\n' + '        "y": "380",\n' + '        "width": "64",\n' + '        "nom": "Jean"\n' + '      }\n' + '    }\n' + '  }\n' + '}';
        // const EXDEBASE =
        // console.log(EXDEBASE());

        let count = 1;




        //permet de mettre les images du scénario dnas le canvas
        for (let key in this.exercice.exerciceDeBase.pictures) {
            let imgNom = this.exercice.exerciceDeBase.pictures[count];
            let img = this.refGame.global.resources.getImage(imgNom.nomFichier).image.src;
            let aInserer = new ImgStatic(imgNom.x, imgNom.y, imgNom.width, img, imgNom.nom[0], false, "");
            this.elements.push(aInserer);
            count++;
        }
        //on met tout les élement dans le canvas
        this.elements.push(this.grid);
        this.elements.push(this.flechePoint1, this.fleche, this.fleche2, this.fleche3, this.fleche4, this.fleche5, this.flechPoint2);

        count = 1;



        this.init();
        this.tuto = false;

    }

    // RAM Le nom de la méthode n'est pas très explicite
    popUp(texte) {

        Swal.fire({
            title: name.toUpperCase(),
            width: 1000,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                //'<td style="padding-right: 30px">' + '<img src=' + imgSousCat + ' width="160px" height="160px"></td>' +
                '<td><div id="activites" style="text-align: justify">' + texte + '</div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    // RAM Il manque la description de la méthode
    handleNext() {

        let langue = "";


        switch (this.counterSteps) {
            //première fois qu'on pèse sur le bouton on change le texte d'intoduction et on montre le point de départ en rouge et le point d'aarivé en vert.
            case 1:
                langue = this.refreshLang(this.refGame.global.resources.getLanguage(), 0);
                // RAM Le texte doit être dans les traductions de la base de données
                this.refGame.global.util.setTutorialText("Tout d'abord, il te faudra trouver le point de <b style='color:green'>départ</b> et celui d'<b style='color:red'>arrivée</b> dans la consigne." + "<br>" + '<p style="font-size:20px;">' + "C'est mercredi, <b style='color:green'>Jean</b> est en route pour aller chez <b style='color:red'>Tom</b> qui l'a invité pour le goûter" + "</p>");
                //On rend visible les flèches d'arrivé et de départ
                this.fleche.setVisible(true);
                this.flechePoint1.setVisible(true);
                this.fleche5.setVisible(true);
                this.flechPoint2.setVisible(true);
                this.counterSteps = 2;
                break;
            //deuxième fois qu'on pèse sur le bouton on rechange le texte d'introduction pour montrer les points de passages qui se trouvent dans la consigne
            // puis on cache les flèche du cas précédant et on affiches les trois point de pasages
            case 2:
                let langue1 = this.refreshLang(this.refGame.global.resources.getLanguage(), 0);
                let langue2 = this.refreshLang(this.refGame.global.resources.getLanguage(), 1);
                let langue3 = this.refreshLang(this.refGame.global.resources.getLanguage(), 2);
                // RAM Le texte doit être dans les traductions de la base de données
                this.refGame.global.util.setTutorialText("En suite, il va faloir repérer les points de passages obligatoires présents dans la phrase." + '<p style="font-size:20px; color:blue;">' + langue1 + "</p>" + '<p style="font-size:20px; color:#2fff9f">' + langue2 + "</p>" + '<p style="font-size:20px; color:red">' + langue3 + "</p>");
                //on rend invisible les deux flèches précédante
                this.fleche.setVisible(false);
                this.flechePoint1.setVisible(false);
                this.fleche5.setVisible(false);
                this.flechPoint2.setVisible(false);


                //on rend visible les flèches des points de passages
                this.fleche2.setVisible(true);
                this.fleche3.setVisible(true);
                this.fleche4.setVisible(true);
                // RAM Le texte "Commencer" doit être dans les traductions de la base de données
                document.getElementById("next").setAttribute('value', "Commencer");
                this.counterSteps = 3;
                break;


            //la troisième fois qu'on pèse sur le bouton on rend le jeux jouable en affichant toute la consigne et en cachant les flèches du cas précédant
            case 3:
                this.fleche.setVisible(false);
                this.flechePoint1.setVisible(false);
                this.fleche2.setVisible(false);
                this.fleche3.setVisible(false);
                this.fleche4.setVisible(false);
                langue = this.refreshLang(this.refGame.global.resources.getLanguage(), 3);
                // RAM Le texte doit être dans les traductions de la base de données
                this.refGame.global.util.setTutorialText("Et maintenant c'est a ton tour ! Trace le chemin que Jean a emprunté pour aller chez Tom." + "<br>" + '<p style="font-size:20px;">' + langue + "</p>");

                //on affiche les boutons valider et réessayer et on leur met un event pour quand on clique dessus
                this.refGame.global.util.showTutorialInputs('btnValider', 'btnResetTr', "btnConfiguration");
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.handleValider.bind(this));
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnResetTr'), ['click'], this.handleReset.bind(this));

                //rend la grille de jeu utilisable
                this.grid.setBlockNode(false);
                // RAM Le texte "Recommencer" doit être dans les traductions de la base de données
                document.getElementById("next").setAttribute('value', "Recommencer");
                document.getElementById("next").setAttribute('onclick', "location.reload()");

                break;

            //dans le cas ou on pèse encore une fois après que le jeux sois lancé on relance la page pour recommancer le tuto.

        }

    }


    // RAM Il manque la description de la méthode
    handleAudioGame() {
        this.popUp(this.refGame.global.resources.getOtherText('exBaseTxt'));
    }
    // RAM Il manque la description de la méthode
    dessin(x, y, r, n, i) {
        this.graphics = new PIXI.Graphics();
        this.graphics.beginFill(0x12badb);
        this.rectangle = this.graphics.drawRoundedRect(x, y, r, n, i);
        this.graphics.visible = false;
        this.graphics.hitarea = this.rectangle;
        return this.graphics;
    }
    // RAM Il manque la description de la méthode
    boutonCarte(x, y, r, n, i) {
        let rectangle = new PIXI.Graphics();
        rectangle.beginFill(0xffffff);
        rectangle.drawRoundedRect(x, y, r, n, i);
        rectangle.visible = false;
        rectangle.hitarea = this.rectangle;
        rectangle.interactive = true;
        rectangle.buttonMode = true;
        return rectangle;
    }
    // RAM Il manque la description de la méthode
    handleValider() {

        if (this.evaluate) {
            // RAM est-ce que c'est des constantes ? Les variables de types constantes doivent être en majuscule et avec le type const
            // Initialisation des valeurs qu'on va utiliser
            let valueStartAndStop = 40; // 20 start and 20 stop
            let valueMin = 30;
            let valueMax = 60;
            let result = 0;
            // Il nous faut le pourcentage et les informations d'où il est passé ou non
            let resultReceive = this.grid.setOnShapeCreated(this.exercice.verif, this.arrayBoutons, false);
            // 0 = pourcentage, 1 = texte
            let text = resultReceive.split(",", 2);
            // on attribut les points selon le pourcentage obtenu
            if ((text[0] - valueStartAndStop) < valueMin) {
                result = 0;
            } else if ((text[0] - valueStartAndStop) >= valueMin && (text[0] - valueStartAndStop) < valueMax) {
                result = 1;
            } else {
                result = 2;
            }
            // On met tous les informations dans un String (this.gameid est l'id du jeu)
            let resultSend;
            this.refGame.global.statistics.updateStats(resultSend);

            if (result === 2) {
                if (document.getElementById("progressBar") != null) {
                    window.history.back();
                }
                Swal.fire(
                    {

                        title: this.refGame.global.resources.getOtherText('good'),
                        width: 500,
                        icon: 'success',
                        confirmButtonColor: BLEU,
                        confirmButtonText: 'OK',
                        showConfirmButton: true,
                        html: this.refGame.global.resources.getOtherText('goodWays'),

                    });


            } else {
                Swal.fire({
                    title: this.refGame.global.resources.getOtherText('errorPopup'),
                    width: 500,
                    icon: 'error',
                    confirmButtonColor: BLEU,
                    confirmButtonText: 'OK',
                    showConfirmButton: true,
                    html: this.refGame.global.resources.getOtherText('notGoodWaysTake')
                });
            }

            // On reset

            this.grid.reset();
            this.clear();
            this.elements = [];
            this.title = new PIXI.Text('', { fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center' });
            this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
            // this.startButton.setOnClick(this.newGame.bind(this));

            this.title.x = 300;
            this.title.y = 150;
            this.title.anchor.set(0.5);
            this.elements.push(this.title);
            this.elements.push(this.startButton);
            this.show(this.evaluate);
            /** On enlève la checkbox */
            this.refGame.global.util.hideTutorialInputs("btnLockMap");
            this.refGame.global.util.hideTutorialInputs("btnLockMapid");
        } else {
            /** On initialise la variable valueReturn */
            let valueReturn = "";

            /** On récupère la vérification du chemin envoyé */
            this.retour = this.grid.setOnShapeCreated(this.exercice.exerciceDeBase.verif, this.arrayBoutons, false);


            /** On vérifie si il a réussi ou non */
            if (this.retour !== 1) {
                valueReturn = this.retour.split(",");
            } else {
                valueReturn = this.retour;
            }

            /** On vérifie que toute est juste sinon on affiche une popup avec les erreurs */
            if (valueReturn === 1) {
                this.show(this.evaluate);

            } else {
                let resultPassage = valueReturn[1].split(";");
                let comptePassage = 0;

                /** On vérifie à partir du quel des chemins il s'est trompé */
                for (let i = 0; i < resultPassage.length - 1; i++) {
                    if (resultPassage[i] === "Ok") {
                        comptePassage++;
                    }
                }
                /** On initialise la variable 'tradError' */
                let tradError = "";
                /** On récupére les traductions */
                for (let i = comptePassage; i <= resultPassage.length - 1; i++) {
                    // RAM c'est quoi la différence entre le if et le else if ?
                    if (i === resultPassage.length - 1) {
                        switch (this.refGame.global.resources.getLanguage()) {
                            case "fr":
                                tradError += this.exercice.error[i].fr + "";
                                break;
                            case "de":
                                tradError += this.exercice.error[i].de + " ";
                                break;
                            case "it":
                                tradError += this.exercice.error[i].it + " ";
                                break;
                            case "en":
                                tradError += this.exercice.error[i].en + " ";
                                break;
                        }
                    } else {
                        switch (this.refGame.global.resources.getLanguage()) {
                            case "fr":
                                tradError += this.exercice.error[i].fr + ", ";
                                break;
                            case "de":
                                tradError += this.exercice.error[i].de + ", ";
                                break;
                            case "it":
                                tradError += this.exercice.error[i].it + ", ";
                                break;
                            case "en":
                                tradError += this.exercice.error[i].en + ", ";
                                break;
                        }
                    }

                }
                /** On envoie les traductions dans la méthode pour la popup */
                this.popUpError(this.refGame.global.resources.getOtherText('errorLocation'), tradError);
            }
            /** On enlève les checkboxs */
            this.refGame.global.util.hideTutorialInputs("btnLockMap");
            this.refGame.global.util.hideTutorialInputs("btnLockMapid");
            this.refGame.global.util.hideTutorialInputs("btnLockWays");
            this.refGame.global.util.hideTutorialInputs("btnLockWaysid");
        }

    }



    // RAM Manque la description de la méthode
    refreshLang(lang, nbr) {
        let result;
        // RAM Es-ce que l'on pourrait pas simplifier le code ?
        switch (nbr) {
            case 0:
                switch (lang) {
                    case "en":
                        result = this.exercice.exerciceDeBase.error[0].en;
                        break;
                    case "it":
                        result = this.exercice.exerciceDeBase.error[0].it;
                        break;
                    case "de":
                        result = this.exercice.exerciceDeBase.error[0].de;
                        break;
                    case "fr":
                        result = this.exercice.exerciceDeBase.error[0].fr;
                        break;
                }
                break;
            case 1:
                switch (lang) {
                    case "en":
                        result = this.exercice.exerciceDeBase.error[1].en;
                        break;
                    case "it":
                        result = this.exercice.exerciceDeBase.error[1].it;
                        break;
                    case "de":
                        result = this.exercice.exerciceDeBase.error[1].de;
                        break;
                    case "fr":
                        result = this.exercice.exerciceDeBase.error[1].fr;
                        break;
                }
                break;
            case 2:
                switch (lang) {
                    case "en":
                        result = this.exercice.exerciceDeBase.error[2].en;
                        break;
                    case "it":
                        result = this.exercice.exerciceDeBase.error[2].it;
                        break;
                    case "de":
                        result = this.exercice.exerciceDeBase.error[2].de;
                        break;
                    case "fr":
                        result = this.exercice.exerciceDeBase.error[2].fr;
                        break;
                }
                break;

            case 3:
                switch (lang) {
                    case "en":
                        result = this.exercice.exerciceDeBase.translate.en;
                        break;
                    case "it":
                        result = this.exercice.exerciceDeBase.translate.it;
                        break;
                    case "de":
                        result = this.exercice.exerciceDeBase.translate.de;
                        break;
                    case "fr":
                        result = this.exercice.exerciceDeBase.translate.fr;
                        break;
                }
                break;
        }

        return result;
    }
    // RAM Manque la description de la méthode
    handleReset() {
        this.grid.onReset(this.arrayBoutons);
    }

    // RAM Manque la description de la méthode
    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }
    // RAM Manque la description de la méthode
    popUpWarning(texte) {
        Swal.fire({
            title: name.toUpperCase(),
            width: 350,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td><div id="activites" style="text-align: justify">' + texte + '</div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }
    // RAM Manque la description de la méthode
    popUpError(error, message) {
        Swal.fire({
            title: error,
            width: 500,
            icon: 'error',
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html: '<span style="color:red">' + message + '</span>'
        });
    }


    //rend la grille visible et invisible quand on pèse sur la checkBox
    setMapLock() {
        if (this.maplock) {
            this.maplock = false;
            this.grid.setVisiblePoint(this.maplock);
            document.getElementById('btnLockMap').className = "btn btn-secondary";
        } else {
            this.maplock = true;
            this.grid.setVisiblePoint(this.maplock);
            document.getElementById('btnLockMap').className = "btn btn-success";
        }
    }

    // RAM Manque la description de la méthode
    handleLockMap() {
        this.setMapLock();
    }

    //permet d'activer / desactiver le controlle a la fin ud tuto
    setWaysLock() {
        if (this.grid.blockVisibleWays) {
            this.popUpWarning(this.refGame.global.resources.getOtherText("impossiblePass"));
            document.getElementById('btnLockWays').checked = !this.correctWays;
        } else {
            if (!this.correctWays) {
                this.correctWays = true;
                this.grid.setVisibleCorrectWays(this.correctWays);
            } else {
                this.correctWays = false;
                this.grid.setVisibleCorrectWays(this.correctWays);
            }
        }
    }

    // RAM Manque la description de la méthode
    handleLockWays() {
        this.setWaysLock();
    }


}
class Play extends Interface {

    constructor(refGame, refTrainMode) {

        super(refGame, "play");

        this.clear();
        this.elements = [];

        this.exercices = {};

        this.selectedGameId = -1;

        this.refTrainMode = refTrainMode;

        this.title = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left'});
        this.scrollPane = new ScrollPane(0, 60, 600, 480);
        this.btn = new Button(300, 570, '', 0x007bff, 0xFFFFFF, true);

        this.title.anchor.set(0.5);
        this.title.x = 300;
        this.title.y = 30;

         this.btn.setOnClick(function () {
            if (this.selectedGameId < 0)
                return;
            this.playExerice(this.selectedGameId);
        }.bind(this));


        this.elements.push(this.title);
        this.elements.push(this.scrollPane);
        this.elements.push(this.btn);
    }

    init() {
        this.refGame.global.util.hideTutorialInputs('btnReset', "next");
        super.init();
    }

    show() {
        this.clear();

        this.exercices = this.refGame.global.resources.getExercicesEleves();
        if (this.exercices) this.exercices = JSON.parse(this.exercices);

        this.refreshLang(this.refGame.global.resources.getLanguage());

        this.setElements(this.elements);

        this.init();
    }

    refreshLang(lang) {
        this.generateScrollPane(lang);
        this.refGame.showText(this.refGame.global.resources.getOtherText("play"));
        this.btn.setText(this.refGame.global.resources.getOtherText('playBtn'));

        this.elements.push(this.title);
        this.elements.push(this.scrollPane);
        this.elements.push(this.btn);
    }

    refreshFont(isOpenDyslexic){

    }

    playExerice(exId) {
        this.refGame.trainMode.show(this.exercices[exId]);
    }

    generateScrollPane(lang) {
        if (!this.exercices)
            return;
        this.scrollPane.clear();
        let tg = new ToggleGroup('single');
        for (let exID of Object.keys(this.exercices)) {
            let ex = this.exercices[exID];
            let label = this.refGame.global.resources.getOtherText('routePlayer') + ex.name;
            let btn = new ToggleButton(0, 0, label, false, 580);
            btn.enable();
//            btn = new Button(0, 0, label, 0x00AA00, 0xFFFFFF, false, 580);
            tg.addButton(btn);

            btn.setOnClick(function () {
                this.selectedGameId = exID;
            }.bind(this));
            this.scrollPane.addElements(btn);
        }
        this.scrollPane.init();

        this.elements.push(this.title);
        this.elements.push(this.scrollPane);
        this.elements.push(this.btn);
    }
}
class Train extends Interface {

    constructor(refGame) {
        super(refGame, "train");
        this.refGame = refGame;

        this.exercice = null;
        this.evaluate = false;

        this.title = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
        this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
        this.startButton.setOnClick(this.newGame.bind(this));

        this.title.x = 300;
        this.title.y = 150;
        this.title.anchor.set(0.5);

    }

    show(evaluate, exercice) {
        document.getElementById("btnAudioGame").style="display: none;";
        document.getElementById("btnLockMap").style="display: none;";
        this.gameid = 11;

        this.exerciceGive = exercice;
        for (let i = 0; i < this.elements.length; i++) {
            if (typeof this.elements[i].origin !== "undefined") {
                this.elements[i].reset();
            }
        }

        this.clear();
        this.elements = [];

        this.elements.push(this.title);
        this.elements.push(this.startButton);
        this.evaluate = evaluate;

        // suite
        this.refGame.global.util.hideTutorialInputs('next', 'btnReset', 'tuto1', 'tuto2', 'tuto3', "btnLockWays", 'btnValider', 'btnResetTr', "btnConfiguration");
        this.refreshLang(this.refGame.global.resources.getLanguage());

        this.init();
    }


    refreshLang(lang) {
        if (this.evaluate) {
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('welcomeEval'));
            this.title.text = this.refGame.global.resources.getOtherText('welcomeEval');
        } else {
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('welcomeTrain'));
            this.title.text = this.refGame.global.resources.getOtherText('welcomeTrain');
        }
        this.startButton.setText(this.refGame.global.resources.getOtherText('btnStartCreate'));
        this.startButton.setVisible(true);

    }

    /**
     * Methode qui permet changer le texte de l'interieur du canvas quand on choisis la police OpenDyslexic
     * @param isOpenDyslexic {boolean} Est opendyslexic ou pas
     */
    refreshFont(isOpenDyslexic) {

    }

    newGame(){

        this.elements.unshift(this.title);
        this.elements.unshift(this.startButton);

        this.title.text = "";
        this.startButton.setVisible(false);

        if(typeof this.exerciceGive === "undefined"){
            this.exercice = this.refGame.global.resources.getExercice();
            if (forced){
                console.log("forced : "+forced);
                this.exercice = this.refGame.global.resources.getExerciceById(forced);
            }
            else{
                console.log("Not forced");
                this.exercice = this.refGame.global.resources.getExercice();
            }
            // this.exercice = this.refGame.global.resources.getExercice();
            this.currentExerciceId = this.exercice.id;

            // résoudre problème de retour à la ligne dans le JSON
            const array = Array.from(this.exercice.exercice);
            const arrayWithoutCarriageReturn = array.map((char) => {
                return char === '\n' ? 'RETURN' : char
            });
            this.exercice.exercice = arrayWithoutCarriageReturn.join('');

            this.exercice = JSON.parse(this.exercice.exercice);
            } else {
            this.exercice = this.exerciceGive;
            this.exercice = this.exercice.exercice;
        }

        // Affichage ou non de la checkbox pour bloquer le changement de couleur lors de la correction
        if(this.evaluate){
            // Cache les anciennes informations du mode "Explorer"
            this.refGame.global.util.hideTutorialInputs('next', 'btnReset', 'tuto1', 'tuto2', 'tuto3', "btnLockWays");
            // Permet d'afficher les boutons "Valider", "Réessayer" ...
            this.refGame.global.util.showTutorialInputs('btnValider', 'btnResetTr', "btnConfiguration");
            document.getElementById("tutorial").className='smoothie ';
            this.refGame.global.util.setTutorialText("<p class='scroller'>"+this.refGame.global.resources.getOtherText("tutoEval")+"</p>"+'<p style="font-size:20px;" class="scroller">'+this.exercice.translate.fr+"</p>");
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.handleValider.bind(this));
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnResetTr'), ['click'], this.handleReset.bind(this));
            this.refGame.global.util.hideTutorialInputs("btnLockWays");
            // Affichage ou non de la checkbox pour afficher les points
            // Permet d'afficher les boutons "Valider", "Réessayer" ...
            this.refGame.global.util.showTutorialInputs('btnValider', 'btnResetTr', "btnConfiguration");
            if(this.exercice.enableLockMap){
                this.refGame.global.util.showTutorialInputs("btnLockMap");
                this.refGame.global.util.showTutorialInputs("btnLockMapid");
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnLockMap'), ['click'], this.handleLockMap.bind(this));
            } else {
                this.refGame.global.util.hideTutorialInputs("btnLockMap");
            }
                // Modifier le texte qui va être lû par le bouton "Son"
               // this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.handleValider.bind(this));
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnResetTr'), ['click'], this.handleReset.bind(this));

            // On initialise la partie évaluation
            this.refGame.global.statistics.addStats(2);
        } else {
            // Permet d'afficher les boutons "Valider", "Réessayer" ...
            this.refGame.global.util.showTutorialInputs('btnValider', 'btnResetTr', "btnConfiguration");
            if(this.exercice.enableLockWays){
                this.refGame.global.util.showTutorialInputs("btnLockWays");
                this.refGame.global.util.showTutorialInputs("btnLockWaysid");
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnLockWays'), ['click'], this.handleLockWays.bind(this));
            } else {
                this.refGame.global.util.hideTutorialInputs("btnLockWays");
                this.refGame.global.util.showTutorialInputs("btnLockWaysid");
            }
            if(this.exercice.enableLockMap){
                this.refGame.global.util.showTutorialInputs("btnLockMap");
                this.refGame.global.util.showTutorialInputs("btnLockMapid");
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnLockMap'), ['click'], this.handleLockMap.bind(this));
            } else {
                this.refGame.global.util.hideTutorialInputs("btnLockMap");
                this.refGame.global.util.showTutorialInputs("btnLockMapid");
            }
            // Modifier le texte qui va être lû par le bouton "Son"
          //  let lang = this.refGame.global.resources.getLanguage();
           // let texte="this.exercice.translate."+this.refGame.global.resources.getLanguage();

          
            console.log(this.refGame.global.resources.getLanguage());

            let regex = "RETURN";
            this.langue=this.changeLang(this.refGame.global.resources.getLanguage());
            this.refGame.global.util.setTutorialText('<p style="font-size:20px;" class="scroller">'+this.langue.replaceAll(regex,'<br>')+"</p>");
            document.getElementById("tutorial").className='smoothie ';
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.handleValider.bind(this));
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnResetTr'), ['click'], this.handleReset.bind(this));
        }
        this.grid = new DrawingGrid(11, 9, 50, this.scene);
        this.grid.setRefGameDG(this.refGame);
        this.grid.reset();

        //background
        // let back = this.exercice.background;
        // let backImag = this.refGame.global.resources.getImage('background').image.src;
        // let insererBack = new ImgBack(back.x, back.y, back.width, back.height, backImag, 'background');
        // this.elements.push(insererBack, this.grid);
        // this.elements.push(insererBack);

        //images
        let count = 1;
        let lang = this.refGame.global.resources.getLanguage();
        let nbre;
        switch(lang){
            case "fr" :
                nbre = 0;
                break;
            case "de" :
                nbre = 1;
                break;
            case "it" :
                nbre = 2;
                break;
            case "en" :
                nbre = 3;
                break;
        }
        for (let key in this.exercice.pictures) {
            let imgNom = this.exercice.pictures[count];
            let img = this.refGame.global.resources.getImage(imgNom.nomFichier).image.src;
            let aInserer = new ImgStatic(imgNom.x, imgNom.y, imgNom.width, img, imgNom.nom[nbre], false, "");
            this.elements.push(aInserer);
            count++;
        }
        this.elements.push(this.grid);
        count = 1;

        this.tts = this.refGame.global.resources.getImage('speaker').image.src;

        const json = JSON.stringify(this.exercice.translate)
        const words = json.split(/(RETURN)/);
        //console.log('words: ', words)
        const arrayWithCarriageReturn = words.map((word) => {
            return word === 'RETURN' ? '\\n' : word
        });
        this.exercice.translate = arrayWithCarriageReturn.join('');
        this.exercice.translate = JSON.parse(this.exercice.translate);

        switch (this.refGame.global.resources.getLanguage()) {
            case "fr" :
                this.texte = new PIXI.Text(this.exercice.translate.fr, {
                    // fontFamily: 'Arial',
                    fontSize: 18,
                    fill: 0x000000,
                    align: 'left',
                    wordWrap: true,
                    wordWrapWidth: 580
                });
                this.ttsImg = new ImgStatic(50, 455, 32, this.tts, "", true, this.exercice.translate.fr);
                break;
            case "de" :
                this.texte = new PIXI.Text(this.exercice.translate.de, {
                    // fontFamily: 'Arial',
                    fontSize: 18,
                    fill: 0x000000,
                    align: 'left',
                    wordWrap: true,
                    wordWrapWidth: 580
                });
                this.ttsImg = new ImgStatic(50, 455, 32, this.tts, "", true, this.exercice.translate.de);
                break;
            case "it" :
                this.texte = new PIXI.Text(this.exercice.translate.it, {
                    // fontFamily: 'Arial',
                    fontSize: 18,
                    fill: 0x000000,
                    align: 'left',
                    wordWrap: true,
                    wordWrapWidth: 580
                });
                this.ttsImg = new ImgStatic(50, 455, 32, this.tts, "", true, this.exercice.translate.it);
                break;
            case "en" :
                this.texte = new PIXI.Text(this.exercice.translate.en, {
                    // fontFamily: 'Arial',
                    fontSize: 18,
                    fill: 0x000000,
                    align: 'left',
                    wordWrap: true,
                    wordWrapWidth: 580
                });
                this.ttsImg = new ImgStatic(50, 455, 32, this.tts, "", true, this.exercice.translate.en);
                break;

        }




       // this.texte.x = 300;
       // this.texte.y = 515;
       // this.texte.anchor.set(0.5);


        //this.texte, this.ttsImg
       // this.elements.push(this.texte, this.ttsImg);
       // this.elements.push(this.ttsImg);

        /** valeur des variables lock */
        this.correctWays = true;
        this.grid.setVisibleCorrectWays(this.correctWays);
        this.maplock = false;
        this.grid.setVisiblePoint(this.maplock);

        this.init();
    }

    changeLang(lang){

        console.log("testLangue")
        let langReturn="";
        switch (lang) {
            case "fr":
                langReturn=this.exercice.translate.fr;
                break;
            case "de":
                langReturn=this.exercice.translate.de;
                break;
            case "en":
                langReturn=this.exercice.translate.en;
                break;
            case "it":
                langReturn=this.exercice.translate.it;
                break;
        }
        return langReturn;
    }

    popUpWarning(texte){
        Swal.fire({
            title: name.toUpperCase(),
            width: 350,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td><div id="activites" style="text-align: justify">' + texte + '</div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    popUpError(error, message){
        Swal.fire({
            title: error,
            width: 500,
            icon: 'error',
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html: '<span style="color:red">' + message + '</span>'
        });
    }

    setWaysLock(){
        if(this.grid.blockVisibleWays){
            this.popUpWarning(this.refGame.global.resources.getOtherText("impossiblePass"));
            document.getElementById('btnLockWays').checked = !this.correctWays;
        } else {
            if(!this.correctWays){
                this.correctWays = true;
                this.grid.setVisibleCorrectWays(this.correctWays);
            } else {
                this.correctWays = false;
                this.grid.setVisibleCorrectWays(this.correctWays);
            }
        }
    }

    setMapLock(){
        if(this.maplock){
            this.maplock = false;
            this.grid.setVisiblePoint(this.maplock);
           // document.getElementById('btnLockMap').className = "btn btn-secondary";
        } else {
            this.maplock = true;
            this.grid.setVisiblePoint(this.maplock);
            //document.getElementById('btnLockMap').className = "btn btn-success";
        }
    }

    handleValider() {
        if(this.evaluate){
            // Initialisation des valeurs qu'on va utiliser
            let valueStartAndStop = 40; // 20 start and 20 stop
            let valueMin = 30;
            let valueMax = 60;
            let result = 0;
            // Il nous faut le pourcentage et les informations d'où il est passé ou non

            let resultReceive = this.grid.setOnShapeCreated(this.exercice.verif, this.arrayBoutons, this.evaluate);
            // 0 = pourcentage, 1 = texte
            let text = resultReceive.split(",", 2);
            // on attribut les points selon le pourcentage obtenu
            if((text[0] - valueStartAndStop)  < valueMin){
                result = 0;
            } else if ((text[0] - valueStartAndStop) >= valueMin && (text[0] - valueStartAndStop) < valueMax){
                result = 1;
            } else {
                result = 2;
            }
            // On met tous les informations dans un String (this.gameid est l'id du jeu)
            let resultSend = result;
            this.refGame.global.statistics.updateStats(resultSend);
            if(result === 2){

                Swal.fire({
                    title: this.refGame.global.resources.getOtherText('good'),
                    width: 500,
                    icon: 'success',
                    confirmButtonColor: BLEU,
                    confirmButtonText: 'OK',
                    showConfirmButton: true,
                    html: this.refGame.global.resources.getOtherText('goodWays')
                })
            } else {
                Swal.fire({
                    title: this.refGame.global.resources.getOtherText('errorPopup'),
                    width: 500,
                    icon: 'error',
                    confirmButtonColor: BLEU,
                    confirmButtonText: 'OK',
                    showConfirmButton: true,
                    html: this.refGame.global.resources.getOtherText('notGoodWaysTake')
                })
            }

            // On reset
            this.grid.reset();
            this.clear();
            this.elements = [];
            this.title = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
            this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
            this.startButton.setOnClick(this.newGame.bind(this));

            this.title.x = 300;
            this.title.y = 150;
            this.title.anchor.set(0.5);
            this.elements.push(this.title);
            this.elements.push(this.startButton);
            this.show(this.evaluate);
            /** On enlève la checkbox */
            this.refGame.global.util.hideTutorialInputs("btnLockMap");
            this.refGame.global.util.hideTutorialInputs("btnLockMapid");
        } else {
            /** On initialise la variable valueReturn */
            let valueReturn = "";

            /** On récupère la vérification du chemin envoyé */
            this.retour = this.grid.setOnShapeCreated(this.exercice.verif, this.arrayBoutons, this.evaluate);

            /** On vérifie si il a réussi ou non */
            if(this.retour !== 1){
                valueReturn = this.retour.split(",");
            } else {
                valueReturn = this.retour;
            }

            /** On vérifie que toute est juste sinon on affiche une popup avec les erreurs */
            if(valueReturn === 1){
                this.grid.reset();
                this.clear();
                this.elements = [];
                this.title = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
                this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
                this.startButton.setOnClick(this.newGame.bind(this));
                this.title.x = 300;
                this.title.y = 150;
                this.title.anchor.set(0.5);
                this.elements.push(this.title);
                this.elements.push(this.startButton);
                this.show(this.evaluate);
            } else {
                let resultPassage = valueReturn[1].split(";");
                let comptePassage = 0;

                /** On vérifie à partir du quel des chemins il s'est trompé */
                for (let i = 0; i < resultPassage.length-1 ; i++) {
                    if(resultPassage[i] === "Ok"){
                        comptePassage++;
                    }
                }
                /** On initialise la variable 'tradError' */
                let tradError = "";
                /** On récupére les traductions */
                for (let i = comptePassage; i <= resultPassage.length-1; i++) {
                    if(i === resultPassage.length-1){
                        switch (this.refGame.global.resources.getLanguage()) {
                            case "fr" :
                                tradError += this.exercice.error[i].fr + "";
                                break;
                            case "de" :
                                tradError += this.exercice.error[i].de + " ";
                                break;
                            case "it" :
                                tradError += this.exercice.error[i].it + " ";
                                break;
                            case "en" :
                                tradError += this.exercice.error[i].en + " ";
                                break;
                        }
                    } else {
                        switch (this.refGame.global.resources.getLanguage()) {
                            case "fr" :
                                tradError += this.exercice.error[i].fr + ", ";
                                break;
                            case "de" :
                                tradError += this.exercice.error[i].de + ", ";
                                break;
                            case "it" :
                                tradError += this.exercice.error[i].it + ", ";
                                break;
                            case "en" :
                                tradError += this.exercice.error[i].en + ", ";
                                break;
                        }
                    }

                }
                /** On envoie les traductions dans la méthode pour la popup */
                this.popUpError(this.refGame.global.resources.getOtherText('errorLocation'), tradError);
            }
            /** On enlève les checkboxs */
            this.refGame.global.util.hideTutorialInputs("btnLockMap");
            this.refGame.global.util.hideTutorialInputs("btnLockMapid");
            this.refGame.global.util.hideTutorialInputs("btnLockWays");
            this.refGame.global.util.hideTutorialInputs("btnLockWaysid");
        }
    }

    handleReset() {
        this.grid.onReset(this.arrayBoutons);
    }

    handleLockMap(){
        this.setMapLock();
    }

    handleLockWays(){
        this.setWaysLock();
    }

    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }

}
/**
 * @classdesc Représente un mode de jeu
 * @author Vincent Audergon
 * @version 1.0
 */
class Mode {

    /**
     * Constructeur d'un mode de jeu
     * @param {string} name le nom du mode de jeu (référencé dans le JSON de langues)
     */
    constructor(name) {
        this.name = name;
        this.interfaces = [];
        this.texts = [];
    }

    /**
     * Défini la liste des interfaces utilisées par le mode de jeu
     * @param {Interface[]} interfaces la liste des interfaces
     */
    setInterfaces(interfaces) {
        this.interfaces = interfaces;
    }

    /**
     * Ajoute une interface au mode de jeu
     * @param {string} name le nom de l'interface
     * @param {Interface} inter l'interface
     */
    addInterface(name, inter) {
        this.interfaces[name] = inter;
    }

    /**
     * Retourne un texte dans la langue courrante
     * @param {string} key la clé du texte
     * @return {string} le texte
     */
    getText(key){
        return this.texts[key];
    }

    /**
     * Méthode de callback appelée lors d'un changement de langue.
     * Indique à toutes les interfaces du mode de jeu de changer les textes en fonction de la nouvelle langue
     * @param {string} lang la langue
     */
    onLanguageChanged(lang) {
        this.texts = this.refGame.global.resources.getOtherText(this.name)
        for (let i in this.interfaces) {
            let inter = this.interfaces[i];
            if (inter instanceof Interface) {
                inter.setTexts(this.texts);
                inter.refreshLang(lang);
            }
        }
    }

}

class CreateMode extends Mode{

    constructor(refGame){
        super('create');
        this.refGame = refGame;
        this.setInterfaces({
            create: new Create(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.create.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.create.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.create.refreshFont(isOpendyslexic)
    }
}

class ExploreMode extends Mode{

    constructor(refGame){
        super('explore');
        this.refGame = refGame;
        this.setInterfaces({
            explore: new Explore(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.explore.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.explore.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.explore.refreshFont(isOpendyslexic)
    }
}

class PlayMode extends Mode{

    constructor(refGame){
        super('play');
        this.refGame = refGame;
        this.setInterfaces({
            play: new Play(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(){
    }

    /**
     * Affiche la page de garde de la création
     */
    show(){
        this.interfaces.play.show();
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.play.refreshLang(lang);
    }


    onFontChange(isOpendyslexic){
        this.interfaces.play.refreshFont(isOpendyslexic)
    }
}
class TrainMode extends Mode {

    constructor(refGame) {
        super('train');
        this.refGame = refGame;
        this.evaluate = false;
        this.setInterfaces({
            train: new Train(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(evaluate) {
        this.evaluate = evaluate;
    }

    /**
     * Affiche la page de garde de la création
     */
    show(exercice = undefined) {
        this.interfaces.train.show(this.evaluate, exercice);


       //  console.log(this.exercice);
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.train.refreshLang(lang);
    }


    onFontChange(isOpendyslexic) {
        this.interfaces.train.refreshFont(isOpendyslexic)
    }


    grille() {
        var x = []
        var y = []
    }
}
/**
 * @classdesc Classe principale du jeu
 * @author Vincent Audergon
 * @version 1.0
 */

const VERSION = 1.0;

class Game {

    /**
     * Constructeur du jeu
     * @param {Object} global Un objet contenant les différents objets du Framework
     */
    constructor(global) {
        this.global = global;
        this.global.util.callOnGamemodeChange(this.onGamemodeChanged.bind(this));
        this.global.resources.callOnLanguageChange(this.onLanguageChanged.bind(this));
        this.global.resources.callOnFontChange(this.onFontChange.bind(this));
        this.scenes = {
            explore:new PIXI.Container(),
            train:new PIXI.Container(),
            create:new PIXI.Container(),
            play:new PIXI.Container()
        };
        this.exploreMode = new ExploreMode(this);
        this.trainMode = new TrainMode(this);
        this.createMode = new CreateMode(this);
        this.playMode = new PlayMode(this);
        this.oldGamemode = undefined;

    }

    /**
     * Affiche une scène sur le canvas
     * @param {PIXI.Container} scene La scène à afficher
     */
    showScene(scene) {
        this.reset();
        this.global.pixiApp.stage.addChild(scene);
    }

    /**
     * Retire toutes les scènes du stage PIXI
     */
    reset() {
        for (let scene in this.scenes) {
            this.global.pixiApp.stage.removeChild(this.scenes[scene])
        }
    }

    /**
     * Affiche du texte sur la page (à gauche du canvas)
     * @param {string} text Le texte à afficher
     */
    showText(text) {
        this.global.util.setTutorialText(text);
    }

    /**
     * Fonction de callback appelée lors d'un chagement de langue.
     * Indiques à tous les modes de jeu le changement de langue
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.exploreMode.onLanguageChanged(lang);
        this.trainMode.onLanguageChanged(lang);
        this.createMode.onLanguageChanged(lang);
        this.playMode.onLanguageChanged(lang);
    }


    onFontChange(isOpendyslexic) {
        this.createMode.onFontChange(isOpendyslexic);
        this.exploreMode.onFontChange(isOpendyslexic);
        this.trainMode.onFontChange(isOpendyslexic);
        this.playMode.onFontChange(isOpendyslexic);
    }

    /**
     * Listener appelé par le Framework lorsque le mode de jeu change.
     * Démarre le bon mode de jeu en fonction de celui qui est choisi
     * @async
     */
    async onGamemodeChanged() {
        let gamemode = this.global.util.getGamemode();
        /** Nous créeons toutes les modes pour que l'option "Opendyslexic" fonctionne sans bug
        this.trainMode.show();
        this.createMode.show();
        this.playMode.show();
         this.exploreMode.show();
         */


        switch (gamemode) {
            case this.global.Gamemode.Evaluate:
                this.trainMode.init(true);
                this.trainMode.show();
                break;
            case this.global.Gamemode.Train:
                this.trainMode.init(false);
                this.trainMode.show();
                break;
            case this.global.Gamemode.Explore:
                this.exploreMode.init();
                this.exploreMode.show();
                break;
            case this.global.Gamemode.Create:
                this.createMode.init();
                this.createMode.show();
                break;
            case this.global.Gamemode.Play:
                this.playMode.init();
                this.playMode.show();
                break;
        }
        this.oldGamemode = gamemode;
    }
}
const global = {};
global.canvas = Canvas.getInstance();
global.Log = Log;
global.util = Util.getInstance();
global.pixiApp = new PixiFramework().getApp();
global.resources = Resources;
global.statistics = Statistics;
global.Gamemode = Gamemode;
global.listenerManager = ListenerManager.getInstance();
const game = new Game(global);
