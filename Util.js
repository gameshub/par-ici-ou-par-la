define(["require", "exports", "./Gamemode", "./Statistics"], function (require, exports, Gamemode_1, Statistics) {
    "use strict";
    var Util = (function () {
        function Util() {
            var _this = this;
            this.div = document.getElementById('tutorial');
            this.gamemode = Gamemode_1.Gamemode.Explore;
            $('#onclick_gamemode_explore').on('click', function () {
                if (_this.gamemode !== Gamemode_1.Gamemode.Explore) {
                    _this.gamemode = Gamemode_1.Gamemode.Explore;
                    _this.onGameDataChanged();
                }
            });
            $('#onclick_gamemode_train').on('click', function () {
                if (_this.gamemode !== Gamemode_1.Gamemode.Train) {
                    _this.gamemode = Gamemode_1.Gamemode.Train;
                    _this.onGameDataChanged();
                }
            });
            $('#onclick_gamemode_evaluate').on('click', function () {
                if (_this.gamemode !== Gamemode_1.Gamemode.Evaluate) {
                    _this.gamemode = Gamemode_1.Gamemode.Evaluate;
                    _this.onGameDataChanged();
                }
            });
            $('#onclick_gamemode_create').on('click', function () {
                if (_this.gamemode !== Gamemode_1.Gamemode.Create) {
                    _this.gamemode = Gamemode_1.Gamemode.Create;
                    _this.onGameDataChanged();
                }
            });
            try {
                $('#onclick_gamemode_play').on('click', function () {
                    if (_this.gamemode !== Gamemode_1.Gamemode.Play) {
                        _this.gamemode = Gamemode_1.Gamemode.Play;
                        _this.onGameDataChanged();
                    }
                });
            } catch (e) {
                console.log(e);
            }
        }
        Util.getInstance = function () {
            if (!this.instance) {
                this.instance = new Util();
            }
            return this.instance;
        };
        Util.prototype.setTutorialParams = function (options) {
            if (options) {
                if (options.fontSize)
                    this.div.style.fontSize = options.fontSize;
                if (options.fontStyle)
                    this.div.style.fontStyle = options.fontStyle;
                if (options.fontFamily)
                    this.div.style.fontFamily = options.fontFamily;
                if (options.fontWeight)
                    this.div.style.fontWeight = options.fontWeight;
                if (options.color)
                    this.div.style.color = options.color;
                if (options.textDecoration)
                    this.div.style.textDecoration = options.textDecoration;
            }
        };
        Util.prototype.setTutorialText = function (text) {
            this.div.innerHTML = text;
        };
        Util.prototype.showTutorialInputs = function () {
            var ids = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                ids[_i] = arguments[_i];
            }
            for (var _a = 0, ids_1 = ids; _a < ids_1.length; _a++) {
                var id = ids_1[_a];
                $("#" + id).show();
            }
        };
        Util.prototype.hideTutorialInputs = function () {
            var ids = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                ids[_i] = arguments[_i];
            }
            for (var _a = 0, ids_2 = ids; _a < ids_2.length; _a++) {
                var id = ids_2[_a];
                $("#" + id).hide();
            }
        };
        Util.prototype.showAlert = function (type, text, title, footer, callback) {
            Swal.fire({
                icon: type,
                title: title || '',
                text: text,
                footer: footer || '',
                onClose: function () {
                    if (callback)
                        callback();
                }
            });
        };
        Util.prototype.showAlertPersonnalized = function (options) {
            Swal.fire(options);
        };
        Util.prototype.preventDefault = function (e) {
            e.preventDefault();
        };
        Util.prototype.globalizeEvent = function (e) {
            if (e.touches && e.touches[0]) {
                e.clientX = e.touches[0].clientX;
                e.clientY = e.touches[0].clientY;
            }
            if (!e.clientX || !e.clientY) {
                if (e.originalEvent) {
                    e.clientX = e.originalEvent.clientX;
                    e.clientY = e.originalEvent.clientY;
                }
            }
            if (e.type === 'touchmove' || e.type === 'touchstart' || e.buttons === 1) {
                e.isPushed = true;
            }
            else {
                e.isPushed = false;
            }
            return e;
        };
        Util.prototype.callOnGamemodeChange = function (callback) {
            this.gamemodeChangedCallback = callback;
        };
        Util.prototype.onGameDataChanged = function () {
            // if (this.gamemode === Gamemode_1.Gamemode.Train) {
            //     Statistics.addStats(1);
            // }
            // else if (this.gamemode === Gamemode_1.Gamemode.Evaluate) {
            //     Statistics.addStats(2);
            // }
            if (this.gamemodeChangedCallback)
                this.gamemodeChangedCallback();
        };
        Util.prototype.getGamemode = function () { return this.gamemode; };
        Util.prototype.setGamemode = function (gamemode) { this.gamemode = gamemode; };
        ;
        return Util;
    }());
    return Util;
});
