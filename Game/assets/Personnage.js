class Personnage extends Asset {
    constructor(x, y, width, height, imgBackground, name, onMove, onDBClick) {
        super();
        this.y = y;
        this.x = x;
        this.width = width;
        this.height = height;
        this.imgBackground = imgBackground;
        this.name = name;
        this.onMove = onMove;
        this.onDBClick = onDBClick;
        this.container = new PIXI.Container();
        this.isFirstClick = true;
        this.data = undefined;
        this.personne = undefined;
        this.init();
    }

    init() {
        this.container.removeChildren();
        this.personne = PIXI.Sprite.fromImage(this.imgBackground);
        this.personne.anchor.x = 0.5;
        this.personne.anchor.y = 0.5;
        this.personne.x = this.x;
        this.personne.y = this.y;
        this.personne.width = this.width;
        this.personne.height = this.height;

        this.personne.interactive = true;
        this.personne.on('pointerdown', this.onClick.bind(this));
        this.personne.on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));
        this.container.addChild(this.personne);
    }

    onClick() {
        if (this.isFirstClick) {
            this.isFirstClick = false;
            setTimeout(function () {
                this.isFirstClick = true;
            }.bind(this), 500)
        } else {
            this.isFirstClick = true;
            if (this.onDBClick) {
                this.onDBClick(this);
            }
        }
    }

    onDragStart(event) {
        this.data = event.data;
    }

    onDragEnd() {
        this.data = null;
    }

    onDragMove() {
        if (this.data) {
            let newPos = this.data.getLocalPosition(this.personne.parent);

            newPos.x = newPos.x < this.height/2 ? this.height/2 : newPos.x;
            newPos.x = newPos.x > (600-this.height/2) ? (600-this.height/2) : newPos.x;
            newPos.y = newPos.y < this.width/2 ? this.width/2 : newPos.y;
            newPos.y = newPos.y > (600-this.width/2) ? (600-this.width/2) : newPos.y;
            this.personne.x = newPos.x;
            this.personne.y = newPos.y;

            if(this.onMove){
                this.onMove(this);
            }
        }
    }

    getPixiChildren() {
        return [this.container];
    }

    getX() {
        return this.x;
    }

    getY() {
        return this.y;
    }

    setX() {
        this.x = x;
        this.init();
    }

    setY() {
        this.y = y;
        this.init();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible() {
        this.container.visible = visible;
    }


}