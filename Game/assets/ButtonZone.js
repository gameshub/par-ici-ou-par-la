/**
 * @classdesc Asset bouttonZone
 * @author Xavier Montasell
 * @version 1.0
 */
class ButtonZone extends Asset {

    /**
     * Constructeur de l'asset bouton
     * @param {double} x Coordonnée X
     * @param {double} y Coordonnée Y
     * @param {string} label Le texte du bouton
     * @param {int} bgColor La couleur du bouton
     * @param {int} fgColor La couleur du texte
     */
    constructor(x, y, label, bgColor, fgColor, autofit = false, width = 150, height, radius) {
        super();
        /** @type {double} la coordonée x */
        this.x = x;
        /** @type {double} la coordonée y */
        this.y = y;
        /** @type {string} le texte du bouton */
        this.text = label;
        /** @type {int} la couleur de fond du bouton */
        this.bgColor = bgColor;
        /** @type {PIXI.Graphics} l'élément PIXI du fond du bouton */
        this.graphics = new PIXI.Graphics();
        /** @type {PIXI.Text} l'élément PIXI du texte du bouton */
        this.lbl = new PIXI.Text(this.text, {fontFamily: 'Arial', fontSize: 12, fill: fgColor, align: 'left'})

        this.height = height;
        this.radius = radius;
        this.autofit = autofit;
        this.width = width;
        //this.graphics.visible=false;
        /** @type {function} fonction de callback appelée lorsqu'un click est effectué sur le bouton */
        this.onClick = function () {
            console.log('Replace this action with button.setOnClick')
        };
        this.onRelease = function () {
            console.log('Replace this action with button.setOnRelease')
        };
        this.init();
    }

    /**
     * Initialise les éléments qui composent le bouton
     */
    init() {
        this.setText(this.text);
        this.graphics.interactive = false;
        this.graphics.buttonMode = false;

        this.graphics.on('pointerdown', function () {
            this.onClick()
        }.bind(this));
        this.graphics.on('mouseup', function () {
            this.onRelease()
        }.bind(this));
        this.graphics.on('touchend', function () {
            this.onRelease()
        }.bind(this));
        this.graphics.on('mouseupoutside', function () {
            this.onRelease()
        }.bind(this));
        this.graphics.on('touchendoutside', function () {
            this.onRelease()
        }.bind(this));
    }


    /**
     * Défini le texte à afficher sur le bouton
     * @param {string} text Le texte à afficher
     */
    setText(text) {
        this.lbl.text = text;
        this.update();
    }

    update() {
        let buttonWidth = 200;
        let buttonHeight = 12;
        //this.height = buttonHeight;
        this.graphics.clear();
        this.graphics.beginFill(this.bgColor);
        this.graphics.drawRoundedRect(this.x - this.width / 2, this.y - this.height / 2, this.width,  this.height, this.radius);
        this.graphics.endFill();
        this.lbl.anchor.set(0.5);
        this.lbl.x = this.x;
        this.lbl.y = this.y;
    }

    /**
     * Défini la fonction de callback à appeler après un click sur le bouton
     * @param {function} onClick La fonction de callback
     */
    setOnClick(onClick) {
        this.onClick = onClick;
    }
    setOnRelease(onRelease) {
        this.onRelease = onRelease;
    }

    /**
     * Retourne les éléments PIXI du bouton
     * @return {Object[]} les éléments PIXI qui composent le bouton
     */
    getPixiChildren() {
        return [this.graphics, this.lbl];
    }


    updateFont(font) {
        this.lbl.style.fontFamily = font;
    }

    getHeight() {
        return this.height;
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {
         for (let element of this.getPixiChildren()) {
             element.visible = visible
         }
        //this.container.visible = visible;
    }
    setInteractive(interactive) {
        for (let element of this.getPixiChildren()) {
            element.interactive = interactive
        }
        //this.container.visible = visible;
    }
    setButtonmode(buttonmode) {
        for (let element of this.getPixiChildren()) {
            element.visible = buttonmode
        }
        //this.container.visible = visible;
    }

    setY(y) {
        this.y = y;
        this.update();
    }

    setX(x) {
        this.x = x;
        this.update();
    }


    getWidth() {
        return this.autofit ? (this.width < this.lbl.width + 20 ? this.lbl.width + 20 : this.width) : this.width;
    }
}