/**
 * @classdesc Asset grille de dessin, retourne une séries de points lorsque l'utilisateur a dessiné le chemin
 * @author Xavier Montasell
 * @version 3.0
 */
class DrawingGrid extends Asset {

    /**
     * Constructeur de la grille de dessin
     * @param {int} col Le nombre de colonnes qui composent la grille
     * @param {int} lines Le nombre de lignes qui composent la grille
     * @param {double} width La largeur / hauteur entre chaque noeuds de la grille
     * @param {Pixi.Stage} stage Le stage Pixi
     * @param {function} onShapeCreated La fonction de callback appelée lorsqu'une forme a été dessinée
     */
    constructor(col, lines, width, stage, onShapeCreated) {
        super();
        /** @type {int} le nombre de colonnes */
        this.col = col;
        /** @type {int} le nombre de lignes */
        this.lines = lines;
        /** @type {double} la largeur / hauteur d'une cellule */
        this.width = width;
        /** @type {PIXI.Stage} le stage PIXI sur lequel dessiner les éléments de la grille */
        this.stage = stage;
        /** @type {Node[]} la liste des noeuds qui composent la grille */
        this.nodes = [];
        /** @type {ImgStatic[]} la liste des images qui composent la grille */
        this.logoCreate = [];
        /** @type {Point[]} la liste des points de la forme en cours de dessin */
        this.points = [];
        /** @type {PIXI.Graphics} la liste des lignes dessinées */
        this.strLines = [];
        /** @type {Point} l'origine de la forme en cours de dessin */
        this.origin = new Point(0, 0);
        /** @type {Node} le dernier noeud rencontré lors du dessin */
        this.lastnode = undefined;
        /** @type {PIXI.Graphics} le trait qui suit le curseur lors d'un dessin */
        this.line = undefined;
        /** @type {function} fonction de callback appelée lorsqu'une forme est dessinée */
        this.onShapeCreated = onShapeCreated;
        /** @type {boolean} si un dessin est en cours */
        this.drawing = false;
        /** @type {boolean} si l'option est en true le chemin change de couleur lorsque nous vérifions le chemin */
        this.visibleWay = true;
        /** @type {boolean} si l'option est en false nous ne pourrons pas changer le mode visibleWays */
        this.blockVisibleWays = false;
        /** @type {int} le nombre de point qui doit y passer */
        this.nombrePointAPasser = 0;
        /** Permet de savoir si c'est le mode "Créer" */
        this.modeCreate = false;
        this.blockNode = false;
        this.createWays = false;
        this.pointStartEnd = [];
        this.startSetup = false;
        this.endSetup = false;
        this.tradImage = [];
        this.init();
    }

    /**
     * Initialise la grille de dessin
     */
    init() {
        this.stage.interactive = true;
        this.startSetup = false;
        this.endSetup = false;
        this.stage.on('pointermove', this.onPointerMove.bind(this));
        this.stage.on('pointerup', this.onReleased.bind(this));

        for (let c = 0; c < this.col; c++) {
            for (let l = 0; l < this.lines; l++) {

                if (this.col === 5 && this.lines === 4) {
                    this.nodes.push(new Node(c * this.width + 95, l * this.width + 45, this.width / 50, this, c + ";" + l));
                } else {
                    /** Modification effectué par David Dumas
                     * this.nodes.push(new Node(c * this.width + 15, l * this.width + 30, this.width / 4.5, this, c + ";" + l)); (jeu par défaut)
                     * this.nodes.push(new Node(c * this.width + 38, l * this.width + 10, this.width / 50, this, c + ";" + l));
                     * */
                    this.nodes.push(new Node(c * this.width + 38, l * this.width + 12, this.width / 50, this, c + ";" + l));
                }
            }
        }
    }

    /**
     * Cette méthode permet d'afficher l'image donnée avec les paramètres suivants
     * @param logo le nom du logo dans la DB
     * @param nodeHold les coordonnées ou l'implenter
     * @param namePeople Le nom de l'image en fonction de ci c'est un bonhomme ou non
     * @param booleanStart le boolean de l'information de start
     * @param booleanEnd le boolean de l'information de end
     */
    affichageImage(logo, nodeHold, namePeople, booleanStart, booleanEnd) {
        let nbreLang = 4;

        let lang = this.refGameDG.global.resources.getLanguage();
        let numberLang = this.nbreLangue(lang);

        let trad = [];
        for (let i = 0; i < nbreLang; i++) {
            trad[i] = namePeople[i];
        }
        this.tradImage.push(trad);

        // On récupère l'image dans la DB
        let defaultWidth = 65;
        // on prend l'image en question
        let logoImg = this.refGameDG.global.resources.getImage(logo).image.src;
        let logoImage = new ImgStatic(nodeHold.x, nodeHold.y, defaultWidth, logoImg, namePeople[numberLang], true, "");
        logoImage.setNameLogo(logo);
        logoImage.setModeCreate(true);
        logoImage.setRefDG(this);
        logoImage.setRefCreate(this.refCreate);
        logoImage.setRefTrad(this.refGameDG);
        logoImage.setStart(booleanStart)
        logoImage.setEnd(booleanEnd);
        this.logoCreate.push(logoImage);
        this.refCreate.displayLogo(this.logoCreate);
        this.resetNoLogoCreate();
    }

    nbreLangue(lang) {
        let nbre;
        switch (lang) {
            case "fr":
                return nbre = 0;
            case "de":
                return nbre = 1;
            case "it":
                return nbre = 2;
            case "en":
                return nbre = 3;
        }
    }

    /**
     * Permet de récupérer LogoCreate un tableau d'ImgStatic
     * @returns {ImgStatic[]}
     */
    getLogoCreate() {
        return this.logoCreate;
    }

    /**
     * Réinitialise la grille de dessin
     */
    reset() {
        for (let line of this.strLines) {
            this.stage.removeChild(line);
        }
        this.strLines = [];
        this.points = [];
        this.logoCreate = [];
        this.origin = new Point(0, 0);
        this.drawing = false;
        this.blockVisibleWays = false;
        this.lastnode = null;
    }

    /**
     * Méthode qui permet de tout reset sauf le tableau logoCreate
     */
    resetNoLogoCreate() {
        for (let line of this.strLines) {
            this.stage.removeChild(line);
        }
        this.strLines = [];
        this.points = [];
        this.origin = new Point(0, 0);
        this.drawing = false;
        this.blockVisibleWays = false;
        this.lastnode = null;
    }

    /**
     * Change la valeur de la variable blockNode
     * @param boolean
     */
    setBlockNode(boolean) {
        this.blockNode = boolean;
    }

    /**
     * Méthode qui permet de bloquer le fait qu'on puisse cliquer sur une image
     * @param boolean
     */
    setBlockLogo(boolean) {
        for (let i = 0; i < this.logoCreate.length; i++) {
            if (this.logoCreate[i] !== null) {
                this.logoCreate[i].setTouchable(boolean);
            } else {
                this.logoCreate[i] = null;
            }
        }
    }

    /**
     * Méthode qui permet de modifier la valeur de "logoCreate"
     * @param arrayImg
     */
    setLogoCreate(arrayImg) {
        this.logoCreate = arrayImg;
    }

    /**
     * Créer le prochain point du polygone en cours de dessin
     * @param {Node} node le {@link Node} qui défini le nouveau point
     * @return {Point} le nouveau {@link Point}
     */
    createNextPoint(node) {
        let nodeCoords = node.getid();
        let coords = nodeCoords.split(";");
        let p = new Point(coords[0], coords[1]);
        p.name = node.getid();
        return p;
    }

    /**
     * Vérifie qu'un point ne soit pas deja existant dans la grille
     * @param {Point} p le point à controller
     * @return {boolean} si le point existe déjà
     */
    containsPoint(p) {
        for (let point of this.points) {
            if (point.x === p.x && point.y === p.y) return true;
        }
        return false;
    }

    /**
     * Créer le trait affiché sur la grille lorsque l'utilisateur déssine
     * @param {Node} node le noeud de départ
     */
    initLine(node) {
        this.line = new PIXI.Graphics();
        this.stage.addChild(this.line);
        this.line.lineStyle(6, 0xFF0000, 1);
        this.line.moveTo(node.center().x, node.center().y);
    }

    /**
     * Dessine une ligne le dernier noeud rencontré et le noeud donné en argument
     * @param {Node} node le noeud jusqu'au quel faire le trait
     */
    drawStrLine(node) {
        if (this.lastnode.center().x == node.center().x || this.lastnode.center().y == node.center().y) {
            let lastNodeCoords = this.lastnode.getid();
            let lnCoords = lastNodeCoords.split(";");
            let nodeCoords = node.getid();
            let coords = nodeCoords.split(";");
            let sommeX = coords[0] - lnCoords[0];
            let sommeY = coords[1] - lnCoords[1];
            if (sommeX == 1 || sommeX == -1 || sommeX == 0) {
                if (sommeY == 1 || sommeY == -1 || sommeY == 0) {
                    this.stage.removeChild(this.line);
                    this.initLine(node);
                    let straightLine = new PIXI.Graphics();
                    this.strLines.push(straightLine);
                    this.stage.addChild(straightLine);
                    straightLine.lineStyle(6, ORANGE, 1);
                    straightLine.moveTo(this.lastnode.center().x, this.lastnode.center().y);
                    straightLine.lineTo(node.center().x, node.center().y);
                    this.lastnode = node;
                    if (this.onLineDrawn) this.onLineDrawn();
                }
            }
        }
    }

    /**
     * Retourne la liste des éléments PIXI de la grille de dessin
     * @return {Object[]} les éléments PIXI qui composent la grille
     */
    getPixiChildren() {
        let children = [];
        for (let n of this.nodes) {
            if (n.graphics) children.push(n.graphics);
            if (n.point) children.push(n.point);
        }
        return children;
    }

    /**
     * Méthode de callback appelée lorsqu'un click est effectué sur un noeud de la grille.
     * Défini le noeud comme étant le noeud de départ et crée le trait de dessin
     * @param {Event} e l'événement JavaScript
     * @param {Node} node le noeud sur lequel on a clické
     */
    onNodeClicked(e, node) {
        if (!this.blockNode) {
            if (this.lastnode != null) {
                if (this.lastnode.getid() == node.getid()) {
                    this.initLine(this.lastnode);
                    this.drawing = true;
                    this.blockVisibleWays = true;
                    this.points.push();
                    this.origin = this.createNextPoint(node);
                }
            } else {
                this.lastnode = node;
                this.initLine(node);
                let nodeCoords = node.getid();
                let coords = "";
                coords = nodeCoords.split(";");
                if (this.modeCreate) {
                    this.drawing = false;
                    this.actionPopPup(coords);
                } else {
                    this.drawing = true;
                    this.blockVisibleWays = true;
                    this.points.push();
                    this.origin = this.createNextPoint(node);
                    let p = new Point(coords[0], coords[1]);
                    p.name = this.origin.name;
                    this.points.push(p);
                    this.drawStrLine(node)
                }

            }
        }
    }

    /**
     * Méthode de callback appelée lorsque le click est relâché
     * @param {Event} e L'événement JavaScript
     */
    onReleased(e) {
        if (this.points.length === 1) {
            this.stage.removeChild(this.line);
            this.initLine(this.lastnode);
            let straightLine = new PIXI.Graphics();
            this.strLines.push(straightLine);
            this.stage.addChild(straightLine);
            straightLine.lineStyle(6, ORANGE, 1);
            const squareLine = {
                width: 10,
                height: 10
            };
            straightLine.beginFill(ORANGE, 1);
            straightLine.drawRect(this.lastnode.center().x - squareLine.width / 2, this.lastnode.center().y - squareLine.height / 2, squareLine.width, squareLine.height);
            straightLine.endFill();
        }
        this.drawing = false;
        this.stage.removeChild(this.line);
    }

    /**
     * Méthode de callback appelée lorsque le curseur bouge.
     * Déssine le trait à la position du curseur
     * @param {Event} e L'événement JavaScript
     */
    onPointerMove(e) {
        if (this.drawing) {
            let position = e.data.getLocalPosition(this.stage);
            this.line.lineTo(position.x, position.y);
            /** Correction David Dumas "Hors du jeu, on réinitialise le lien" */
            if (position.x < 0 || position.x > 600 || position.y < 0 || position.y > 600) {
                this.drawing = false;
                this.stage.removeChild(this.line);
            }
            for (let n of this.nodes) {
                if (n.graphics.containsPoint(e.data.getLocalPosition(n.graphics.parent))) this.onNodeEncountered(e, n);
            }
        }
    }

    /**
     * Méthode de callback appelée lorsque que le curseur touche un noeud de la grille.
     * Vérifie si la forme est terminée ou si il faut dessiner un trait droit entre ce noeud et le noeud de départ.
     * @param {Event} e L'événement JavaScript
     * @param {Node} node Le noeud de la grille touché
     */
    onNodeEncountered(e, node) {
        if (this.drawing && this.lastnode !== node) { // Si le noeud rencontré n'est pas le dernier noeud
            let point = this.createNextPoint(node);
            this.drawStrLine(node);
            let len = this.points.length;
            if (this.lastnode.center().x == node.center().x || this.lastnode.center().y == node.center().y) {
                this.points.push(point);
            }
        }
    }

    /**
     * Défini la fonction de callback appelée lorsqu'une ligne est dessinée
     * @param {function} onLineDrawn La fonction de callback
     */
    setOnLineDrawn(onLineDrawn) {
        this.onLineDrawn = onLineDrawn;
    }

    /**
     * Ce qu'il va etre réalise l'hors que le bouton reesayer est appuyé
     * @param {Array} boutons liste des boutons
     */
    onReset(boutons) {
        this.reset();
    }

    /**
     * Cette methode permet changer la couleur du chemin dessiné
     * @param pointDebut String contenant les coordonees du point initial
     * @param pointFin String contenant les coordonees du point final
     * @param color nouveau couleur de la ligne
     */
    changecolor(pointDebut, pointFin, color) {
        if (this.visibleWay) {
            let count = 0;
            let debutX = 0;
            let debutY = 0;
            let finX = 0;
            let finY = 0;
            for (let key in this.nodes) {
                let coords = this.nodes[count].getid();
                let coordss = coords.split(";");
                if (coordss[0] == pointDebut.x) {
                    if (coordss[1] == pointDebut.y) {
                        debutX = this.nodes[count].center().x;
                        debutY = this.nodes[count].center().y;
                    }
                }
                if (coordss[0] == pointFin.x) {
                    if (coordss[1] == pointFin.y) {
                        finX = this.nodes[count].center().x;
                        finY = this.nodes[count].center().y;
                    }
                }
                count++;
            }
            let straightLine = new PIXI.Graphics();
            straightLine.lineStyle(6, color, 1);
            straightLine.moveTo(debutX, debutY);
            straightLine.lineTo(finX, finY);
            this.strLines.push(straightLine);
            this.stage.addChild(straightLine);
        }
    }

    /**
     * Réalise la verification du chemin déssiné
     * @param {json} chemin Le chemin déssiné par l'utilisateur
     * @param {Array} listeBoutons Le chemin déssiné par l'utilisateur
     * @param {boolean} evaluation est un boolean pour savoir si c'est en mode evaluation
     */
    setOnShapeCreated(chemin, listeBoutons, evaluation) {
        // on regarde si c'est la création du chemin dans "créer" ou si c'est le jeu
        if (!this.createWays) {
            /** On réinitialise à 0 en cas ou nous avions déjà utilisé */
            this.nombrePointAPasser = 0;
            this.boutonsAafficher = new Array();
            this.onShapeCreated = chemin;
            this.points.name;
            // si plusieurs points add -> || chemin.debut[2] == this.points[0].name || chemin.debut[3] == this.points[0].name || chemin.debut[4] == this.points[0].name
            if(this.points.length !== 0) {
                if (chemin.debut[1] === this.points[0].name) {
                    if (!evaluation) {
                        this.changecolor(this.points[0], this.points[1], VERT);
                    }
                    let count = 1;
                    let ok = 1;
                    let positionSegment = [];
                    let i = 0;
                    this.dernierPointCorrecte = this.points[1];
                    this.pourcentage = -1;
                    for (let key in chemin.segments) {
                        let segment = chemin.segments[count].split("/");
                        this.nombrePointAPasser++;
                        for (let key in this.points) {
                            if (this.points[i].name == segment[0] || this.points[i].name == segment[1]) {
                                if ((this.points[i + 2].name == segment[1] || this.points[i + 2].name == segment[0]) || (segment[0] === segment[1])) {
                                    if (positionSegment > i) {
                                        ok = -1;
                                        this.dernierPointCorrecte = this.points[i];
                                        break;
                                    } else {
                                        positionSegment.push(i);
                                        this.dernierPointCorrecte = this.points[i + 2];
                                        ok++;
                                    }
                                    break;
                                }
                            }
                            i++;
                        }
                        i = 0;
                        count++;
                        // lorsque c'est en mode entraine pour afficher les corrections
                        if (!evaluation) {
                            if (ok == count) {
                                for (let i = 0; i < this.points.length - 1; i++) {
                                    this.changecolor(this.points[i], this.points[i + 1], VERT);
                                }
                                Swal.fire({
                                    title: this.refGameDG.global.resources.getOtherText('good'),
                                    width: 500,
                                    icon: 'success',
                                    confirmButtonColor: BLEU,
                                    confirmButtonText: 'OK',
                                    showConfirmButton: true,
                                    html: this.refGameDG.global.resources.getOtherText('goodWays')
                                });
                                this.pourcentage = 1;
                            } else if (ok == -1) {
                                this.segmentsok(false);
                                Swal.fire({
                                    title: this.refGameDG.global.resources.getOtherText('errorPopup'),
                                    width: 500,
                                    icon: 'error',
                                    confirmButtonColor: BLEU,
                                    confirmButtonText: 'OK',
                                    showConfirmButton: true,
                                    html: this.refGameDG.global.resources.getOtherText('notAllGoodWays')
                                });
                                this.pourcentage = 0;
                            } else {
                                /*
                                listeBoutons[count - 1].setVisible(true);
                                listeBoutons[count - 1].interactive = false;
                                listeBoutons[count - 1].buttonMode = false;
                                listeBoutons[count - 1].update();
                                 */
                                this.segmentsok(false);
                                Swal.fire({
                                    title: this.refGameDG.global.resources.getOtherText("errorPopup"),
                                    width: 500,
                                    icon: 'error',
                                    confirmButtonColor: BLEU,
                                    confirmButtonText: 'OK',
                                    showConfirmButton: true,
                                    html: this.refGameDG.global.resources.getOtherText("notGoodFinish")
                                });
                                this.pourcentage = 0;
                            }
                            if (ok == count) {
                                // si plusieurs point de départ (3) -> || chemin.fin[2] == this.points[this.points.length - 1].name || chemin.fin[3] == this.points[this.points.length - 1].name || chemin.fin[4] == this.points[this.points.length - 1].name
                                if (chemin.fin[1] == this.points[this.points.length - 1].name) {
                                } else {
                                    /*
                                    listeBoutons[listeBoutons.length - 1].setVisible(true);
                                    listeBoutons[listeBoutons.length - 1].interactive = false;
                                    listeBoutons[listeBoutons.length - 1].buttonMode = false;
                                    listeBoutons[listeBoutons.length - 1].update();
                                     */
                                    this.segmentsok(false);
                                    Swal.fire({
                                        title: this.refGameDG.global.resources.getOtherText("errorPopup"),
                                        width: 500,
                                        icon: 'error',
                                        confirmButtonColor: BLEU,
                                        confirmButtonText: 'OK',
                                        showConfirmButton: true,
                                        html: this.refGameDG.global.resources.getOtherText('goodWays')
                                    });
                                }
                            } else {
                                let passageText;
                                let passage = "";
                                for (let j = 1; j < ok; j++) {
                                    this.pourcentage += (60 / this.nombrePointAPasser);
                                }
                                for (let j = 0; j < ok; j++) {
                                    if (ok !== j + 1) {
                                        passageText = "Ok";
                                    } else {
                                        passageText = "Ko";
                                    }
                                    if ((j + 1) === this.nombrePointAPasser) {
                                        passage += passageText;
                                    } else {
                                        passage += passageText + ";";
                                    }
                                }
                                this.pourcentage = this.pourcentage + "," + passage;
                            }
                        } else {
                            this.pourcentage = 0;
                            if (chemin.fin[1] == this.points[this.points.length - 1].name) this.pourcentage += 20;
                            if (chemin.debut[1] == this.points[0].name) this.pourcentage += 20;
                            let passageText = "";
                            let passage = "";
                            // ok = 3 donc this.nombrePointAPasser + 1
                            if (ok === count) {
                                this.pourcentage = 100;
                                for (let j = 0; j < this.nombrePointAPasser; j++) {
                                    passageText = "Ok";
                                    if ((j + 1) === this.nombrePointAPasser) {
                                        passage += passageText;
                                    } else {
                                        passage += passageText + ";";
                                    }
                                }
                                this.pourcentage = this.pourcentage + "," + passage;
                            } else {
                                for (let j = 1; j < ok; j++) {
                                    this.pourcentage += (60 / this.nombrePointAPasser);
                                }
                                for (let j = 0; j < this.nombrePointAPasser; j++) {
                                    if (ok !== j + 1) {
                                        passageText = "Ok";
                                    } else {
                                        passageText = "Ko";
                                    }
                                    if ((j + 1) === this.nombrePointAPasser) {
                                        passage += passageText;
                                    } else {
                                        passage += passageText + ";";
                                    }
                                }
                                this.pourcentage = this.pourcentage + "," + passage;
                            }
                        }

                    }
                } else {
                    if (!evaluation) {
                        /*
                        for (let i = 0; i < listeBoutons.length; i++) {
                            listeBoutons[i].setVisible(true);
                            listeBoutons[i].interactive = false;
                            listeBoutons[i].buttonMode = false;
                            listeBoutons[i].update();
                        }
                         */

                        for (let j = 0; j < this.points.length - 1; j++) {
                            this.changecolor(this.points[j], this.points[j + 1], ROUGE);
                        }
                        Swal.fire({
                            title: this.refGameDG.global.resources.getOtherText("errorPopup"),
                            width: 500,
                            icon: 'error',
                            confirmButtonColor: BLEU,
                            confirmButtonText: 'OK',
                            showConfirmButton: true,
                            html: this.refGameDG.global.resources.getOtherText("notGoodStart")
                        });
                    } else {
                        this.pourcentage = 0 + ",Ko fs";
                    }
                }
            }
        }
        return this.pourcentage;
    }

    /**
     * Cette methode permet change la couleur des segments. Elle est separeé de la methode change colo
     * @param ok {boolean} qui nous indique si c'est juste ou faux
     */
    segmentsok(ok) {
        if (ok) {
            let changerCouleur = false;
            let dernierPoint = 0;
            for (let i = 0; i < this.points.length - 1; i++) {
                if (this.points[i].name == this.dernierPointCorrecte.name) {
                    changerCouleur = true;
                }
                if (changerCouleur) {
                    this.changecolor(this.points[i], this.points[i + 1], ROUGE);
                    for (let j = 0; j < dernierPoint; j++) {
                        this.changecolor(this.points[j], this.points[j + 1], VERT);
                    }
                } else {
                    dernierPoint++;
                }
            }
            dernierPoint = 0;
        } else {
            let changerCouleur = false;
            for (let i = 0; i < this.points.length - 1; i++) {
                if (this.points[i].name == this.dernierPointCorrecte.name) {
                    changerCouleur = true;
                }
                if (changerCouleur) {
                    this.changecolor(this.points[i], this.points[i + 1], ROUGE);
                }
            }
        }
    }

    /**
     * On modifie la variable modeCreate en fonction de la valeur du boolean
     * @param boolean 'true' on active le mode 'false' on le désactive
     */
    setModeCreate(boolean) {
        this.modeCreate = boolean;
    }

    /**
     * On modifie la variable refGameDG en fonction de la valeur du donnée
     * @param ref est la ref global de l'application pour pouvoir mettre les traductions dans cette partie
     */
    setRefGameDG(ref) {
        this.refGameDG = ref;
    }

    /**
     * On modifie la variable refCreate en fonction de la valeur donnée
     * @param refCreate
     */
    setRefCreate(refCreate) {
        this.refCreate = refCreate;
    }

    /**
     * Méthode qui permet la prise d'information des images/logos ainsi que des coordonnées en question
     * @param coords les coordonnées du point sélectionner
     */
    async actionPopPup(coords) {
        let nodeHold;
        let namePeople = [];

        // On crée des variables pour les personnes
        // MR : Pas besoin de fixer ceci dans le code
        /*         var boy = "boy";
                var businessWoman = "businessWoman";
                var femaleUser = "femaleUser";
                var girl = "girl";
                var standingMan = "standingMan"; */

        for (let i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i]._id === coords[0] + ";" + coords[1]) {
                nodeHold = this.nodes[i];
            }
        }
        // ERROR COME HERE
        this.tableImage = this.getImages();

        // Implémentation toutes les images/logos
        /** Popup avec toutes les images dont nous avons besoin */
        const {value: logoselected} = await Swal.fire({
            title: this.refGameDG.global.resources.getOtherText('selectImage'),
            html: this.addLogo(),
            width: 600,
            focusConfirm: false,
            preConfirm: () => {
                if (document.querySelector("input[name='logo']:checked") !== null) {
                    return [
                        document.querySelector("input[name='logo']:checked").id
                    ]
                } else {
                    return [null]
                }
            }
        });

        // if (logoselected[0] == null) {
        //     Swal.fire({
        //         icon: 'error',
        //         text: this.refGameDG.global.resources.getOtherText('logoSelectOptions')
        //     });
        // }

        if (typeof logoselected === 'undefined') {
            this.resetNoLogoCreate();
        } else if (logoselected[0] !== null) {
            // On vérifie ce que l'utilisateur a sélectionné et on regarde si c'est un personnage ou non
            //Aif(logoselected[0] === boy || logoselected[0] === businessWoman || logoselected[0] === femaleUser || logoselected[0] === girl || logoselected[0] === standingMan){
            let name = [];
            /** On regarde si il y a déjà le start et/ou end actif */
            if (!this.startSetup && !this.endSetup) {
                const {value: nameB} = await Swal.fire({
                    title: this.refGameDG.global.resources.getOtherText("name"),
                    html:
                        '<input id="name" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_FR") + '">' +
                        '<input id="nameDE" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_DE") + '">' +
                        '<input id="nameIT" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_IT") + '">' +
                        '<input id="nameEN" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_EN") + '">' +
                        '<input type="checkbox" id="start">\n' +
                        '<label for="start">' + this.refGameDG.global.resources.getOtherText("useStart") + '</label><br>' +
                        '<input type="checkbox" id="end">\n' +
                        '<label for="end">' + this.refGameDG.global.resources.getOtherText("useEnd") + '</label><br>'
                    ,
                    focusConfirm: false,
                    showCancelButton: true,
                    preConfirm: () => {
                        return [
                            document.getElementById("name").value,
                            document.getElementById("nameDE").value,
                            document.getElementById("nameIT").value,
                            document.getElementById("nameEN").value,
                            document.getElementById("start").checked,
                            document.getElementById("end").checked
                        ]
                    }
                });

                /** On vérifie que l'élève n'a pas cliqué sur les start et end */
                if (name[1] && name[2]) {
                    Swal.fire({
                        icon: 'error',
                        text: this.refGameDG.global.resources.getOtherText('optionsStartEndDefined')
                    });

                } else {
                    name = nameB;
                    this.resetNoLogoCreate();
                }

                /** Si le start est déjà définit on autorise que le end */
            } else if (this.startSetup && !this.endSetup) {
                const {value: nameB} = await Swal.fire({
                    title: this.refGameDG.global.resources.getOtherText("name"),
                    html:
                        '<input id="name" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_FR") + '">' +
                        '<input id="nameDE" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_DE") + '">' +
                        '<input id="nameIT" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_IT") + '">' +
                        '<input id="nameEN" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_EN") + '">' +
                        '<input type="checkbox" id="end">\n' +
                        '<label for="end">' + this.refGameDG.global.resources.getOtherText("useEnd") + '</label><br>'
                    ,
                    focusConfirm: false,
                    showCancelButton: true,
                    preConfirm: () => {
                        return [
                            document.getElementById("name").value,
                            document.getElementById("nameDE").value,
                            document.getElementById("nameIT").value,
                            document.getElementById("nameEN").value,
                            false,
                            document.getElementById("end").checked
                        ]
                    }
                });
                nameB[1] = false;
                name = nameB;

                /** Si le end est déjà définit on autorise que le start */
            } else if (this.endSetup && !this.startSetup) {
                const {value: nameB} = await Swal.fire({
                    title: this.refGameDG.global.resources.getOtherText("namePNJ"),
                    html:
                        '<input id="name" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_FR") + '">' +
                        '<input id="nameDE" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_DE") + '">' +
                        '<input id="nameIT" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_IT") + '">' +
                        '<input id="nameEN" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_EN") + '">' +
                        '<input type="checkbox" id="start">\n' +
                        '<label for="end">' + this.refGameDG.global.resources.getOtherText("useStart") + '</label><br>'
                    ,
                    focusConfirm: false,
                    showCancelButton: true,
                    preConfirm: () => {
                        return [
                            document.getElementById("name").value,
                            document.getElementById("nameDE").value,
                            document.getElementById("nameIT").value,
                            document.getElementById("nameEN").value,
                            document.getElementById("start").checked,
                            false
                        ]
                    }
                });
                name = nameB;

                /** On autorise l'élève à mettre le nom de l'objet en question */
            } else {
                const {value: nameB} = await Swal.fire({
                    title: this.refGameDG.global.resources.getOtherText("name"),
                    html:
                        '<input id="name" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_FR") + '">' +
                        '<input id="nameDE" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_DE") + '">' +
                        '<input id="nameIT" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_IT") + '">' +
                        '<input id="nameEN" class="swal2-input" placeholder="' + this.refGameDG.global.resources.getOtherText("translate_name_object_EN") + '">'
                    ,
                    focusConfirm: false,
                    showCancelButton: true,
                    preConfirm: () => {
                        return [
                            document.getElementById("name").value,
                            document.getElementById("nameDE").value,
                            document.getElementById("nameIT").value,
                            document.getElementById("nameEN").value,
                            false,
                            false
                        ]
                    }
                });
                name = nameB;
            }
            /** on vérifie qu'il a bien rempli l'endroit */
            if (typeof name !== 'undefined') {
                if (name[0] === "" && name[4] || name[0] === "" && name[5]) {
                    Swal.fire({
                        icon: 'error',
                        text: this.refGameDG.global.resources.getOtherText('nameEmpty')
                    });
                    // } else if (name[4] && name[5]) {
                    //     Swal.fire({
                    //         icon: 'error',
                    //         text: this.refGameDG.global.resources.getOtherText('impossibleStartEnd')
                    //     });
                } else {
                    if (name[4] && name[5]) {
                        if (!this.startSetup && !this.endSetup) {
                            this.startSetup = true;
                            this.endSetup = true;
                            this.pointStartEnd.push((coords[0] * 2 + 1) + ";" + (coords[1] * 2 + 1));
                            namePeople[0] = name[0];
                            namePeople[1] = name[0];
                            namePeople[2] = name[0];
                            namePeople[3] = name[0];
                            // this.affichageImage(logoselected[0], nodeHold, namePeople, name[1], name[2]);
                            this.affichageImage(logoselected[0], nodeHold, namePeople, name[4], name[5]);
                        } else {
                            Swal.fire({
                                icon: 'error',
                                text: this.refGameDG.global.resources.getOtherText('optionsStartEnable')
                            });
                        }
                    } else if (name[4] && !name[5]) {
                        if (!this.startSetup) {
                            this.startSetup = true;
                            this.pointStartEnd.push((coords[0] * 2 + 1) + ";" + (coords[1] * 2 + 1));
                            namePeople[0] = name[0];
                            namePeople[1] = name[0];
                            namePeople[2] = name[0];
                            namePeople[3] = name[0];
                            // this.affichageImage(logoselected[0], nodeHold, namePeople, name[1], name[2]);
                            this.affichageImage(logoselected[0], nodeHold, namePeople, name[4], name[5]);
                        } else {
                            Swal.fire({
                                icon: 'error',
                                text: this.refGameDG.global.resources.getOtherText('optionsStartEnable')
                            });
                        }
                    } else if (name[5] && !name[4]) {
                        if (!this.endSetup) {
                            this.endSetup = true;
                            this.pointStartEnd.push((coords[0] * 2 + 1) + ";" + (coords[1] * 2 + 1));
                            namePeople[0] = name[0];
                            namePeople[1] = name[0];
                            namePeople[2] = name[0];
                            namePeople[3] = name[0];
                            // this.affichageImage(logoselected[0], nodeHold, namePeople, name[1], name[2]);
                            this.affichageImage(logoselected[0], nodeHold, namePeople, name[4], name[5]);
                        } else {
                            Swal.fire({
                                icon: 'error',
                                text: this.refGameDG.global.resources.getOtherText('optionsStartEnable')
                            });
                        }
                    } else if (!name[5] && !name[4]) {
                        namePeople[0] = name[0];
                        namePeople[1] = name[0];
                        namePeople[2] = name[0];
                        namePeople[3] = name[0];
                        // this.affichageImage(logoselected[0], nodeHold, namePeople, name[1], name[2]);
                        this.affichageImage(logoselected[0], nodeHold, namePeople, name[4], name[5]);
                    }
                }
                this.resetNoLogoCreate();
            } else {
                this.resetNoLogoCreate();
            }
        }

        // implémentation de la partie images/logos
        document.getElementById("swal2-content").innerHTML += '<div style="padding: 30px">' +
            '<table>' +
            '<tbody>' +
            '<tr>' +
            '<div class="center">' +
            '<div class="buttonsLogo"></div>' +
            '</br>' +
            '</br>' +
            '</div>' +
            '</tr>' +
            '</tbody>' +
            '</table>' +
            '</div>';
    }

    /**
     * Permet d'ajouter les images depuis la popup
     */
    addLogo() {
        // document.getElementById("swal2-content").style.display = "block";
        var nbreImageLigne = 5;
        var div = '<div style="overflow:scroll; overflow-x: hidden;  display: block; width: 100%; height:500px; border:#000000 1px solid;">';
        var nameImage = "";
        let nbreImage = 0;
        let imgCompar;
        var arrayImage = [];
        // image où nous devons rien faire (image inutile pour ici)
        let son = this.tableImage.son.image;
        let defaultSprite = this.tableImage.default_sprite_error_framework.image;
        let speaker = this.tableImage.speaker.image;
        let background = this.tableImage.background.image;
        let backgroundValidation = this.tableImage.backgroundValidation.image;
        let carreStart = this.tableImage.carreStart.image;
        let carreEnd = this.tableImage.carreEnd.image;
        let segmentHorizon = this.tableImage.segmentHorizon.image;
        let segmentVertical = this.tableImage.segmentVertical.image;
        let backgroundCreate = this.tableImage.backgroundCreate.image;
        const organisedItems = []
        for (let i = 0; i < Object.keys(this.tableImage).length; i++) {
            imgCompar = Object.values(this.tableImage)[i].image;
            if ((imgCompar !== son) && (imgCompar !== defaultSprite) && (imgCompar !== speaker) && (imgCompar !== background) && (imgCompar !== backgroundValidation) && (imgCompar !== carreStart) && (imgCompar !== carreEnd) && (imgCompar !== segmentHorizon) && (imgCompar !== segmentVertical) && (imgCompar !== backgroundCreate)){
                if (this.refGameDG.global.resources.getOtherText(Object.keys(this.tableImage)[i]) !== '404 - Text Not Found') {
                    organisedItems.push(
                        {
                            'trad': this.refGameDG.global.resources.getOtherText(Object.keys(this.tableImage)[i]),
                            'key': Object.keys(this.tableImage)[i],
                            'value': Object.values(this.tableImage)[i]
                        })
                }
            }
        }
        organisedItems.sort((a, b) => a.trad.localeCompare(b.trad))

        for (let i = 0; i < organisedItems.length; i++) {
            // On récupère le nom de l'image
                nameImage = Object.keys(this.tableImage)[i];
                var trad = this.refGameDG.global.resources.getOtherText(nameImage);
                arrayImage.push(nameImage);
                // On divise toutes les images par 8 pour avoir 8 logo par pages

                if ((nbreImage % nbreImageLigne) === 0) {
                    div += '<div style="display: flex;"><div style="display: inline;width:25%;height:80px"><img src="' + organisedItems[i].value.image.currentSrc + ' " /></br><span>' + organisedItems[i].trad + '</span></br><input type="radio" name="logo" id="' + organisedItems[i].key + '"></div>';
                } else if ((nbreImage % nbreImageLigne) === (nbreImageLigne - 1)) {
                    div += '<div><img src="' + organisedItems[i].value.image.currentSrc + '"/></br><span>' + organisedItems[i].trad + '</span></br><input type="radio" name="logo" id="' + organisedItems[i].key + '"></div></div></br>';
                } else {
                    div += '<div><img src="' + organisedItems[i].value.image.currentSrc + '"/></br><span>' + organisedItems[i].trad + '</span></br><input type="radio" name="logo" id="' + organisedItems[i].key + '"></div>';
                }
                nbreImage++;
        }
        // on ferme les div's
        div += '</div></div>';
        // on modifie dans la popup pour l'affichage
        // document.getElementsByClassName("buttonsLogo")[0].innerHTML = div;
        return div;
    }

    /**
     * Cette méthode retourne toutes les images de la db
     */
    getImages() {
        this.tableImage = this.refGameDG.global.resources.IMAGES;
        return this.tableImage;
    }

    /**
     * Cette méthode retourne les tradImages
     */
    getTradImage() {
        return this.tradImage;
    }

    /**
     * Cette méthode permet de modifier si les points sont visible (dans Node.js)
     * @param actif est l'état, si c'est visible ou non
     */
    setVisiblePoint(actif) {
        for (let i = 0; i < this.nodes.length; i++) {
            this.nodes[i].setVisiblePoint(actif);
        }
    }

    /**
     * Change la valeur pour ne plus afficher les corrections
     */
    setVisibleCorrectWays(actif) {
        this.visibleWay = actif
    }

    /**
     * Permet de récupérer le nombre de point à passer
     * @returns {Number}
     */
    getNombrePointAPasser() {
        return this.nombrePointAPasser;
    }

    /**
     * Permet de modifier la valeur du nombrePointAPasser en fonction du nbre donnée
     * @param nombrePointAPasser
     */
    setNombrePointAPasser(nombrePointAPasser) {
        this.nombrePointAPasser = nombrePointAPasser;
    }

    /**
     * Cette méthode modifie la variable de "createWays"
     * @param boolean
     */
    setCreateWays(boolean) {
        this.createWays = boolean;
    }

    /**
     * Méthode qui retourne un tableau de "Point"
     * @returns {Point[]}
     */
    getPoint() {
        return this.points;
    }

    /**
     * Retourne le tableau pointStartEnd
     * @returns {[]|*[]}
     */
    getPointStartEnd() {
        return this.pointStartEnd;
    }

    /**
     * Méthode qui permet de modifier la valeur de startSetup
     * @param boolean valeur pour modifier
     */
    setStartSetup(boolean) {
        this.startSetup = boolean;
    }

    /**
     * Méthode qui permet de modifier la valeur de endSetup
     * @param boolean valeur pour modifier
     */
    setEndSetup(boolean) {
        this.endSetup = boolean
    }
}
