class ImgStatic extends Asset {

    constructor(x, y, width, imgBackground, text, touchable, textSound) {
        super();
        this.y = y;
        this.x = x;
        this.width = width;
        this.height = width;
        this.imgBackground = imgBackground;
        this.text = text;
        this.start = false;
        this.end = false;
        this.isFirstClick = true;
        this.onClick = function() {
           if(this.modeCreate){
               Swal.fire({
                   title: this.refTrad.global.resources.getOtherText("deleteLogo"),
                   icon: 'warning',
                   showCancelButton: true,
                   confirmButtonColor: '#3085d6',
                   cancelButtonColor: '#d33',
                   confirmButtonText: this.refTrad.global.resources.getOtherText("yes"),
                   cancelButtonText: this.refTrad.global.resources.getOtherText("no")
               }).then((result) => {
                   // if (result.dismiss !== "cancel") {
                   if (result.value) { // on supprime l'objet
                       this.refCreate.takeOffLogo(this);
                       if (this.getStart() === true){
                           this.refDG.startSetup = false;
                       }
                       if (this.getEnd() === true){
                           this.refDG.endSetup = false;
                       }
                   }
               });
           } else {
               this.popUp(textSound);
           }
        };
        this.container = new PIXI.Container();
        this.imgStatic = undefined;
        this.touchable = touchable;
        this.init();
    }
    init() {
        this.container.removeChildren();
        // Sprite permet de rendre l'objet manipulable
        this.imgStatic = PIXI.Sprite.fromImage(this.imgBackground);
        this.imgStatic.anchor.x = 0.5;
        this.imgStatic.anchor.y = 0.3;
        this.imgStatic.x = this.x;
        this.imgStatic.y = this.y;
        this.imgStatic.width = this.width;
        this.imgStatic.height = this.height;
        let textAecrire = new PIXI.Text(this.text,{fontFamily: "\"Arial\", cursive, sans-serif", fontVariant: "small-caps", fontWeight: 600, fontSize: 14, fill : 0x000000, align : 'center'});
        textAecrire.x = this.x;
        textAecrire.y = this.y;
        textAecrire.anchor.x = 0.5;
        if(this.width === 60){
            textAecrire.anchor.y = -1;
        }else if (this.width === 40) {
            textAecrire.anchor.y = -1;
        }else{
            textAecrire.anchor.y = -3;
        }
        if(this.touchable){
            this.imgStatic.interactive = true;
            this.imgStatic.on('pointerdown', function () {this.onClick()}.bind(this));
        } else {
            this.imgStatic.interactive = false;
        }
        this.container.addChild(this.imgStatic, textAecrire);
    }

    setTouchable(boolean){
        this.imgStatic.interactive = !boolean;
    }

    setNameLogo(nameLogo){
        this.name = nameLogo;
    }

    setModeCreate(boolean){
        this.modeCreate = boolean;
    }

    setRefDG(ref){
        this.refDG = ref;
    }

    setRefTrad(ref){
        this.refTrad = ref;
    }

    setRefCreate(ref){
        this.refCreate = ref;
    }

    setOnClick(onClick) {
        this.onClick = onClick;
    }

    update(y){
        this.y = y;
    }

    getPixiChildren() {
        return [this.container];
    }

    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setY(y) {
        this.imgStatic.y = y;
        this.update();
    }

    setX(x) {
        this.imgStatic.x = x;
        this.update();
    }

    getWidth() {
        return this.width;
    }

    getHeight() {
        return this.height;
    }

    setVisible(visible) {
        this.container.visible = visible;
    }

    /**
     * Méthode vide pour un clique sans action
     */
    nothing(){}

    /**
     * Popup qui apparait quand on clique sur le hautparleur du dans le canvas
     * @param texte
     */
    popUp(texte) {
        Swal.fire({
            title: name.toUpperCase(),
            width: 1000,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td><div id="activites" style="text-align: center; padding-left : 40px">' + texte + '</div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    setStart(boolean){
        this.start = boolean;
    }
    getStart(){
        return this.start;
    }

    setEnd(boolean){
        this.end = boolean;
    }

    getEnd(){
        return this.end;
    }
}