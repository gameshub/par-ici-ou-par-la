/**
 * @classdesc Bean node. Noeud qui compose une grille de dessin
 * @author Vincent Audergon
 * @version 1.0
 */
class Node extends Asset{

    getY() {
       // super.getY();
        return this.y;
    }

    getX() {
       // super.getX();
        return this.x;
    }

    getWidth() {
        // super.getWidth();
        return this.width;
    }

    /**
     * Constructeur de Node
     * @param {double} x La position x du noeud
     * @param {double} y La position y du noeud
     * @param {double} width La largeur et hauteur du noeud
     * @param {DrawingGrid} refGrid La référence vers la {@link DrawingGrid} (grille de dessin)
     */
    constructor(x, y, width, refGrid,id ) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.refGrid = refGrid;
        this.init();
        this._id = id;
    }

    /**
     * Initialise le noeud
     */
    init() {
      this.graphics = new PIXI.Graphics();
      this.graphics.interactive = true;
      this.graphics.buttonMode = true;
      // 0xFFFFFF
      this.graphics.beginFill(0xFFFFFF);
      this.graphics.drawRect(this.x -5 ,this.y -5, this.width+40,this.width+40);
      this.graphics.endFill(0xFFFFFF);
      // Enlever renderable et mettre visible en cas de besoin
      this.graphics.renderable =false;
      //this.graphics.visible = true;
      this.graphics.on('pointerdown', this.onNodeClicked.bind(this));

        this.point = new PIXI.Graphics();
        this.point.beginFill(0x000000);
        this.point.drawRect(this.x+10, this.y+10, this.width-10, this.width-10);
        this.point.endFill(0x000000);
        this.point.visible = true;

        this.point.interactive = true;
        this.point.buttonMode = true;
        this.point.on('pointerdown', this.onNodeClicked.bind(this));
    }

    /**
     * Modifier la valeur visible
     */
    setVisiblePoint (actif){
        if(!actif){
            this.point.visible = true;
        } else {
            this.point.visible = false;
        }
    }

    /**
     * Retourne les coordonées du centre du noeud
     * @return {Point} le centre du noeud
     */
    center() {
        return new Point(this.x+5 + this.width / 2, this.y+5 + this.width / 2);
    }

    /**
     * Méthode de callback appelée lorsqu'un click est détecté sur le noeud
     * @param {Event} e L'événement JavaScript
     */
    onNodeClicked(e) {
        this.refGrid.onNodeClicked(e, this);
    }

    /**
     * Méthode de callback appelée lorsque le click est enfoncé
     * et que le curseur passe au dessus du noeud
     * @deprecated
     * @param {Event} e L'événement JavaScript
     */
    onNodeEncountered(e) {
        this.refGrid.onNodeEncountered(e, this)
    }

    getid() {
        return this._id;
    }
}
