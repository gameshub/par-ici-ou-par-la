class ToggleGroup extends Asset {

    /**
     *
     * @param mode
     * @param buttons
     */
    constructor(mode = 'single', buttons = []) {
        super();
        this.mode = mode;
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    addButton(button) {
        if (button != null) {
            button.setRefToggleGroup(this);
            this.buttons.push(button);
        }
    }

    setButtons(buttons = []) {
        this.buttons = buttons;
        for (let btn of buttons) {
            btn.setRefToggleGroup(this);
        }
    }

    getButtons() {
        return this.buttons;
    }

    getActives() {
        let actives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'active')
                actives.push(btn);
        return actives;
    }

    getInactives() {
        let inactives = [];
        for (let btn of this.buttons)
            if (btn.currentState === 'inactive')
                inactives.push(btn);
        return inactives;
    }

    getPixiChildren() {
        let elements = [];
        for (let toggleButton of this.buttons)
            for (let element of toggleButton.getPixiChildren())
                elements.push(element);
        return elements;
    }

    updateList(button) {
        if (this.mode === 'single' && button.isActive()) {
            for (let btn of this.buttons) {
                if (btn !== button && btn.isActive()) {
                    btn.toggle();
                }
            }
        }
    }

    reset(){
        for (let btn of this.buttons){
            if(btn.isActive())
                btn.toggle();
        }
    }

    show() {
        for (let btn of this.buttons)
            btn.show();
    }

    hide() {
        for (let btn of this.buttons)
            btn.hide();
    }

    updateFont(font){
        for (let btn of this.buttons) {
            btn.updateFont(font);
        }
    }

    clearButtons(){
        this.buttons = [];
    }


    getY() {
        return 0;
    }

    getX() {
        return 0;
    }

    setVisible(visible) {
        for (let btn of this.buttons)
            btn.setVisible(visible);
    }
}