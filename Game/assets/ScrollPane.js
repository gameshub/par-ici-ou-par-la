class ScrollPane extends Asset {


    constructor(x, y, width, height) {
        super();
        this.x = x | 0;
        this.y = y | 0;
        this.width = width | 0;
        this.height = height | 0;
        this.index = 1;

        this.maxIndex = 0;

        this.touchScrollLimiter = 0;
        this.lastTouch = 0;
        this.data = {};
        this.dragging = false;

        this.elements = [];

        this.graphics = {
            elementContainer: new PIXI.Container(),
            scrollBar: new PIXI.Graphics(),
            scrollButton: new PIXI.Graphics()
        };

        this.graphics.scrollButton
            .on('mousedown', this.onDragStart.bind(this))
            .on('touchstart', this.onDragStart.bind(this))
            .on('mouseup', this.onDragEnd.bind(this))
            .on('mouseupoutside', this.onDragEnd.bind(this))
            .on('touchend', this.onDragEnd.bind(this))
            .on('touchendoutside', this.onDragEnd.bind(this))
            .on('mousemove', this.onDragMove.bind(this))
            .on('touchmove', this.onDragMove.bind(this));


        let canvas = document.getElementById('canvas');
        canvas.addEventListener('wheel', function (event) {
            this.index += (event.deltaY > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length));
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }.bind(this));
        canvas.addEventListener('touchmove', function (event) {
            if (this.lastTouch === 0)
                this.lastTouch = event.touches[0].clientY;
            this.touchScrollLimiter += (this.lastTouch - event.touches[0].clientY);
            this.lastTouch = event.touches[0].clientY;
            if ((this.touchScrollLimiter > (this.maxIndex / this.elements.length))
                || (this.touchScrollLimiter < -(this.maxIndex / this.elements.length))) {
                this.index += this.touchScrollLimiter > 0 ? (this.height / this.elements.length) : -(this.height / this.elements.length);
                this.touchScrollLimiter = 0;
                if (this.index > this.height - 31)
                    this.index = (this.height - 31);
                else if (this.index <= 1)
                    this.index = 1;
                this.scroll();
            }
        }.bind(this));
        canvas.addEventListener('touchend', function (event) {
            this.lastTouch = 0;
        }.bind(this));
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
        this.init();
    }

    addElements(...elements) {
        for (let element of elements)
            this.elements.push(element);
    }

    init() {

        this.graphics.scrollBar.clear();
        this.graphics.scrollBar.beginFill(0xDDDDDD);
        this.graphics.scrollBar.drawRect(this.x + this.width - 18, this.y, 18, this.height);
        this.graphics.scrollBar.endFill();

        this.graphics.scrollButton.clear();
        this.graphics.scrollButton.beginFill(0x999999);
        this.graphics.scrollButton.drawRect(this.x + this.width - 17, this.y + 1, 16, 30);
        this.graphics.scrollButton.endFill();

        this.graphics.scrollButton.interactive = true;
        this.graphics.scrollButton.buttonMode = true;

        let y = this.y;
        for (let element of this.elements) {
            if (element instanceof Asset) {
                element.setY(y);
                element.setX((this.width-20) / 2);
                y += element.getHeight();
                element.setVisible(this.checkIsInContainer(element.getY(), element.getHeight() / 2));
                for (let pc of element.getPixiChildren())
                    this.graphics.elementContainer.addChild(pc);
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                element.y = y;
                y += (typeof element.height !== 'undefined' ? element.height : 25);
                element.visible = this.checkIsInContainer(element.y);
                this.graphics.elementContainer.addChild(element);
                element.x = this.width / 2;
            }
            y += 5;
        }
        this.maxIndex = y;

        this.graphics.scrollButton.visible = (this.height < this.maxIndex);
        this.graphics.scrollBar.visible = (this.height < this.maxIndex);

        this.scroll();
    }

    clear() {
        this.elements = [];
    }

    scroll() {
        if (this.elements.length === 0)
            return;

        this.graphics.scrollButton.position.y = this.index;

        let firstDisplayedIndex = Math.floor((this.elements.length - 1) * (this.index / this.height));

        let posY = 1 + this.y + this.elements[firstDisplayedIndex].getHeight()/2;

        for (let i = 0; i < this.elements.length; i++) {
            let element = this.elements[i];
            if (element instanceof Asset) {
                if (i < firstDisplayedIndex)
                    element.setVisible(false);
                else {
                    element.setY(posY);
                    element.setVisible(this.checkIsInContainer(element.getY()));
                    posY += element.getHeight() + 5;
                }
            } else if (typeof element.y !== 'undefined' && typeof element.visible !== 'undefined') {
                if (i < firstDisplayedIndex)
                    element.visible = false;
                else {
                    element.y = posY;
                    element.visible = this.checkIsInContainer(element.y);
                    posY += element.height + 5;
                }
            }
            if(element instanceof ToggleButton)
                element.updateView();
        }
    }


    checkIsInContainer(posY, compensation = 0) {
        return (posY < (this.y + this.height) && (posY > (this.y + compensation)));
    }

    getPixiChildren() {
        let elements = [];
        for (let e in this.graphics)
            if (this.graphics[e] instanceof Asset)
                for (let element of this.graphics[e].getPixiChildren())
                    elements.push(element);
            else
                elements.push(this.graphics[e]);
        return elements;
    }


    getY() {
        return this.y;
    }

    getX() {
        return this.x;
    }

    setVisible(visible) {

    }

    onDragStart(event) {
        this.data = event.data;
        this.graphics.scrollButton.alpha = 0.5;
        this.dragging = true;
    }

    onDragEnd() {
        this.graphics.scrollButton.alpha = 1;
        this.dragging = false;
        this.data = null;
    }

    onDragMove() {
        if (this.dragging) {
            let newPosition = this.data.getLocalPosition(this.graphics.scrollButton.parent);
            this.index = newPosition.y - this.y;
            if (this.index > this.height - 31)
                this.index = (this.height - 31);
            else if (this.index <= 1)
                this.index = 1;
            this.scroll();
        }
    }
}