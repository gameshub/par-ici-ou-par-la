class TrainMode extends Mode {

    constructor(refGame) {
        super('train');
        this.refGame = refGame;
        this.evaluate = false;
        this.setInterfaces({
            train: new Train(refGame)
        });
    }

    /**
     * Initialise le mode création. Demande le niveau Harmos de l'exercice.
     */
    init(evaluate) {
        this.evaluate = evaluate;
    }

    /**
     * Affiche la page de garde de la création
     */
    show(exercice = undefined) {
        this.interfaces.train.show(this.evaluate, exercice);


       //  console.log(this.exercice);
    }

    /**
     * Fonctione de callback appelée lors d'une changement de langue.
     * Affiche la consigne de la question dans la nouvelle langue.
     * @param {string} lang la nouvelle langue
     */
    onLanguageChanged(lang) {
        this.interfaces.train.refreshLang(lang);
    }


    onFontChange(isOpendyslexic) {
        this.interfaces.train.refreshFont(isOpendyslexic)
    }


    grille() {
        var x = []
        var y = []
    }
}
