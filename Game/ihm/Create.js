// const { MultiStyleText } = require("pixi-multistyle-text");
class Create extends Interface {

    constructor(refGame) {
        super(refGame, "create");
        this.refGame = refGame;

        this.initializeVarThis();

        // grid implmentation d'image
        this.grid = new DrawingGrid(5, 4, 100, this.scene);

        // grid implémentation des lignes
        this.gridEnd = new DrawingGrid(11, 9, 50, this.scene);
    }

    initializeVarThis(){
        this.gameName = [];
        this.tableauGridLogo = [];
        this.tableauPoint = [];
        this.langue = {};
        this.traduction = [];
        this.listTrad = [];
        this.nbrePointToCheck = -1;
        this.finishEnable = false;
        this.removeGrid = false;
        this.helpWays = false;
        this.nbreLangueCheck = 1;
        this.nbreLangue = 4;
    }

    show() {
        this.clear();
        this.elements = [];

        this.grid.init();

        this.refGame.global.util.hideTutorialInputs('next', 'btnReset', 'tuto1', 'tuto2', 'tuto3', "btnLockWays", 'btnValider', 'btnResetTr', "btnConfiguration");
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('welcomeCreate'));
        this.mainTitle = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
        this.startButton = new Button(300, 400, '', 0x32CD32, 0x000000, true);

        // Configuration de la partie des assets
        this.mainTitle.anchor.set(0.5);
        this.mainTitle.x = 300;
        this.mainTitle.y = 150;
        this.startButton.setOnClick(this.startCreateMode.bind(this));
        this.mainTitle.text = this.refGame.global.resources.getOtherText("welcomeCreate");
        this.startButton.setText(this.refGame.global.resources.getOtherText("btnStartCreate"));


        this.elements.push(this.mainTitle);
        this.elements.push(this.startButton);
        this.clear();
        this.refreshLang(this.refGame.global.resources.getLanguage());
        this.init();
    }

    refreshLang(lang) {
    }

    refreshFont(isOpenDyslexic) {
    }

    /** Partie des fonctions utilités */

    /**
     * Méthode qui permet l'affichage de la page "createMap"
     */
    startCreateMode() {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('step2'));
        // Suppresion des éléments d'avant
        this.clear();
        this.elements = [];
        this.saveNextButtonTranslate = new Button(475, 550, '', 0x32CD32, 0x000000, true);
        let backgroundImage = this.refGame.global.resources.getImage('backgroundCreate').image.src;
        let insertBack = new ImgBack(300, 202, 509, 409, backgroundImage, '');


        // Configuration des assets
        this.saveNextButtonTranslate.setOnClick(this.startTranslate.bind(this));
        this.saveNextButtonTranslate.setText(this.refGame.global.resources.getOtherText("saveNext"));

        this.grid.setVisiblePoint(false);
        this.grid.setModeCreate(true);
        this.grid.setRefGameDG(this.refGame);
        this.grid.setRefCreate(this);

        // on implémente les éléments crée dans l'affichage
        this.elements.push(insertBack);
        this.elements.push(this.grid);
        this.elements.push(this.saveNextButtonTranslate);

        // on remet les images si il y a déjà des images d'implenter
        if (this.tableauGridLogo.length !== 0) {
            this.grid.setLogoCreate(this.tableauGridLogo);
            let tableTemp = this.grid.getLogoCreate();
            this.grid.setBlockNode(false);
            this.grid.setBlockLogo(false);
            for (let i = 0; i < tableTemp.length; i++) {
                if (tableTemp[i] !== null) {
                    this.elements.push(tableTemp[i]);
                }
            }
        }
        this.init();
    }

    /**
     * Méthode qui permet l'affichage de la page "Translate"
     */
    startTranslate() {
        this.tableauGridLogo = this.grid.getLogoCreate();
        this.grid.setBlockNode(true);
        this.grid.setBlockLogo(true);

        //option pour la textarea
        var options = {
            value: "",
            placeholder: "",
            placeholderColor: "#999",
            width: 400,
            height: 85,
            padding: 5,
            borderColor: "black",
            borderWidth: 2,
            borderRadius: 5,
            backgroundColor: "#fff",
            boxShadow: null,
            innerShadow: null,
            text: {
                font: "14px Arial",
                fill: "#000",
                align: "left",
                lineHeight: 20
            }
        };

        // On crée les éléments dont nous avons besoin
        this.btnReturn = new Button(150, 550, '', 0x32CD32, 0x000000, true);
        this.translate = new PIXI.Textarea(options);

        // Configuration des éléments
        this.saveNextButtonTranslate.setOnClick(this.nextLangage.bind(this));
        this.saveNextButtonTranslate.setText(this.refGame.global.resources.getOtherText("saveNext"));
        this.btnReturn.setOnClick(this.returnLangage.bind(this));
        this.btnReturn.setText(this.refGame.global.resources.getOtherText("return"));
        this.translate.y = 420;
        this.translate.x = 100;

        // On enlève tous et on clear l'interface
        this.clear();
        this.elements = [];

        // On remet le tout
        let backgroundImage = this.refGame.global.resources.getImage('backgroundCreate').image.src;
        let insertBack = new ImgBack(300, 202, 509, 409, backgroundImage, '');
        this.elements.push(insertBack);
        this.elements.push(this.grid);

        for (let i = 0; i < this.tableauGridLogo.length; i++) {
            if (this.tableauGridLogo[i] !== null) {
                this.elements.push(this.tableauGridLogo[i]);
            }
        }
        this.elements.push(this.saveNextButtonTranslate);
        this.elements.push(this.btnReturn);
        this.elements.push(this.translate);

        this.retourLangues = false
        this.changeLanguage(this.nbreLangueCheck);
        // on regarde s'il n'est pas déjà passé dans ce mode
        if (this.listTrad.length !== 0) this.translate.value = this.listTrad[0].text;

        this.init();
    }

    removeCarriageReturnsInText = () => {
        const array = Array.from(this.translate.value)
        const arrayWithoutCarriageReturn = array.map((char) => {
            return char === '\n' ? 'RETURN' : char
        })

        this.translate.value = arrayWithoutCarriageReturn.join('');
        this.translate.array = arrayWithoutCarriageReturn;
    }

    addCarriageReturnInText = () => {
        let result = "";
        if (this.translate.array) {
            const arrayWithCarriageReturn = this.translate.array.map((char) => {
                return char === 'RETURN' ? '\\n' : char
            })
            result = arrayWithCarriageReturn.join('');
        }
        return result;
    }

    /**
     * Bouton qui permet d'avancer dans la page "Translate"
     */
    nextLangage() {
        this.removeCarriageReturnsInText();
        this.retourLangues = false;
        if (this.nbreLangueCheck !== 5) this.nbreLangueCheck += 1;
        this.changeLanguage(this.nbreLangueCheck);
        var valueLangue;

        if (this.nbreLangueCheck - 2 === this.listTrad.length) {
            valueLangue = JSON.parse(this.langue);
            this.listTrad.push(valueLangue);
            this.translate.value = "";
        } else if (this.nbreLangueCheck - 1 === this.listTrad.length) {
            this.translate.value = "";
        } else {
            valueLangue = JSON.parse(this.langue);
            this.listTrad[this.nbreLangueCheck - 2] = valueLangue;
            this.translate.value = this.listTrad[this.nbreLangueCheck - 1].text;
        }
        if (this.nbreLangueCheck === 5){
            valueLangue = JSON.parse(this.langue);
            this.listTrad[this.nbreLangueCheck - 2] = valueLangue;
            this.translate.value = this.listTrad[this.nbreLangueCheck - 2].text;
        }
    }

    /**
     * Bouton qui permet de reculer dans la page "Translate"
     */
    returnLangage() {
        this.nbreLangueCheck--;

        if (this.nbreLangueCheck === 0) {
            // on désactive le textarea
            this.translate.blur();
            this.translate.value = "";
            this.nbreLangueCheck = 1;
            this.changeLanguage(this.nbreLangueCheck);
            this.startCreateMode();
            this.grid.setBlockNode(false);
        } else {

            this.changeLanguage(this.nbreLangueCheck);
            this.translate.value = this.listTrad[this.nbreLangueCheck - 1].text;
        }
    }

    /**
     * Méthode qui permet l'affichage de la page "Options"
     */
    optionsPage() {
        // on change le texte en haut à gauche
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInForm'));

        // On enlève tous et on clear l'interface
        this.clear();
        this.elements = [];
        console.log(this.nbreLangueCheck);

        if (typeof this.lblNbrePointToCheck === "undefined") {
            // On crée les éléments de l'interface
            this.btnSaveNextButtonOptions = new Button(475, 550, '', 0x32CD32, 0x000000, true);
            this.btnReturnLangage = new Button(150, 550, '', 0x32CD32, 0x000000, true);
            this.lblNbrePointToCheck = new PIXI.Text('', {
                fontFamily: 'Arial',
                fontSize: 16,
                fill: 0x000000,
                align: 'center'
            });
            this.lblNbrePointToCheck2 = new PIXI.Text('', {
                fontFamily: 'Arial',
                fontSize: 16,
                fill: 0x000000,
                align: 'center'
            });
            this.txtNbrePoint = new PIXI.TextInput({
                input: {
                    fontSize: '16px',
                    padding: '6px',
                    width: '40px',
                    color: '#26272E'
                },
                box: {
                    default: {fill: 0xE8E9F3, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
                    focused: {fill: 0xE1E3EE, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
                    disabled: {fill: 0xDBDBDB, rounded: 12}
                }
            });

            this.cbxActiveHelpWays = new CheckBox(400, 500, "", 1);
            this.cbxActiveRemoveGrid = new CheckBox(400, 450, "", 1);

            // Configuration des éléments
            this.btnSaveNextButtonOptions.setOnClick(this.endPage.bind(this));
            this.btnSaveNextButtonOptions.setText(this.refGame.global.resources.getOtherText("saveNext"));
            this.btnReturnLangage.setOnClick(this.returnOptions.bind(this));
            this.btnReturnLangage.setText(this.refGame.global.resources.getOtherText("return"));
            this.lblNbrePointToCheck.anchor.set(0.5);
            this.lblNbrePointToCheck.x = 275;
            this.lblNbrePointToCheck.y = 115;
            this.lblNbrePointToCheck2.anchor.set(0.5);
            this.lblNbrePointToCheck2.x = 255;
            this.lblNbrePointToCheck2.y = 135;

            let tradNbrePoint = this.refGame.global.resources.getOtherText("txtNbrePoint");
            let tradDiv = tradNbrePoint.split("*sup*");
            this.lblNbrePointToCheck.text = tradDiv[0];
            this.lblNbrePointToCheck2.text = tradDiv[1];

            this.cbxActiveHelpWays.setX(75);
            this.cbxActiveHelpWays.setY(250);
            this.cbxActiveHelpWays.setText(this.refGame.global.resources.getOtherText("activeHelpWays"));
            this.cbxActiveHelpWays.select(false);
            this.cbxActiveRemoveGrid.setX(75);
            this.cbxActiveRemoveGrid.setY(350);
            this.cbxActiveRemoveGrid.setText(this.refGame.global.resources.getOtherText("activeRemoveGrid"));
            this.cbxActiveRemoveGrid.select(false);
            this.txtNbrePoint.x = 250;
            this.txtNbrePoint.y = 160;
        }

        /** On implémente dans l'interface */
        this.elements.push(this.btnSaveNextButtonOptions);
        this.elements.push(this.btnReturnLangage);
        this.elements.push(this.lblNbrePointToCheck);
        this.elements.push(this.lblNbrePointToCheck2);
        this.elements.push(this.txtNbrePoint);
        this.elements.push(this.cbxActiveHelpWays);
        this.elements.push(this.cbxActiveRemoveGrid);
        //this.elements.push(text);

        // Affichage
        this.init();
    }

    /**
     * Bouton qui permet de d'aller à la page "endPage"
     */
    endPage() {
        this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo1'));
        // On récupère les valeurs de la page précédente
        this.removeGrid = this.cbxActiveRemoveGrid.isChecked();
        this.helpWays = this.cbxActiveHelpWays.isChecked();
        this.nbrePointToCheck = parseInt(this.txtNbrePoint.text);

        // Initialisation des variables
        this.nbreEnd = 0;
        this.tableauPointToJSON = [];

        if (this.nbrePointToCheck > 0 && this.nbrePointToCheck < 11) {
            // Suppresion des éléments d'avant
            this.clear();
            this.elements = [];

            // on crée un nouveau grid pour les traits
            this.gridEnd.setVisiblePoint(false);
            this.gridEnd.setModeCreate(false);
            this.gridEnd.setRefGameDG(this.refGame);
            this.gridEnd.setRefCreate(this);
            this.gridEnd.setCreateWays(true);

            // option pour la textarea
            var options = {
                value: "",
                placeholder: "",
                placeholderColor: "#999",
                width: 450,
                height: 60,
                padding: 5,
                borderColor: "black",
                borderWidth: 2,
                borderRadius: 5,
                backgroundColor: "#fff",
                boxShadow: null,
                innerShadow: null,
                text: {
                    font: "14px Arial",
                    fill: "#000",
                    align: "left",
                    lineHeight: 20
                }
            };

            // on ajoute dans l'interface les images qu'on a ajouté précédemment
            for (let i = 0; i < this.tableauGridLogo.length; i++) {
                if (this.tableauGridLogo[i] !== null) {
                    this.elements.push(this.tableauGridLogo[i]);
                }
            }

            console.log("allo ?");


            // On crée les éléments dont nous avons besoin
            this.returnEndPage = new Button(75, 550, '', 0x32CD32, 0x000000, true);
            this.btnReset = new Button(260, 550, '', 0x32CD32, 0x000000, true);
            this.saveAll = new Button(475, 550, '', 0x32CD32, 0x000000, true);
            this.saveNextButtonEndPage = new Button(475, 550, '', 0x32CD32, 0x000000, true);
            this.txtTranslateHelp = new PIXI.Textarea(options);

            // on configure les éléments crée
            this.returnEndPage.setOnClick(this.returnEndPageF.bind(this));
            this.returnEndPage.setText(this.refGame.global.resources.getOtherText("return"));
            this.btnReset.setOnClick(this.resetGridEnd.bind(this));
            this.btnReset.setText(this.refGame.global.resources.getOtherText("btnResetCreate"));
            this.saveNextButtonEndPage.setOnClick(this.saveNextEndPage.bind(this));
            this.saveNextButtonEndPage.setText(this.refGame.global.resources.getOtherText("saveNext"));
            this.saveAll.setOnClick(this.namePage.bind(this));
            this.saveAll.setText(this.refGame.global.resources.getOtherText("saveNext"));

            // on regarde la langue sélectionne
            switch (this.refGame.global.resources.getLanguage()) {
                case "fr" :
                    this.langCurrent = 1;
                    break;
                case "de" :
                    this.langCurrent = 2;
                    break;
                case "it" :
                    this.langCurrent = 3;
                    break;
                case "en" :
                    this.langCurrent = 4;
                    break;
            }
            this.txtTranslateHelp.value = this.listTrad[this.langCurrent - 1].text;
            this.txtTranslateHelp.y = 450;
            this.txtTranslateHelp.x = 50;
            this.txtTranslateHelp.interactive = false;

            // on implémente dans l'interface
            this.elements.push(this.gridEnd);
            this.elements.push(this.returnEndPage);
            this.elements.push(this.txtTranslateHelp);
            this.elements.push(this.saveNextButtonEndPage);
            this.elements.push(this.saveAll);
            this.elements.push(this.btnReset);

            this.saveAll.setVisible(false);

            // on affiche l'interface
            this.init();
        } else {
            Swal.fire({
                icon: 'error',
                text: this.refGame.global.resources.getOtherText('notCorrectOptions')
            });
        }
    }

    /**
     * Bouton qui permet de revenir à la page "Translate"
     */
    returnOptions() {
        // On enlève tous et on clear l'interface
        this.clear();
        this.elements = [];

        this.elements.push(this.grid);
        for (let i = 0; i < this.tableauGridLogo.length; i++) {
            if (this.tableauGridLogo[i] !== null) {
                this.elements.push(this.tableauGridLogo[i]);
            }
        }

        this.elements.push(this.saveNextButtonTranslate);
        this.elements.push(this.btnReturn);
        this.elements.push(this.translate);
        this.nbreLangueCheck = 4;
        this.retourLangues = true;
        this.changeLanguage(this.nbreLangueCheck);

        this.init();
    }

    /**
     * Bouton qui permet de revenir à la page "Options"
     */
    returnEndPageF() {
        if (this.nbreEnd >= 1) {
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo' + (this.nbreEnd + 2)));
            this.nbreEnd--;
            this.changeDrawingLine();
        } else {
            this.gridEnd.reset();
            this.optionsPage();
        }
    }

    /**
     * Bouton qui terminer la end page
     */
    async saveNextEndPage() {
        this.tableauPoint = this.gridEnd.getPoint();
        this.grid.setBlockNode(true);
        if (this.tableauPoint.length !== 3 && this.tableauPoint.length !== 1) {
            Swal.fire({
                icon: 'error',
                text: this.refGame.global.resources.getOtherText('notCorrectPoint')
            });
            this.resetGridEnd();
        } else {
            if (this.nbrePointToCheck >= this.nbreEnd + 1) {
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo' + (this.nbreEnd + 2)));
                /** On vérifie que l'élève n'a pas annulé l'action */
                let result = await this.popupTextEndPage();

                if(typeof result !== "undefined"){
                    console.log("undifined state");
                    this.traduction[this.nbreEnd] = result;
                    this.nbreEnd++;
                    this.changeDrawingLine();
                } else {
                    if(this.nbreEnd === 0){
                        console.log("number = 0 state");
                        this.changeDrawingLine();
                    } else {
                        console.log("else state");
                        this.nbreEnd--;
                        this.changeDrawingLine();
                    }
                }
            }
        }
    }

    /**
     * la page de nommage du niveau
     */
    namePage(){
        //Clear du canvas
        this.clear();
        this.elements = [];


        //variables d'options pour les différents élémets

        let otpionsLBL = {
            fontFamily: 'Arial',
            fontSize: 16,
            fill: 0x000000,
            align: 'center'
        };


        //Création des éléments
        this.btnReturn = new Button(150, 550, '', 0x32CD32, 0x000000, true);
        this.btnsaveAll = new Button(475, 550, '', 0x32CD32, 0x000000, true);

        this.btnsaveNextNamePage = new Button(475, 550, '', 0x32CD32, 0x000000, true);

        this.txaTitleFr = new PIXI.Textarea({
                value: "",
                placeholder: "",
                placeholderColor: "#999",
                width: 200,
                height: 25,
                padding: 5,
                borderColor: "black",
                borderWidth: 2,
                borderRadius: 3,
                backgroundColor: "#fff",
                boxShadow: null,
                innerShadow: null,
                text: {
                    font: "14px Arial",
                    fill: "#000",
                    align: "left",
                    lineHeight: 20
                }
            }
            );
        this.lblTitleFr = new PIXI.Text('Nom du jeux en Français :', otpionsLBL);

        this.txaTitleDe = new PIXI.Textarea({
            value: "",
            placeholder: "",
            placeholderColor: "#999",
            width: 200,
            height: 25,
            padding: 5,
            borderColor: "black",
            borderWidth: 2,
            borderRadius: 3,
            backgroundColor: "#fff",
            boxShadow: null,
            innerShadow: null,
            text: {
                font: "14px Arial",
                fill: "#000",
                align: "left",
                lineHeight: 20
            }
        });
        this.lblTitleDe = new PIXI.Text('Nom du jeux en Allemand :', otpionsLBL);

        this.txaTitleIt = new PIXI.Textarea({
            value: "",
            placeholder: "",
            placeholderColor: "#999",
            width: 200,
            height: 25,
            padding: 5,
            borderColor: "black",
            borderWidth: 2,
            borderRadius: 3,
            backgroundColor: "#fff",
            boxShadow: null,
            innerShadow: null,
            text: {
                font: "14px Arial",
                fill: "#000",
                align: "left",
                lineHeight: 20
            }
        });
        this.lblTitleIt = new PIXI.Text('Nom du jeux en Italien :', otpionsLBL);

        this.txaTitleEn = new PIXI.Textarea({
            value: "",
            placeholder: "",
            placeholderColor: "#999",
            width: 200,
            height: 25,
            padding: 5,
            borderColor: "black",
            borderWidth: 2,
            borderRadius: 3,
            backgroundColor: "#fff",
            boxShadow: null,
            innerShadow: null,
            text: {
                font: "14px Arial",
                fill: "#000",
                align: "left",
                lineHeight: 20
            }
        });
        this.lblTitleEn = new PIXI.Text('Nom du jeux en Anglais :', otpionsLBL);
        console.log(this.txaTitleFr === this.txaTitleIt);

        //Paramétrage des éléments
        this.btnReturn.setOnClick(this.returntoEndPage.bind(this));
        this.btnReturn.setText(this.refGame.global.resources.getOtherText("return"));
        this.btnsaveAll.setOnClick(this.saveNextNamePage.bind(this));
        this.btnsaveAll.setText(this.refGame.global.resources.getOtherText("finish"));
        this.refGame.global.util.setTutorialText("Entrer le nom de votre niveau. (Français obligatoire)");

        this.lblTitleFr.x = 75;
        this.lblTitleFr.y = 125;
        this.txaTitleFr.x = 275;
        this.txaTitleFr.y = 115;

        this.lblTitleDe.x = 70;
        this.lblTitleDe.y = 205;
        this.txaTitleDe.x = 275;
        this.txaTitleDe.y = 195;

        this.lblTitleIt.x = 93;
        this.lblTitleIt.y = 285;
        this.txaTitleIt.x = 275;
        this.txaTitleIt.y = 275;

        this.lblTitleEn.x = 85;
        this.lblTitleEn.y = 365;
        this.txaTitleEn.x = 275;
        this.txaTitleEn.y = 355;

        this.txaTitleFr.autofocus = false;
        this.txaTitleDe.autofocus = false;
        this.txaTitleEn.autofocus = false;
        this.txaTitleIt.autofocus = false;

        //Ajout des éléments
        this.elements.push(this.btnsaveAll);
        this.elements.push(this.btnReturn);

        this.elements.push(this.txaTitleFr);
        this.elements.push(this.lblTitleFr);

        this.elements.push(this.txaTitleDe);
        this.elements.push(this.lblTitleDe);

        this.elements.push(this.txaTitleIt);
        this.elements.push(this.lblTitleIt);

        this.elements.push(this.txaTitleEn);
        this.elements.push(this.lblTitleEn);


        // Affichage
        this.init();

    }

    /**
     * Méthode sauvegardant le nom du niveau
     */
    saveNextNamePage(){
        if ((this.txaTitleFr.value != null) && (this.txaTitleFr.value != "")){
            if(this.isValidAttribute(this.txaTitleFr.value)&&this.isValidAttribute(this.txaTitleDe.value)&&this.isValidAttribute(this.txaTitleIt.value)&&this.isValidAttribute(this.txaTitleEn.value)){
                this.gameName[0] = this.txaTitleFr.value;
                this.gameName[1] = this.txaTitleDe.value;
                this.gameName[2] = this.txaTitleIt.value;
                this.gameName[3] = this.txaTitleEn.value;

                this.saveAllF();
            } else {
                Swal.fire({
                    icon: 'error',
                    text: 'L\'un des champs n\'est pas valide !'
                });
            }

        }else{
            Swal.fire({
                icon: 'error',
                text: 'Le nom du jeux en Français est obligatoire !'
            });
        }
    }

    /**
     * retourne à la end page
     */
    returntoEndPage(){
        this.endPage();
    }

    /**
     * controle si un sting ne contiens pas de caractères sensible. ( ", /, ')
     *
     * @param string : le string a vérifier
     * @returns {boolean} : si le string contiens un caractère sensible, retourne false, sinon retourne true.
     */
    isValidAttribute(string){
        let validate;
        if (string.includes('"') || string.includes("/") || string.includes("'")){
            validate = false;
        }else{
            validate = true;
        }
        return validate;
    }

    /**
     * Méthode qui réinitilise l'interface
     */
    resetGridEnd() {
        this.gridEnd.reset();
    }

    /**
     * Bouton qui permet de créer le JSON pour l'exercice
     */
    saveAllF() {
        this.arrayStartEnd = this.grid.getPointStartEnd();
        if ((this.arrayStartEnd.length === 2) || (this.arrayStartEnd.length === 1) || (this.arrayStartEnd.length === 0)) {
            let coordStart = this.arrayStartEnd[0];
            let coordEnd = this.arrayStartEnd[1];
            // JSON de fin pour envoyer dans la base de données
            var JSONNewExercice = {};

            /**  Pour donner un nom au niveau dans le Json*/
            JSONNewExercice= '{"name":{';
            JSONNewExercice += '"fr" : "'+this.gameName[0]+'",';
            JSONNewExercice += '"de" : "'+this.gameName[1]+'",';
            JSONNewExercice += '"en" : "'+this.gameName[2]+'",';
            JSONNewExercice += '"it" : "'+this.gameName[3]+'"';
            JSONNewExercice += '},';

            /** La partie verif du JSON */
            JSONNewExercice += '"verif" : {' +
                '"debut" : {' +
                '"1" : "'+ coordStart +'"' +
                '},';
            JSONNewExercice += '"fin" : {' +
                '"1" : "'+ coordEnd +'"' +
                '},';
            /** Les points obligatoires à passer */
            JSONNewExercice += '"segments" : {';
            for (let i = 0; i < this.tableauPointToJSON.length ; i++) {
                i++;
                if(i !== this.tableauPointToJSON.length){
                    JSONNewExercice += '"'+ i +'" : "'+ this.tableauPointToJSON[i-1] +'",'
                } else {
                    JSONNewExercice += '"'+ i +'" : "'+ this.tableauPointToJSON[i-1] +'"';
                }
                i--;
            }
            JSONNewExercice += '}';
            // on ferme celui du verif
            JSONNewExercice += '},';

            /** la partie des options */
            JSONNewExercice += '"enableLockWays" : ' + this.helpWays + ',';
            JSONNewExercice += '"enableLockMap" : ' + this.removeGrid + ',';


            /* la partie background
                JSONNewExercice["background"] = ;
             */

            /** la partie erreur */
            JSONNewExercice += '"error" : {';
            for (let i = 0; i < this.traduction.length ; i++) {
                if((i+1) !== this.traduction.length){
                    JSONNewExercice += '"'+i+'" : {';
                    JSONNewExercice += '"fr" : "'+this.traduction[i][0]+'",';
                    JSONNewExercice += '"de" : "'+this.traduction[i][1]+'",';
                    JSONNewExercice += '"en" : "'+this.traduction[i][2]+'",';
                    JSONNewExercice += '"it" : "'+this.traduction[i][3]+'"';
                    JSONNewExercice += '},';
                } else {
                    JSONNewExercice += '"'+i+'" : {';
                    JSONNewExercice += '"fr" : "'+this.traduction[i][0]+'",';
                    JSONNewExercice += '"de" : "'+this.traduction[i][1]+'",';
                    JSONNewExercice += '"en" : "'+this.traduction[i][2]+'",';
                    JSONNewExercice += '"it" : "'+this.traduction[i][3]+'"';
                    JSONNewExercice += '}';
                }
            }
            JSONNewExercice += '},';

            /** la partie background */
            JSONNewExercice += '"translate" : {';
            for (let i = 0; i < this.nbreLangue; i++) {
                switch(i+1) {
                    case 1 :
                        JSONNewExercice += '"fr" : "' + JSON.parse(this.fr).text + '",';
                        break;
                    case 2 :
                        JSONNewExercice += '"de" : "' + JSON.parse(this.de).text + '",';
                        break;
                    case 3 :
                        JSONNewExercice += '"en" : "' + JSON.parse(this.en).text + '",';
                        break;
                    case 4 :
                        JSONNewExercice += '"it" : "' + JSON.parse(this.it).text + '"';
                        break;
                }
            }
            JSONNewExercice += '},';


            /** la partie des images */
            JSONNewExercice += '"pictures" : {';
            for (let i = 0; i < this.tableauGridLogo.length; i++) {
                i++;
                if(i !== this.tableauGridLogo.length){
                    JSONNewExercice += '"'+ i +'" : {' +
                        '"nomFichier" : "'+  this.tableauGridLogo[i-1].name +'",' +
                        '"x" : "'+ this.tableauGridLogo[i-1].x +'",' +
                        '"y" : "'+ this.tableauGridLogo[i-1].y +'",' +
                        '"width" : "'+ this.tableauGridLogo[i-1].width +'",' +
                        '"nom" : {';

                    let listTrad = this.grid.getTradImage();
                    for (let j = 0; j < listTrad[i-1].length; j++) {
                        if(j !== listTrad[i-1].length-1){
                            JSONNewExercice += '"'+ j +'" : "'+ listTrad[i-1][j] +'",';
                        } else {
                            JSONNewExercice += '"'+ j +'" : "'+ listTrad[i-1][j] +'"';
                        }
                    }

                    JSONNewExercice += '}';
                    JSONNewExercice += '},';
                } else {
                    JSONNewExercice += '"'+ i +'" : {' +
                        '"nomFichier" : "'+  this.tableauGridLogo[i-1].name +'",' +
                        '"x" : "' + this.tableauGridLogo[i-1].x + '",' +
                        '"y" : "' + this.tableauGridLogo[i-1].y + '",' +
                        '"width" : "' + this.tableauGridLogo[i-1].width + '",' +
                        '"nom" : {';

                    let listTrad = this.grid.getTradImage();
                        for (let j = 0; j < listTrad[i-1].length; j++) {
                            if(j !== listTrad[i-1].length-1){
                                JSONNewExercice += '"'+ j +'" : "'+ listTrad[i-1][j] +'",';
                            } else {
                                JSONNewExercice += '"'+ j +'" : "'+ listTrad[i-1][j] +'"';
                            }
                        }

                    JSONNewExercice += '}';
                    JSONNewExercice += '}';

                }
                i--;
            }
            JSONNewExercice += '}';

            /** Pour fermer l'ensemble du JSON */
            JSONNewExercice += '}';

            console.log(JSONNewExercice);

            /** J'envoie dans la base de données le JSON et j'affiche un message de reception */
            this.refGame.global.resources.saveExercice(JSONNewExercice, function (result) {
                Swal.fire(
                    result.message,
                    undefined,
                    result.type
                );
                if(result){
                    // On réinitialise toute la page
                    this.initializeVarThis();
                     this.grid.setLogoCreate(this.tableauGridLogo);
                     this.grid.setBlockNode(false);
                    this.txtNbrePoint.text = "";
                    this.clear();
                    this.elements = [];
                    this.show();
                }
            }.bind(this));


        } else {
            /** On affiche un message d'erreur car l'élève n'a pas mis un point de départ (start) et un point de fin (end) */
            Swal.fire({
                icon: 'error',
                text: this.refGame.global.resources.getOtherText('notSelectStartEnd')
            });
        }
    }

    /** Méthode intraClass utilisé seulement ici */

    /**
     * Fonction qui permet de mettre l'information dans langue et de changer les traductions
     * @param nbre le nbre en langue (1 = FR, 2 = DE, 3 = IT, 4 = EN)
     */
    changeLanguage(nbre) {
        const text = this.addCarriageReturnInText();
        switch (nbre - 1) {
            case 0 :
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('translateFR'));
                this.langue = '{"langue" : ' + nbre + ', "text" : "' + text + '"}';
                if (this.retourLangues) this.de = '{"text" : "' + text + '"}';
                break;
            case 1 :
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('translateDE'));
                this.langue = '{"langue" : ' + nbre + ', "text" : "' + text + '"}';
                this.retourLangues ? this.it = '{"text" : "' + text + '"}' : this.fr = '{"text" : "' + text + '"}';
                break;
            case 2 :
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('translateIT'));
                this.langue = '{"langue" : ' + nbre + ', "text" : "' + text + '"}';
                this.retourLangues ? this.en = '{"text" : "' + text + '"}' : this.de = '{"text" : "' + text + '"}';
                break;
            case 3 :
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('translateEN'));
                this.langue = '{"langue" : ' + nbre + ', "text" : "' + text + '"}';
                if (!this.retourLangues) this.it = '{"text" : "' + text + '"}';
                break;
            case 4 :
                this.langue = '{"langue" : ' + nbre + ', "text" : "' + text + '"}';
                this.en = '{"text" : "' + text + '"}';
                // Vérifier que le français a bien été rempli
                if (typeof this.listTrad[0].text !== "undefined" && this.listTrad[0].text !== "") {
                    // on désactive le textarea
                    this.translate.blur();
                    this.optionsPage();
                } else {
                    Swal.fire({
                        icon: 'error',
                        text: this.refGame.global.resources.getOtherText('missingInfoLangage')
                    });
                }
                break;
        }
    }

    /**
     * Fonction qui permet d'afficher le tableau demandé de la page "createMap"
     * @param listLogo la liste des images à implenter
     */
    displayLogo(listLogo) {
        this.clear();
        this.elements = [];

        let backgroundImage = this.refGame.global.resources.getImage('backgroundCreate').image.src;
        let insertBack = new ImgBack(300, 202, 509, 409, backgroundImage, '');
        this.elements.push(insertBack);

        this.elements.push(this.grid);
        this.elements.push(this.saveNextButtonTranslate);

        for (let i = 0; i < listLogo.length; i++) {
            if (listLogo[i] !== null) {
                this.elements.push(listLogo[i]);
            }
        }
        this.init();
    }

    /**
     * Méthode qui permet de supprimer l'image donnée dans l'affichage
     * @param image est l'image qui sera supprimé
     */
    takeOffLogo(image) {
        let listLogo = this.grid.getLogoCreate();

        for (let i = 0; i < listLogo.length; i++) {
            if (listLogo[i] === image) {
                listLogo[i] = null;
            }
        }
        this.displayLogo(listLogo);
    }

    /**
     * Déffinit la in de l'action end page
     */
    displayNext(boolean) {
        this.finishEnable = boolean;
        if (boolean) {
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo' + (this.nbreEnd + 1)));
        } else {
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo' + (this.nbreEnd + 1)));
            this.gridEnd.reset();
        }
        this.btnReset.setVisible(!boolean);
        this.saveNextButtonEndPage.setVisible(!boolean);
        this.saveAll.setVisible(boolean);

    }

    /**
     * Permet de changer l'interface en fonction de la variable "nbreEnd"
     */
    changeDrawingLine() {
        if (this.finishEnable) this.displayNext(!this.finishEnable);
        if(this.nbreEnd === 0){
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo' + (this.nbreEnd + 1)));
            this.gridEnd.reset();
        }
        else if(this.nbreEnd === 10){
            if(this.tableauPoint[2] === undefined){
                this.tableauPointToJSON.push(this.tableauPoint[0].name + "/" + this.tableauPoint[0].name);
            } else {
                this.tableauPointToJSON.push(this.tableauPoint[0].name + "/" + this.tableauPoint[2].name);
            }
            this.gridEnd.reset();
            this.displayNext(true);
        } else {
            if(this.tableauPoint[2] === undefined){
                this.tableauPointToJSON.push(this.tableauPoint[0].name + "/" + this.tableauPoint[0].name);
            } else {
                this.tableauPointToJSON.push(this.tableauPoint[0].name + "/" + this.tableauPoint[2].name);
            }
            this.gridEnd.reset();
            if (this.nbreEnd === this.nbrePointToCheck)
                this.namePage();
            else
                this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('fillInfo' + (this.nbreEnd + 1)));
        }
    }

    /**
     * Popup pour que l'élève puisse dire ou est/sont les fautes
     */
    async popupTextEndPage() {
        const {value: text} = await Swal.fire({
            title: this.refGame.global.resources.getOtherText('introTrad'),
            html:
                '<textarea name="textarea" rows="4" cols="40" id="txtFR">'+JSON.parse(this.fr).text+'</textarea>' +
                '<br>' +
                '<textarea name="textarea" rows="4" cols="40" id="txtDE">'+JSON.parse(this.de).text+'</textarea>' +
                '<br>' +
                '<textarea name="textarea" rows="4" cols="40" id="txtIT">'+JSON.parse(this.it).text+'</textarea>' +
                '<br>' +
                '<textarea name="textarea" rows="4" cols="40" id="txtEN">'+JSON.parse(this.en).text+'</textarea>'
            ,
            focusConfirm: false,
            showCancelButton: true,
            preConfirm: () => {
                return [
                    document.getElementById('txtFR').value,
                    document.getElementById('txtDE').value,
                    document.getElementById('txtIT').value,
                    document.getElementById('txtEN').value
                ]
            }
        });
        return text;
    }

    padTo2Digits(num) {
        return num.toString().padStart(2, '0');
    }

    /**
     * formatte unedate en format dd/mm/yy
     * @param date la date a formaté
     * @returns {string} la date formatée
     */
    formatDate(date) {
        return [
            this.padTo2Digits(date.getDate()),
            this.padTo2Digits(date.getMonth() + 1),
            date.getFullYear(),
        ].join('/');
    }

}