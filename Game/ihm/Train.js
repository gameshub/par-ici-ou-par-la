class Train extends Interface {

    constructor(refGame) {
        super(refGame, "train");
        this.refGame = refGame;

        this.exercice = null;
        this.evaluate = false;

        this.title = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
        this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
        this.startButton.setOnClick(this.newGame.bind(this));

        this.title.x = 300;
        this.title.y = 150;
        this.title.anchor.set(0.5);

    }

    show(evaluate, exercice) {
        document.getElementById("btnAudioGame").style="display: none;";
        document.getElementById("btnLockMap").style="display: none;";
        this.gameid = 11;

        this.exerciceGive = exercice;
        for (let i = 0; i < this.elements.length; i++) {
            if (typeof this.elements[i].origin !== "undefined") {
                this.elements[i].reset();
            }
        }

        this.clear();
        this.elements = [];

        this.elements.push(this.title);
        this.elements.push(this.startButton);
        this.evaluate = evaluate;

        // suite
        this.refGame.global.util.hideTutorialInputs('next', 'btnReset', 'tuto1', 'tuto2', 'tuto3', "btnLockWays", 'btnValider', 'btnResetTr', "btnConfiguration");
        this.refreshLang(this.refGame.global.resources.getLanguage());

        this.init();
    }


    refreshLang(lang) {
        if (this.evaluate) {
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('welcomeEval'));
            this.title.text = this.refGame.global.resources.getOtherText('welcomeEval');
        } else {
            this.refGame.global.util.setTutorialText(this.refGame.global.resources.getOtherText('welcomeTrain'));
            this.title.text = this.refGame.global.resources.getOtherText('welcomeTrain');
        }
        this.startButton.setText(this.refGame.global.resources.getOtherText('btnStartCreate'));
        this.startButton.setVisible(true);

    }

    /**
     * Methode qui permet changer le texte de l'interieur du canvas quand on choisis la police OpenDyslexic
     * @param isOpenDyslexic {boolean} Est opendyslexic ou pas
     */
    refreshFont(isOpenDyslexic) {

    }

    newGame(){

        this.elements.unshift(this.title);
        this.elements.unshift(this.startButton);

        this.title.text = "";
        this.startButton.setVisible(false);

        if(typeof this.exerciceGive === "undefined"){
            this.exercice = this.refGame.global.resources.getExercice();
            if (forced){
                console.log("forced : "+forced);
                this.exercice = this.refGame.global.resources.getExerciceById(forced);
            }
            else{
                console.log("Not forced");
                this.exercice = this.refGame.global.resources.getExercice();
            }
            // this.exercice = this.refGame.global.resources.getExercice();
            this.currentExerciceId = this.exercice.id;

            // résoudre problème de retour à la ligne dans le JSON
            const array = Array.from(this.exercice.exercice);
            const arrayWithoutCarriageReturn = array.map((char) => {
                return char === '\n' ? 'RETURN' : char
            });
            this.exercice.exercice = arrayWithoutCarriageReturn.join('');

            this.exercice = JSON.parse(this.exercice.exercice);
            } else {
            this.exercice = this.exerciceGive;
            this.exercice = this.exercice.exercice;
        }

        // Affichage ou non de la checkbox pour bloquer le changement de couleur lors de la correction
        if(this.evaluate){
            // Cache les anciennes informations du mode "Explorer"
            this.refGame.global.util.hideTutorialInputs('next', 'btnReset', 'tuto1', 'tuto2', 'tuto3', "btnLockWays");
            // Permet d'afficher les boutons "Valider", "Réessayer" ...
            this.refGame.global.util.showTutorialInputs('btnValider', 'btnResetTr', "btnConfiguration");
            document.getElementById("tutorial").className='smoothie ';
            this.refGame.global.util.setTutorialText("<p class='scroller'>"+this.refGame.global.resources.getOtherText("tutoEval")+"</p>"+'<p style="font-size:20px;" class="scroller">'+this.exercice.translate.fr+"</p>");
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.handleValider.bind(this));
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnResetTr'), ['click'], this.handleReset.bind(this));
            this.refGame.global.util.hideTutorialInputs("btnLockWays");
            // Affichage ou non de la checkbox pour afficher les points
            // Permet d'afficher les boutons "Valider", "Réessayer" ...
            this.refGame.global.util.showTutorialInputs('btnValider', 'btnResetTr', "btnConfiguration");
            if(this.exercice.enableLockMap){
                this.refGame.global.util.showTutorialInputs("btnLockMap");
                this.refGame.global.util.showTutorialInputs("btnLockMapid");
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnLockMap'), ['click'], this.handleLockMap.bind(this));
            } else {
                this.refGame.global.util.hideTutorialInputs("btnLockMap");
            }
                // Modifier le texte qui va être lû par le bouton "Son"
               // this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.handleValider.bind(this));
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnResetTr'), ['click'], this.handleReset.bind(this));

            // On initialise la partie évaluation
            this.refGame.global.statistics.addStats(2);
        } else {
            // Permet d'afficher les boutons "Valider", "Réessayer" ...
            this.refGame.global.util.showTutorialInputs('btnValider', 'btnResetTr', "btnConfiguration");
            if(this.exercice.enableLockWays){
                this.refGame.global.util.showTutorialInputs("btnLockWays");
                this.refGame.global.util.showTutorialInputs("btnLockWaysid");
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnLockWays'), ['click'], this.handleLockWays.bind(this));
            } else {
                this.refGame.global.util.hideTutorialInputs("btnLockWays");
                this.refGame.global.util.showTutorialInputs("btnLockWaysid");
            }
            if(this.exercice.enableLockMap){
                this.refGame.global.util.showTutorialInputs("btnLockMap");
                this.refGame.global.util.showTutorialInputs("btnLockMapid");
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnLockMap'), ['click'], this.handleLockMap.bind(this));
            } else {
                this.refGame.global.util.hideTutorialInputs("btnLockMap");
                this.refGame.global.util.showTutorialInputs("btnLockMapid");
            }
            // Modifier le texte qui va être lû par le bouton "Son"
          //  let lang = this.refGame.global.resources.getLanguage();
           // let texte="this.exercice.translate."+this.refGame.global.resources.getLanguage();

          
            console.log(this.refGame.global.resources.getLanguage());

            let regex = "RETURN";
            this.langue=this.changeLang(this.refGame.global.resources.getLanguage());
            this.refGame.global.util.setTutorialText('<p style="font-size:20px;" class="scroller">'+this.langue.replaceAll(regex,'<br>')+"</p>");
            document.getElementById("tutorial").className='smoothie ';
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.handleValider.bind(this));
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnResetTr'), ['click'], this.handleReset.bind(this));
        }
        this.grid = new DrawingGrid(11, 9, 50, this.scene);
        this.grid.setRefGameDG(this.refGame);
        this.grid.reset();

        //background
        // let back = this.exercice.background;
        // let backImag = this.refGame.global.resources.getImage('background').image.src;
        // let insererBack = new ImgBack(back.x, back.y, back.width, back.height, backImag, 'background');
        // this.elements.push(insererBack, this.grid);
        // this.elements.push(insererBack);

        //images
        let count = 1;
        let lang = this.refGame.global.resources.getLanguage();
        let nbre;
        switch(lang){
            case "fr" :
                nbre = 0;
                break;
            case "de" :
                nbre = 1;
                break;
            case "it" :
                nbre = 2;
                break;
            case "en" :
                nbre = 3;
                break;
        }
        for (let key in this.exercice.pictures) {
            let imgNom = this.exercice.pictures[count];
            let img = this.refGame.global.resources.getImage(imgNom.nomFichier).image.src;
            let aInserer = new ImgStatic(imgNom.x, imgNom.y, imgNom.width, img, imgNom.nom[nbre], false, "");
            this.elements.push(aInserer);
            count++;
        }
        this.elements.push(this.grid);
        count = 1;

        this.tts = this.refGame.global.resources.getImage('speaker').image.src;

        const json = JSON.stringify(this.exercice.translate)
        const words = json.split(/(RETURN)/);
        //console.log('words: ', words)
        const arrayWithCarriageReturn = words.map((word) => {
            return word === 'RETURN' ? '\\n' : word
        });
        this.exercice.translate = arrayWithCarriageReturn.join('');
        this.exercice.translate = JSON.parse(this.exercice.translate);

        switch (this.refGame.global.resources.getLanguage()) {
            case "fr" :
                this.texte = new PIXI.Text(this.exercice.translate.fr, {
                    // fontFamily: 'Arial',
                    fontSize: 18,
                    fill: 0x000000,
                    align: 'left',
                    wordWrap: true,
                    wordWrapWidth: 580
                });
                this.ttsImg = new ImgStatic(50, 455, 32, this.tts, "", true, this.exercice.translate.fr);
                break;
            case "de" :
                this.texte = new PIXI.Text(this.exercice.translate.de, {
                    // fontFamily: 'Arial',
                    fontSize: 18,
                    fill: 0x000000,
                    align: 'left',
                    wordWrap: true,
                    wordWrapWidth: 580
                });
                this.ttsImg = new ImgStatic(50, 455, 32, this.tts, "", true, this.exercice.translate.de);
                break;
            case "it" :
                this.texte = new PIXI.Text(this.exercice.translate.it, {
                    // fontFamily: 'Arial',
                    fontSize: 18,
                    fill: 0x000000,
                    align: 'left',
                    wordWrap: true,
                    wordWrapWidth: 580
                });
                this.ttsImg = new ImgStatic(50, 455, 32, this.tts, "", true, this.exercice.translate.it);
                break;
            case "en" :
                this.texte = new PIXI.Text(this.exercice.translate.en, {
                    // fontFamily: 'Arial',
                    fontSize: 18,
                    fill: 0x000000,
                    align: 'left',
                    wordWrap: true,
                    wordWrapWidth: 580
                });
                this.ttsImg = new ImgStatic(50, 455, 32, this.tts, "", true, this.exercice.translate.en);
                break;

        }




       // this.texte.x = 300;
       // this.texte.y = 515;
       // this.texte.anchor.set(0.5);


        //this.texte, this.ttsImg
       // this.elements.push(this.texte, this.ttsImg);
       // this.elements.push(this.ttsImg);

        /** valeur des variables lock */
        this.correctWays = true;
        this.grid.setVisibleCorrectWays(this.correctWays);
        this.maplock = false;
        this.grid.setVisiblePoint(this.maplock);

        this.init();
    }

    changeLang(lang){

        console.log("testLangue")
        let langReturn="";
        switch (lang) {
            case "fr":
                langReturn=this.exercice.translate.fr;
                break;
            case "de":
                langReturn=this.exercice.translate.de;
                break;
            case "en":
                langReturn=this.exercice.translate.en;
                break;
            case "it":
                langReturn=this.exercice.translate.it;
                break;
        }
        return langReturn;
    }

    popUpWarning(texte){
        Swal.fire({
            title: name.toUpperCase(),
            width: 350,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td><div id="activites" style="text-align: justify">' + texte + '</div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    popUpError(error, message){
        Swal.fire({
            title: error,
            width: 500,
            icon: 'error',
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html: '<span style="color:red">' + message + '</span>'
        });
    }

    setWaysLock(){
        if(this.grid.blockVisibleWays){
            this.popUpWarning(this.refGame.global.resources.getOtherText("impossiblePass"));
            document.getElementById('btnLockWays').checked = !this.correctWays;
        } else {
            if(!this.correctWays){
                this.correctWays = true;
                this.grid.setVisibleCorrectWays(this.correctWays);
            } else {
                this.correctWays = false;
                this.grid.setVisibleCorrectWays(this.correctWays);
            }
        }
    }

    setMapLock(){
        if(this.maplock){
            this.maplock = false;
            this.grid.setVisiblePoint(this.maplock);
           // document.getElementById('btnLockMap').className = "btn btn-secondary";
        } else {
            this.maplock = true;
            this.grid.setVisiblePoint(this.maplock);
            //document.getElementById('btnLockMap').className = "btn btn-success";
        }
    }

    handleValider() {
        if(this.evaluate){
            // Initialisation des valeurs qu'on va utiliser
            let valueStartAndStop = 40; // 20 start and 20 stop
            let valueMin = 30;
            let valueMax = 60;
            let result = 0;
            // Il nous faut le pourcentage et les informations d'où il est passé ou non

            let resultReceive = this.grid.setOnShapeCreated(this.exercice.verif, this.arrayBoutons, this.evaluate);
            // 0 = pourcentage, 1 = texte
            let text = resultReceive.split(",", 2);
            // on attribut les points selon le pourcentage obtenu
            if((text[0] - valueStartAndStop)  < valueMin){
                result = 0;
            } else if ((text[0] - valueStartAndStop) >= valueMin && (text[0] - valueStartAndStop) < valueMax){
                result = 1;
            } else {
                result = 2;
            }
            // On met tous les informations dans un String (this.gameid est l'id du jeu)
            let resultSend = result;
            this.refGame.global.statistics.updateStats(resultSend);
            if(result === 2){

                Swal.fire({
                    title: this.refGame.global.resources.getOtherText('good'),
                    width: 500,
                    icon: 'success',
                    confirmButtonColor: BLEU,
                    confirmButtonText: 'OK',
                    showConfirmButton: true,
                    html: this.refGame.global.resources.getOtherText('goodWays')
                })
            } else {
                Swal.fire({
                    title: this.refGame.global.resources.getOtherText('errorPopup'),
                    width: 500,
                    icon: 'error',
                    confirmButtonColor: BLEU,
                    confirmButtonText: 'OK',
                    showConfirmButton: true,
                    html: this.refGame.global.resources.getOtherText('notGoodWaysTake')
                })
            }

            // On reset
            this.grid.reset();
            this.clear();
            this.elements = [];
            this.title = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
            this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
            this.startButton.setOnClick(this.newGame.bind(this));

            this.title.x = 300;
            this.title.y = 150;
            this.title.anchor.set(0.5);
            this.elements.push(this.title);
            this.elements.push(this.startButton);
            this.show(this.evaluate);
            /** On enlève la checkbox */
            this.refGame.global.util.hideTutorialInputs("btnLockMap");
            this.refGame.global.util.hideTutorialInputs("btnLockMapid");
        } else {
            /** On initialise la variable valueReturn */
            let valueReturn = "";

            /** On récupère la vérification du chemin envoyé */
            this.retour = this.grid.setOnShapeCreated(this.exercice.verif, this.arrayBoutons, this.evaluate);

            /** On vérifie si il a réussi ou non */
            if(this.retour !== 1){
                valueReturn = this.retour.split(",");
            } else {
                valueReturn = this.retour;
            }

            /** On vérifie que toute est juste sinon on affiche une popup avec les erreurs */
            if(valueReturn === 1){
                this.grid.reset();
                this.clear();
                this.elements = [];
                this.title = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'});
                this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
                this.startButton.setOnClick(this.newGame.bind(this));
                this.title.x = 300;
                this.title.y = 150;
                this.title.anchor.set(0.5);
                this.elements.push(this.title);
                this.elements.push(this.startButton);
                this.show(this.evaluate);
            } else {
                let resultPassage = valueReturn[1].split(";");
                let comptePassage = 0;

                /** On vérifie à partir du quel des chemins il s'est trompé */
                for (let i = 0; i < resultPassage.length-1 ; i++) {
                    if(resultPassage[i] === "Ok"){
                        comptePassage++;
                    }
                }
                /** On initialise la variable 'tradError' */
                let tradError = "";
                /** On récupére les traductions */
                for (let i = comptePassage; i <= resultPassage.length-1; i++) {
                    if(i === resultPassage.length-1){
                        switch (this.refGame.global.resources.getLanguage()) {
                            case "fr" :
                                tradError += this.exercice.error[i].fr + "";
                                break;
                            case "de" :
                                tradError += this.exercice.error[i].de + " ";
                                break;
                            case "it" :
                                tradError += this.exercice.error[i].it + " ";
                                break;
                            case "en" :
                                tradError += this.exercice.error[i].en + " ";
                                break;
                        }
                    } else {
                        switch (this.refGame.global.resources.getLanguage()) {
                            case "fr" :
                                tradError += this.exercice.error[i].fr + ", ";
                                break;
                            case "de" :
                                tradError += this.exercice.error[i].de + ", ";
                                break;
                            case "it" :
                                tradError += this.exercice.error[i].it + ", ";
                                break;
                            case "en" :
                                tradError += this.exercice.error[i].en + ", ";
                                break;
                        }
                    }

                }
                /** On envoie les traductions dans la méthode pour la popup */
                this.popUpError(this.refGame.global.resources.getOtherText('errorLocation'), tradError);
            }
            /** On enlève les checkboxs */
            this.refGame.global.util.hideTutorialInputs("btnLockMap");
            this.refGame.global.util.hideTutorialInputs("btnLockMapid");
            this.refGame.global.util.hideTutorialInputs("btnLockWays");
            this.refGame.global.util.hideTutorialInputs("btnLockWaysid");
        }
    }

    handleReset() {
        this.grid.onReset(this.arrayBoutons);
    }

    handleLockMap(){
        this.setMapLock();
    }

    handleLockWays(){
        this.setWaysLock();
    }

    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }

}