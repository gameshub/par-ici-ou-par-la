class Play extends Interface {

    constructor(refGame, refTrainMode) {

        super(refGame, "play");

        this.clear();
        this.elements = [];

        this.exercices = {};

        this.selectedGameId = -1;

        this.refTrainMode = refTrainMode;

        this.title = new PIXI.Text('', {fontFamily: 'Arial', fontSize: 16, fill: 0x000000, align: 'left'});
        this.scrollPane = new ScrollPane(0, 60, 600, 480);
        this.btn = new Button(300, 570, '', 0x007bff, 0xFFFFFF, true);

        this.title.anchor.set(0.5);
        this.title.x = 300;
        this.title.y = 30;

         this.btn.setOnClick(function () {
            if (this.selectedGameId < 0)
                return;
            this.playExerice(this.selectedGameId);
        }.bind(this));


        this.elements.push(this.title);
        this.elements.push(this.scrollPane);
        this.elements.push(this.btn);
    }

    init() {
        this.refGame.global.util.hideTutorialInputs('btnReset', "next");
        super.init();
    }

    show() {
        this.clear();

        this.exercices = this.refGame.global.resources.getExercicesEleves();
        if (this.exercices) this.exercices = JSON.parse(this.exercices);

        this.refreshLang(this.refGame.global.resources.getLanguage());

        this.setElements(this.elements);

        this.init();
    }

    refreshLang(lang) {
        this.generateScrollPane(lang);
        this.refGame.showText(this.refGame.global.resources.getOtherText("play"));
        this.btn.setText(this.refGame.global.resources.getOtherText('playBtn'));

        this.elements.push(this.title);
        this.elements.push(this.scrollPane);
        this.elements.push(this.btn);
    }

    refreshFont(isOpenDyslexic){

    }

    playExerice(exId) {
        this.refGame.trainMode.show(this.exercices[exId]);
    }

    generateScrollPane(lang) {
        if (!this.exercices)
            return;
        this.scrollPane.clear();
        let tg = new ToggleGroup('single');
        for (let exID of Object.keys(this.exercices)) {
            let ex = this.exercices[exID];
            let label = this.refGame.global.resources.getOtherText('routePlayer') + ex.name;
            let btn = new ToggleButton(0, 0, label, false, 580);
            btn.enable();
//            btn = new Button(0, 0, label, 0x00AA00, 0xFFFFFF, false, 580);
            tg.addButton(btn);

            btn.setOnClick(function () {
                this.selectedGameId = exID;
            }.bind(this));
            this.scrollPane.addElements(btn);
        }
        this.scrollPane.init();

        this.elements.push(this.title);
        this.elements.push(this.scrollPane);
        this.elements.push(this.btn);
    }
}