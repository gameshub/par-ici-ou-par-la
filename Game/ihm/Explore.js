class Explore extends Interface {
    constructor(refGame) {
        super(refGame, "explore");
        this.tuto = true;
        this.maplock = false;
        this.refGame = refGame;
        this.exercice = JSON.parse(this.refGame.global.resources.getScenario());
    }

    show() {
        console.log(this.exercice);
        this.clear();
        //permet de créer la grille puis de la mettre dans le canvas et on désactive le tracé
        this.grid = new DrawingGrid(11, 9, 50, this.scene);
        this.grid.setRefGameDG(this.refGame);
        this.grid.reset();
        this.counterSteps = 1;
        this.elements.push(this.grid);
        this.grid.setBlockNode(true);

        this.dys = true;
        let langue = this.refreshLang(this.refGame.global.resources.getLanguage(), 3);
        let lang = this.refGame.global.resources.getLanguage();
        this.refGame.global.util.hideTutorialInputs('btnValider', 'btnResetTr', "btnLockWays", "btnLockMap", "btnAudioGame");

        //permet l'affichage des checkBox pour cacher la grille et de desactivation de suivie
        if (this.exercice.exerciceDeBase.enableLockWays) {
            this.refGame.global.util.showTutorialInputs("btnLockWays");
            this.refGame.global.util.showTutorialInputs("btnLockWaysid");
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnLockWays'), ['click'], this.handleLockWays.bind(this));
        } else {
            this.refGame.global.util.hideTutorialInputs("btnLockWays");
            this.refGame.global.util.hideTutorialInputs("btnLockWaysid");
        }
        if (this.exercice.exerciceDeBase.enableLockMap) {
            this.refGame.global.util.showTutorialInputs("btnLockMap");
            this.refGame.global.util.showTutorialInputs("btnLockMapid");
            this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnLockMap'), ['click'], this.handleLockMap.bind(this));
        } else {
            this.refGame.global.util.hideTutorialInputs("btnLockMap");
            this.refGame.global.util.hideTutorialInputs("btnLockMapid");
        }

        //montre le premier texte avec la consigne en entier
        this.refGame.global.util.showTutorialInputs('next', 'btnReset', 'tuto1', 'tuto2', 'tuto3');
        this.refGame.global.util.setTutorialText("<p class='scroller'>" + "Bienvenue dans Par ici ou par-là, le texte ci-dessous t'aideras à savoir où Jean est allé." + "</p>" + '<p style="font-size:20px;" class="scroller">' + langue + "</p>");

        document.getElementById("tutorial").className = 'smoothie ';
        // document.getElementById("tutorial").style.overflowY='scroll';


        //this.refGame.global.listenerManager.addListenerOn(document.getElementById('next'), ['click'], this.handleNext.bind(this));
        document.getElementById('next').addEventListener("click", this.handleNext.bind(this));


        //création des flèches pour le tuto

        //flèches pour le point de départ
        this.fleche = new Arrow(505, 367.5, 482, 367.5, 0x30B21A, false, [505, 385, 482, 385]);
        this.flechePoint1 = new Arrow(505, 368, 482, 368, 0x30B21A, false, [505, 350, 482, 350]);

        //flèche pour le premier point de passage obligatoire
        this.fleche2 = new Arrow(348, 317.5, 238, 317.5, 0x0000ff, false, [348, 317.5, 238, 317.5]);
        //flèche pour le deuxième point de passage obligatoire
        this.fleche3 = new Arrow(148, 417.5, 38, 417.5, 0x2FFF9F, false, [148, 417.5, 38, 417.5]);
        //flèche pour le troisième point de passage obligatoire
        this.fleche4 = new Arrow(298, 67.5, 290, 67.5, 0xff0000, false, [298, 67.5, 290, 67.5]);
        //flèches pour le point d'arrivé
        this.fleche5 = new Arrow(505, 67.5, 482, 67.5, 0xff0000, false, [505, 52, 482, 52]);
        this.flechPoint2 = new Arrow(505, 66, 482, 66, 0xff0000, false, [505, 82, 482, 82]);


        //on cache toutes les flèches
        this.fleche.setVisible(false);
        this.flechePoint1.setVisible(false);
        this.fleche2.setVisible(false);
        this.fleche3.setVisible(false);
        this.fleche4.setVisible(false);
        this.fleche5.setVisible(false);
        this.flechPoint2.setVisible(false);
        //###########################IMAGES###########################
        //= '{\n' + '  "exerciceDeBase": {\n' + '    "listeBoutons": {\n' + '      "boutons": {\n' + '        "1": {\n' + '          "zone": {\n' + '            "x": "473",\n' + '            "y": "484",\n' + '            "w": "188",\n' + '            "h": "15"\n' + '          },\n' + '          "x": "473",\n' + '          "y": "484",\n' + '          "w": "188",\n' + '          "h": "15"\n' + '        },\n' + '        "2": {\n' + '          "zone": {\n' + '            "x": "40",\n' + '            "y": "500",\n' + '            "w": "40",\n' + '            "h": "15"\n' + '          },\n' + '          "x": "40",\n' + '          "y": "500",\n' + '          "w": "40",\n' + '          "h": "15"\n' + '        },\n' + '        "3": {\n' + '          "zone": {\n' + '            "x": "191",\n' + '            "y": "500",\n' + '            "w": "255",\n' + '            "h": "15"\n' + '          },\n' + '          "x": "191",\n' + '          "y": "500",\n' + '          "w": "255",\n' + '          "h": "15"\n' + '        },\n' + '        "4": {\n' + '          "zone": {\n' + '            "x": "543",\n' + '            "y": "500",\n' + '            "w": "70",\n' + '            "h": "15"\n' + '          },\n' + '          "x": "543",\n' + '          "y": "500",\n' + '          "w": "70",\n' + '          "h": "15"\n' + '        },\n' + '        "5": {\n' + '          "zone": {\n' + '            "x": "90",\n' + '            "y": "517",\n' + '            "w": "140",\n' + '            "h": "15"\n' + '          },\n' + '          "x": "90",\n' + '          "y": "517",\n' + '          "w": "140",\n' + '          "h": "15"\n' + '        }\n' + '      }\n' + '    },\n' + '    "background": {\n' + '      "x": "300",\n' + '      "y": "220",\n' + '      "width": "507",\n' + '      "height": "419"\n' + '    },\n' + '    "pictures": {\n' + '      "1": {\n' + '        "nomFichier": "apartment",\n' + '        "x": "480",\n' + '        "y": "170",\n' + '        "width": "90",\n' + '        "nom": ""\n' + '      },\n' + '      "2": {\n' + '        "nomFichier": "appleTree",\n' + '        "x": "480",\n' + '        "y": "280",\n' + '        "width": "90",\n' + '        "nom": "Pommier"\n' + '      },\n' + '      "3": {\n' + '        "nomFichier": "largeTree",\n' + '        "x": "360",\n' + '        "y": "200",\n' + '        "width": "90",\n' + '        "nom": "Arbre geant"\n' + '      },\n' + '      "4": {\n' + '        "nomFichier": "house",\n' + '        "x": "290",\n' + '        "y": "360",\n' + '        "width": "90",\n' + '        "nom": "Ecole"\n' + '      },\n' + '      "5": {\n' + '        "nomFichier": "everGreen",\n' + '        "x": "210",\n' + '        "y": "362",\n' + '        "width": "64",\n' + '        "nom": ""\n' + '      },\n' + '      "6": {\n' + '        "nomFichier": "company",\n' + '        "x": "110",\n' + '        "y": "270",\n' + '        "width": "90",\n' + '        "nom": "Usine"\n' + '      },\n' + '      "7": {\n' + '        "nomFichier": "barn",\n' + '        "x": "140",\n' + '        "y": "110",\n' + '        "width": "90",\n' + '        "nom": "Ferme"\n' + '      },\n' + '      "8": {\n' + '        "nomFichier": "home96",\n' + '        "x": "480",\n' + '        "y": "60",\n' + '        "width": "90",\n' + '        "nom": ""\n' + '      },\n' + '      "9": {\n' + '        "nomFichier": "museum",\n' + '        "x": "270",\n' + '        "y": "45",\n' + '        "width": "90",\n' + '        "nom": "musee"\n' + '      },\n' + '      "10": {\n' + '        "nomFichier": "building",\n' + '        "x": "220",\n' + '        "y": "200",\n' + '        "width": "90",\n' + '        "nom": ""\n' + '      },\n' + '      "11": {\n' + '        "nomFichier": "son",\n' + '        "x": "550",\n' + '        "y": "200",\n' + '        "width": "40",\n' + '        "nom": "Pierre"\n' + '      },\n' + '      "12": {\n' + '        "nomFichier": "userFemaleRedHair",\n' + '        "x": "550",\n' + '        "y": "150",\n' + '        "width": "40",\n' + '        "nom": "Jenifer"\n' + '      },\n' + '      "13": {\n' + '        "nomFichier": "boy",\n' + '        "x": "550",\n' + '        "y": "60",\n' + '        "width": "40",\n' + '        "nom": "Tom"\n' + '      },\n' + '      "14": {\n' + '        "nomFichier": "businessWoman",\n' + '        "x": "280",\n' + '        "y": "200",\n' + '        "width": "40",\n' + '        "nom": "Laure"\n' + '      },\n' + '      "15": {\n' + '        "nomFichier": "caroussel",\n' + '        "x": "360",\n' + '        "y": "60",\n' + '        "width": "40",\n' + '        "nom": "Place de jeu"\n' + '      },\n' + '      "16": {\n' + '        "nomFichier": "standingMan",\n' + '        "x": "450",\n' + '        "y": "380",\n' + '        "width": "64",\n' + '        "nom": "Jean"\n' + '      }\n' + '    }\n' + '  }\n' + '}';
        // const EXDEBASE =
        // console.log(EXDEBASE());

        let count = 1;




        //permet de mettre les images du scénario dnas le canvas
        for (let key in this.exercice.exerciceDeBase.pictures) {
            let imgNom = this.exercice.exerciceDeBase.pictures[count];
            let img = this.refGame.global.resources.getImage(imgNom.nomFichier).image.src;
            let aInserer = new ImgStatic(imgNom.x, imgNom.y, imgNom.width, img, imgNom.nom[0], false, "");
            this.elements.push(aInserer);
            count++;
        }
        //on met tout les élement dans le canvas
        this.elements.push(this.grid);
        this.elements.push(this.flechePoint1, this.fleche, this.fleche2, this.fleche3, this.fleche4, this.fleche5, this.flechPoint2);

        count = 1;



        this.init();
        this.tuto = false;

    }

    // RAM Le nom de la méthode n'est pas très explicite
    popUp(texte) {

        Swal.fire({
            title: name.toUpperCase(),
            width: 1000,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                //'<td style="padding-right: 30px">' + '<img src=' + imgSousCat + ' width="160px" height="160px"></td>' +
                '<td><div id="activites" style="text-align: justify">' + texte + '</div></td>' +
                '<td style="padding-left: 30px"><div><i class="speaker fas fa-volume-up fa-3x" onclick="audio.textToSpeech(document.getElementById(\'activites\').id);"/></div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }

    // RAM Il manque la description de la méthode
    handleNext() {

        let langue = "";


        switch (this.counterSteps) {
            //première fois qu'on pèse sur le bouton on change le texte d'intoduction et on montre le point de départ en rouge et le point d'aarivé en vert.
            case 1:
                langue = this.refreshLang(this.refGame.global.resources.getLanguage(), 0);
                // RAM Le texte doit être dans les traductions de la base de données
                this.refGame.global.util.setTutorialText("Tout d'abord, il te faudra trouver le point de <b style='color:green'>départ</b> et celui d'<b style='color:red'>arrivée</b> dans la consigne." + "<br>" + '<p style="font-size:20px;">' + "C'est mercredi, <b style='color:green'>Jean</b> est en route pour aller chez <b style='color:red'>Tom</b> qui l'a invité pour le goûter" + "</p>");
                //On rend visible les flèches d'arrivé et de départ
                this.fleche.setVisible(true);
                this.flechePoint1.setVisible(true);
                this.fleche5.setVisible(true);
                this.flechPoint2.setVisible(true);
                this.counterSteps = 2;
                break;
            //deuxième fois qu'on pèse sur le bouton on rechange le texte d'introduction pour montrer les points de passages qui se trouvent dans la consigne
            // puis on cache les flèche du cas précédant et on affiches les trois point de pasages
            case 2:
                let langue1 = this.refreshLang(this.refGame.global.resources.getLanguage(), 0);
                let langue2 = this.refreshLang(this.refGame.global.resources.getLanguage(), 1);
                let langue3 = this.refreshLang(this.refGame.global.resources.getLanguage(), 2);
                // RAM Le texte doit être dans les traductions de la base de données
                this.refGame.global.util.setTutorialText("En suite, il va faloir repérer les points de passages obligatoires présents dans la phrase." + '<p style="font-size:20px; color:blue;">' + langue1 + "</p>" + '<p style="font-size:20px; color:#2fff9f">' + langue2 + "</p>" + '<p style="font-size:20px; color:red">' + langue3 + "</p>");
                //on rend invisible les deux flèches précédante
                this.fleche.setVisible(false);
                this.flechePoint1.setVisible(false);
                this.fleche5.setVisible(false);
                this.flechPoint2.setVisible(false);


                //on rend visible les flèches des points de passages
                this.fleche2.setVisible(true);
                this.fleche3.setVisible(true);
                this.fleche4.setVisible(true);
                // RAM Le texte "Commencer" doit être dans les traductions de la base de données
                document.getElementById("next").setAttribute('value', "Commencer");
                this.counterSteps = 3;
                break;


            //la troisième fois qu'on pèse sur le bouton on rend le jeux jouable en affichant toute la consigne et en cachant les flèches du cas précédant
            case 3:
                this.fleche.setVisible(false);
                this.flechePoint1.setVisible(false);
                this.fleche2.setVisible(false);
                this.fleche3.setVisible(false);
                this.fleche4.setVisible(false);
                langue = this.refreshLang(this.refGame.global.resources.getLanguage(), 3);
                // RAM Le texte doit être dans les traductions de la base de données
                this.refGame.global.util.setTutorialText("Et maintenant c'est a ton tour ! Trace le chemin que Jean a emprunté pour aller chez Tom." + "<br>" + '<p style="font-size:20px;">' + langue + "</p>");

                //on affiche les boutons valider et réessayer et on leur met un event pour quand on clique dessus
                this.refGame.global.util.showTutorialInputs('btnValider', 'btnResetTr', "btnConfiguration");
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnValider'), ['click'], this.handleValider.bind(this));
                this.refGame.global.listenerManager.addListenerOn(document.getElementById('btnResetTr'), ['click'], this.handleReset.bind(this));

                //rend la grille de jeu utilisable
                this.grid.setBlockNode(false);
                // RAM Le texte "Recommencer" doit être dans les traductions de la base de données
                document.getElementById("next").setAttribute('value', "Recommencer");
                document.getElementById("next").setAttribute('onclick', "location.reload()");

                break;

            //dans le cas ou on pèse encore une fois après que le jeux sois lancé on relance la page pour recommancer le tuto.

        }

    }


    // RAM Il manque la description de la méthode
    handleAudioGame() {
        this.popUp(this.refGame.global.resources.getOtherText('exBaseTxt'));
    }
    // RAM Il manque la description de la méthode
    dessin(x, y, r, n, i) {
        this.graphics = new PIXI.Graphics();
        this.graphics.beginFill(0x12badb);
        this.rectangle = this.graphics.drawRoundedRect(x, y, r, n, i);
        this.graphics.visible = false;
        this.graphics.hitarea = this.rectangle;
        return this.graphics;
    }
    // RAM Il manque la description de la méthode
    boutonCarte(x, y, r, n, i) {
        let rectangle = new PIXI.Graphics();
        rectangle.beginFill(0xffffff);
        rectangle.drawRoundedRect(x, y, r, n, i);
        rectangle.visible = false;
        rectangle.hitarea = this.rectangle;
        rectangle.interactive = true;
        rectangle.buttonMode = true;
        return rectangle;
    }
    // RAM Il manque la description de la méthode
    handleValider() {

        if (this.evaluate) {
            // RAM est-ce que c'est des constantes ? Les variables de types constantes doivent être en majuscule et avec le type const
            // Initialisation des valeurs qu'on va utiliser
            let valueStartAndStop = 40; // 20 start and 20 stop
            let valueMin = 30;
            let valueMax = 60;
            let result = 0;
            // Il nous faut le pourcentage et les informations d'où il est passé ou non
            let resultReceive = this.grid.setOnShapeCreated(this.exercice.verif, this.arrayBoutons, false);
            // 0 = pourcentage, 1 = texte
            let text = resultReceive.split(",", 2);
            // on attribut les points selon le pourcentage obtenu
            if ((text[0] - valueStartAndStop) < valueMin) {
                result = 0;
            } else if ((text[0] - valueStartAndStop) >= valueMin && (text[0] - valueStartAndStop) < valueMax) {
                result = 1;
            } else {
                result = 2;
            }
            // On met tous les informations dans un String (this.gameid est l'id du jeu)
            let resultSend;
            this.refGame.global.statistics.updateStats(resultSend);

            if (result === 2) {
                if (document.getElementById("progressBar") != null) {
                    window.history.back();
                }
                Swal.fire(
                    {

                        title: this.refGame.global.resources.getOtherText('good'),
                        width: 500,
                        icon: 'success',
                        confirmButtonColor: BLEU,
                        confirmButtonText: 'OK',
                        showConfirmButton: true,
                        html: this.refGame.global.resources.getOtherText('goodWays'),

                    });


            } else {
                Swal.fire({
                    title: this.refGame.global.resources.getOtherText('errorPopup'),
                    width: 500,
                    icon: 'error',
                    confirmButtonColor: BLEU,
                    confirmButtonText: 'OK',
                    showConfirmButton: true,
                    html: this.refGame.global.resources.getOtherText('notGoodWaysTake')
                });
            }

            // On reset

            this.grid.reset();
            this.clear();
            this.elements = [];
            this.title = new PIXI.Text('', { fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center' });
            this.startButton = new Button(300, 400, '', 0x007bff, 0xFFFFFF, true);
            // this.startButton.setOnClick(this.newGame.bind(this));

            this.title.x = 300;
            this.title.y = 150;
            this.title.anchor.set(0.5);
            this.elements.push(this.title);
            this.elements.push(this.startButton);
            this.show(this.evaluate);
            /** On enlève la checkbox */
            this.refGame.global.util.hideTutorialInputs("btnLockMap");
            this.refGame.global.util.hideTutorialInputs("btnLockMapid");
        } else {
            /** On initialise la variable valueReturn */
            let valueReturn = "";

            /** On récupère la vérification du chemin envoyé */
            this.retour = this.grid.setOnShapeCreated(this.exercice.exerciceDeBase.verif, this.arrayBoutons, false);


            /** On vérifie si il a réussi ou non */
            if (this.retour !== 1) {
                valueReturn = this.retour.split(",");
            } else {
                valueReturn = this.retour;
            }

            /** On vérifie que toute est juste sinon on affiche une popup avec les erreurs */
            if (valueReturn === 1) {
                this.show(this.evaluate);

            } else {
                let resultPassage = valueReturn[1].split(";");
                let comptePassage = 0;

                /** On vérifie à partir du quel des chemins il s'est trompé */
                for (let i = 0; i < resultPassage.length - 1; i++) {
                    if (resultPassage[i] === "Ok") {
                        comptePassage++;
                    }
                }
                /** On initialise la variable 'tradError' */
                let tradError = "";
                /** On récupére les traductions */
                for (let i = comptePassage; i <= resultPassage.length - 1; i++) {
                    // RAM c'est quoi la différence entre le if et le else if ?
                    if (i === resultPassage.length - 1) {
                        switch (this.refGame.global.resources.getLanguage()) {
                            case "fr":
                                tradError += this.exercice.error[i].fr + "";
                                break;
                            case "de":
                                tradError += this.exercice.error[i].de + " ";
                                break;
                            case "it":
                                tradError += this.exercice.error[i].it + " ";
                                break;
                            case "en":
                                tradError += this.exercice.error[i].en + " ";
                                break;
                        }
                    } else {
                        switch (this.refGame.global.resources.getLanguage()) {
                            case "fr":
                                tradError += this.exercice.error[i].fr + ", ";
                                break;
                            case "de":
                                tradError += this.exercice.error[i].de + ", ";
                                break;
                            case "it":
                                tradError += this.exercice.error[i].it + ", ";
                                break;
                            case "en":
                                tradError += this.exercice.error[i].en + ", ";
                                break;
                        }
                    }

                }
                /** On envoie les traductions dans la méthode pour la popup */
                this.popUpError(this.refGame.global.resources.getOtherText('errorLocation'), tradError);
            }
            /** On enlève les checkboxs */
            this.refGame.global.util.hideTutorialInputs("btnLockMap");
            this.refGame.global.util.hideTutorialInputs("btnLockMapid");
            this.refGame.global.util.hideTutorialInputs("btnLockWays");
            this.refGame.global.util.hideTutorialInputs("btnLockWaysid");
        }

    }



    // RAM Manque la description de la méthode
    refreshLang(lang, nbr) {
        let result;
        // RAM Es-ce que l'on pourrait pas simplifier le code ?
        switch (nbr) {
            case 0:
                switch (lang) {
                    case "en":
                        result = this.exercice.exerciceDeBase.error[0].en;
                        break;
                    case "it":
                        result = this.exercice.exerciceDeBase.error[0].it;
                        break;
                    case "de":
                        result = this.exercice.exerciceDeBase.error[0].de;
                        break;
                    case "fr":
                        result = this.exercice.exerciceDeBase.error[0].fr;
                        break;
                }
                break;
            case 1:
                switch (lang) {
                    case "en":
                        result = this.exercice.exerciceDeBase.error[1].en;
                        break;
                    case "it":
                        result = this.exercice.exerciceDeBase.error[1].it;
                        break;
                    case "de":
                        result = this.exercice.exerciceDeBase.error[1].de;
                        break;
                    case "fr":
                        result = this.exercice.exerciceDeBase.error[1].fr;
                        break;
                }
                break;
            case 2:
                switch (lang) {
                    case "en":
                        result = this.exercice.exerciceDeBase.error[2].en;
                        break;
                    case "it":
                        result = this.exercice.exerciceDeBase.error[2].it;
                        break;
                    case "de":
                        result = this.exercice.exerciceDeBase.error[2].de;
                        break;
                    case "fr":
                        result = this.exercice.exerciceDeBase.error[2].fr;
                        break;
                }
                break;

            case 3:
                switch (lang) {
                    case "en":
                        result = this.exercice.exerciceDeBase.translate.en;
                        break;
                    case "it":
                        result = this.exercice.exerciceDeBase.translate.it;
                        break;
                    case "de":
                        result = this.exercice.exerciceDeBase.translate.de;
                        break;
                    case "fr":
                        result = this.exercice.exerciceDeBase.translate.fr;
                        break;
                }
                break;
        }

        return result;
    }
    // RAM Manque la description de la méthode
    handleReset() {
        this.grid.onReset(this.arrayBoutons);
    }

    // RAM Manque la description de la méthode
    setOnShapeCreated(onShapeCreated) {
        this.onShapeCreated = onShapeCreated;
    }
    // RAM Manque la description de la méthode
    popUpWarning(texte) {
        Swal.fire({
            title: name.toUpperCase(),
            width: 350,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html:
                '<div style="padding: 30px">' +
                '<table>' +
                '<tbody">' +
                '<tr>' +
                '<td><div id="activites" style="text-align: justify">' + texte + '</div></td>' +
                '</tr>' +
                '</tbody>' +
                '</table>'
        });
    }
    // RAM Manque la description de la méthode
    popUpError(error, message) {
        Swal.fire({
            title: error,
            width: 500,
            icon: 'error',
            confirmButtonColor: BLEU,
            confirmButtonText: 'OK',
            showConfirmButton: true,
            html: '<span style="color:red">' + message + '</span>'
        });
    }


    //rend la grille visible et invisible quand on pèse sur la checkBox
    setMapLock() {
        if (this.maplock) {
            this.maplock = false;
            this.grid.setVisiblePoint(this.maplock);
            document.getElementById('btnLockMap').className = "btn btn-secondary";
        } else {
            this.maplock = true;
            this.grid.setVisiblePoint(this.maplock);
            document.getElementById('btnLockMap').className = "btn btn-success";
        }
    }

    // RAM Manque la description de la méthode
    handleLockMap() {
        this.setMapLock();
    }

    //permet d'activer / desactiver le controlle a la fin ud tuto
    setWaysLock() {
        if (this.grid.blockVisibleWays) {
            this.popUpWarning(this.refGame.global.resources.getOtherText("impossiblePass"));
            document.getElementById('btnLockWays').checked = !this.correctWays;
        } else {
            if (!this.correctWays) {
                this.correctWays = true;
                this.grid.setVisibleCorrectWays(this.correctWays);
            } else {
                this.correctWays = false;
                this.grid.setVisibleCorrectWays(this.correctWays);
            }
        }
    }

    // RAM Manque la description de la méthode
    handleLockWays() {
        this.setWaysLock();
    }


}